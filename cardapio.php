<?php include("includes/constantes.php") ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php include("head.php") ?>
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <?php include("header_body.php") ?>

      <?php include("modal/modal_cad_lanches.php") ?>
      <?php include("modal/modal_alt_lanches.php") ?>

      <?php include("modal/modal_cad_ingredientes.php") ?>
      <?php include("modal/modal_alt_ingredientes.php") ?>

      <?php include("nav_body.php") ?>
      
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Cardápio
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-list-alt"></i> Cardápio</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active">
                        <a href="#tab_1" id="btn-tab-lanches" data-toggle="tab"><i class="fa fa-coffee"></i> Lanches <span class="label label-default" id="badge-TotalRegistrosLanches">0</span></a>
                      </li>
                      <li >
                        <a href="#tab_2" id="btn-tab-ingredientes" data-toggle="tab"><i class="fa fa-lemon-o"></i> Ingredientes <span class="label label-default" id="badge-TotalRegistrosIngredientes">0</span></a>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                        <div class="row" style="padding-bottom: 10px;">
                          <div class="col-xs-12 col-sm-12 col-md-3 text-center">
                            <button type="button" id="btn-cad-lanches" class="btn btn-info"><i class="fa fa-plus"></i> Lanche</button>
                          </div>
                          <div class="col-xs-12 visible-xs visible-sm">
                            <hr>
                          </div>
                          <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="box-group" id="accordion">
                              <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                              <div class="panel box box-primary">
                                <a data-toggle="collapse" data-parent="#accordion" id="btn-collapse-filtros-lanches" href="#collapseFiltros">
                                  <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <i class="fa fa-filter"></i> Filtros
                                    </h4>
                                  </div>
                                </a>
                                <div id="collapseFiltros" class="panel-collapse collapse">
                                  <div class="box-body">
                                    <div class="row">
                                      <form id="form_filtros_lanches">
                                        <div class="col-xs-4 col-sm-3 col-md-3" id="filtros_lanches_divCodigo">
                                          <div class="form-group" id="filtros_lanches_div_Codigo">
                                            <label class="control-label" for="filtros_lanches_input_Codigo">Codigo</label>
                                            <input type="text" class="form-control" name="filtros_lanches_input_Codigo">
                                          </div>
                                        </div>
                                        <div class="col-xs-8 col-sm-4 col-md-9" id="filtros_lanches_divNome">
                                          <div class="form-group" id="filtros_lanches_div_Nome">
                                            <label class="control-label" for="filtros_lanches_input_Nome">Nome</label>
                                            <input type="text" class="form-control" name="filtros_lanches_input_Nome">
                                          </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-2 text-center" id="div-btn-filtrar-lanches">
                                          <button type="button" class="btn btn-primary" id="btn-filtrar-lanches"><i class="fa fa-filter"></i> Filtrar</button>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 text-center" id="div-btn-remover-filtros-lanches" style="display: none;">
                                          <button type="button" class="btn btn-danger" id="btn-remover-filtros-lanches"><i class="fa fa-filter"></i> Remover Filtros</button>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div><!-- /.box-body -->
                        </div>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabLanches" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Nome</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Valor de Produção</th>
                                  <th class="text-center">Valor de Venda</th>
                                  <th class="text-center">Situação</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosLanches">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Nome</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Valor de Produção</th>
                                  <th class="text-center">Valor de Venda</th>
                                  <th class="text-center">Situação</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-lanches-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-lanches">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <div class="row" style="padding-bottom: 10px;">
                          <div class="col-xs-12 col-sm-12 col-md-3 text-center">
                            <button type="button" id="btn-cad-ingredientes" class="btn btn-info"><i class="fa fa-plus"></i> Ingredientes</button>
                          </div>
                          <div class="col-xs-12 visible-xs visible-sm">
                            <hr>
                          </div>
                          <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="box-group" id="accordion">
                              <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                              <div class="panel box box-primary">
                                <a data-toggle="collapse" data-parent="#accordion" id="btn-collapse-filtros-ingredientes" href="#collapseFiltros2">
                                  <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <i class="fa fa-filter"></i> Filtros
                                    </h4>
                                  </div>
                                </a>
                                <div id="collapseFiltros2" class="panel-collapse collapse">
                                  <div class="box-body">
                                    <div class="row">
                                      <form id="form_filtros_ingredientes">
                                        <div class="col-xs-4 col-sm-3 col-md-3" id="filtros_ingredientes_divCodigo">
                                          <div class="form-group" id="filtros_ingredientes_div_Codigo">
                                            <label class="control-label" for="filtros_ingredientes_input_Codigo">Codigo</label>
                                            <input type="text" class="form-control" name="filtros_ingredientes_input_Codigo">
                                          </div>
                                        </div>
                                        <div class="col-xs-8 col-sm-4 col-md-9" id="filtros_ingredientes_divNome">
                                          <div class="form-group" id="filtros_ingredientes_div_Nome">
                                            <label class="control-label" for="filtros_ingredientes_input_Nome">Nome</label>
                                            <input type="text" class="form-control" name="filtros_ingredientes_input_Nome">
                                          </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-2 text-center" id="div-btn-filtrar-entradasAvulsas">
                                          <button type="button" class="btn btn-primary" id="btn-filtrar-entradasAvulsas"><i class="fa fa-filter"></i> Filtrar</button>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 text-center" id="div-btn-remover-filtros-entradasAvulsas" style="display: none;">
                                          <button type="button" class="btn btn-danger" id="btn-remover-filtros-entradasAvulsas"><i class="fa fa-filter"></i> Remover Filtros</button>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div><!-- /.box-body -->
                        </div>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabIngredientes" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Nome</th>
                                  <th class="text-center">Descricao</th>
                                  <th class="text-center">Valor de Compra</th>
                                  <th class="text-center">Valor de Venda</th>
                                  <th class="text-center">Adicional?</th>
                                  <th class="text-center">Situação</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosIngredientes">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Nome</th>
                                  <th class="text-center">Descricao</th>
                                  <th class="text-center">Valor de Compra</th>
                                  <th class="text-center">Valor de Venda</th>
                                  <th class="text-center">Adicional?</th>
                                  <th class="text-center">Situação</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-ingredientes-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-ingredientes">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                  </div><!-- nav-tabs-custom -->
                </div>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("footer_body.php") ?>
    </div><!-- ./wrapper -->
    <?php include("footer.php") ?>
    <!--Fast Command - PainelControle-->
    <script src="plugins/fast-command/cardapio.js"></script>
    <script src="plugins/fast-command/lanches.js"></script>
    <script src="plugins/fast-command/ingredientes.js"></script>
  </body>
</html>