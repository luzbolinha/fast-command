<form id="form_pag_EntradaAvulsa">
	<input type="hidden" name="modal_pag_entradaAvulsa_input_Codigo">
	<input type="hidden" name="modal_pag_entradaAvulsa_input_VagaPreferencialCodigo">
	<div id="modal_pag_EntradaAvulsa" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-12" id="modal_pag_entradaAvulsa_MensalistaNome" style="display: none;">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_MensalistaNome">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_MensalistaNome">Mensalista <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_MensalistaNome" disabled>
					</div>
				</div>
				<div class="col-sm-12 col-md-5" id="modal_pag_entradaAvulsa_div_divDescricao">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_Descricao">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_Descricao">Descricao</label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_Descricao" disabled>
					</div>
				</div>
				<div class="col-sm-12 col-md-3" id="modal_pag_entradaAvulsa_div_divPlaca">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_Placa">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_Placa">Placa <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_Placa" style="text-transform: uppercase;" disabled>
					</div>
				</div>
				<div class="col-sm-4 col-md-4" id="modal_pag_entradaAvulsa_TipoVeiculo">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_TipoVeiculo">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_TipoVeiculo">Tipo de Veiculo <span class="text-danger">*</span></label>
						<select class="form-control" data-live-search="true" name="modal_pag_entradaAvulsa_input_TipoVeiculo" disabled>
						</select>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divDataEntrada">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_DataEntrada">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_DataEntrada">Data Entrada <span class="text-danger">*</span></label>
						<input type="date" class="form-control" name="modal_pag_entradaAvulsa_input_DataEntrada" disabled>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divHorarioEntrada">
					<div class="bootstrap-timepicker">
						<div class="form-group" id="modal_pag_entradaAvulsa_div_HorarioEntrada">
							<label class="control-label" for="modal_pag_entradaAvulsa_input_HorarioEntrada">Horario Entrada <span class="text-danger">*</span></label>
							<input type="text" class="form-control timepicker" name="modal_pag_entradaAvulsa_input_HorarioEntrada" disabled>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divVagaPreferencial">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_VagaPreferencial">
						<div class="row">
							<div class="col-md-12">
								<label class="control-label" for="modal_pag_entradaAvulsa_input_VagaPreferencial"><span class="btn btn-xs btn-primary"><i class="fa fa-wheelchair"></i></span> Vaga Preferêncial?</label>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_VagaPreferencial" data-value="1" disabled> Sim
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_VagaPreferencial" data-value="2" disabled> Não
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divDataSaida">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_DataSaida">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_DataSaida">Data Saida <span class="text-danger">*</span></label>
						<input type="date" class="form-control" name="modal_pag_entradaAvulsa_input_DataSaida">
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divHorarioSaida">
					<div class="bootstrap-timepicker">
						<div class="form-group" id="modal_pag_entradaAvulsa_div_HorarioSaida">
							<label class="control-label" for="modal_pag_entradaAvulsa_input_HorarioSaida">Horario Saida <span class="text-danger">*</span></label>
							<input type="text" class="form-control timepicker" name="modal_pag_entradaAvulsa_input_HorarioSaida">
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divTempoTotal">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_TempoTotal">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_TempoTotal">Tempo Total <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_TempoTotal" readonly>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divValorHora">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_ValorHora">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_ValorHora">Valor da Hora <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_ValorHora" readonly>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divValorFracao">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_ValorFracao">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_ValorFracao">Valor da Fração <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_ValorFracao" readonly>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divTotal">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_Total">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_Total">Valor Total <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_Total" readonly>
					</div>
				</div>
				<div class="col-sm-4 col-md-4" id="modal_pag_entradaAvulsa_FormaPagamento">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_FormaPagamento">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_FormaPagamento">Forma de Pagamento <span class="text-danger">*</span></label>
						<select class="form-control" data-live-search="true" name="modal_pag_entradaAvulsa_input_FormaPagamento">
						</select>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divPago">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_Pago">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_Pago">Valor Pago <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_Pago">
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_pag_entradaAvulsa_div_divTroco">
					<div class="form-group" id="modal_pag_entradaAvulsa_div_Troco">
						<label class="control-label" for="modal_pag_entradaAvulsa_input_Troco">Valor do Troco <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_pag_entradaAvulsa_input_Troco" readonly>
					</div>
				</div>
				<div class="col-sm-6 col-md-4" id="modal_pag_entradaAvulsa_div_divAplicarHoraCheia">
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-12 text-center" id="modal_pag_entradaAvulsa_div_AplicarHoraCheia">
							<label class="control-label col-xs-12 col-sm-12 col-md-12" for="modal_pag_entradaAvulsa_input_AplicarHoraCheia">Aplicar Hora Cheia ?</label>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_AplicarHoraCheia" data-value="1" checked> Sim
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_AplicarHoraCheia" data-value="2"> Não
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4" id="modal_pag_entradaAvulsa_div_divAplicarFracao">
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-12 text-center" id="modal_pag_entradaAvulsa_div_AplicarFracao">
							<label class="control-label col-xs-12 col-sm-12 col-md-12" for="modal_pag_entradaAvulsa_input_AplicarFracao">Aplicar Fração ?</label>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_AplicarFracao" data-value="1"> Sim
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_AplicarFracao" data-value="2" checked> Não
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4" id="modal_pag_entradaAvulsa_div_divAplicarTolerancia">
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-12 text-center" id="modal_pag_entradaAvulsa_div_AplicarTolerancia">
							<label class="control-label col-xs-12 col-sm-12 col-md-12" for="modal_pag_entradaAvulsa_input_AplicarTolerancia">Aplicar Tolerância ?</label>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_AplicarTolerancia" data-value="1"> Sim
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px; padding-bottom: 10px;">
								<input type="radio" name="modal_pag_entradaAvulsa_input_AplicarTolerancia" data-value="2" checked> Não
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-pag_entradaAvulsa"><i class="fa fa-plus"></i> Lançar</button>
				</div>
			</div>
		</div>
	</div>
</form>