<form id="form_alt_Clientes">
	<input type="hidden" name="modal_alt_clientes_input_Codigo">
	<input type="hidden" name="modal_alt_clientes_input_NomeAnterior">
	<input type="hidden" name="modal_alt_clientes_input_CPFAnterior">
	<div id="modal_alt_Clientes" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-9 col-md-9" id="modal_alt_clientes_div_divNome">
					<div class="form-group" id="modal_alt_clientes_div_Nome">
						<label class="control-label" for="modal_alt_clientes_input_Nome">Nome <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_Nome">
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4" id="modal_alt_clientes_div_divDataNascimento">
					<div class="form-group" id="modal_alt_clientes_div_DataNascimento">
						<label class="control-label" for="modal_alt_clientes_input_DataNascimento">Data de Nascimento</label>
						<input type="date" class="form-control" name="modal_alt_clientes_input_DataNascimento">
					</div>
				</div>
				<div class="col-xs-6 col-sm-3 col-md-3" id="modal_alt_clientes_div_divRG">
					<div class="form-group" id="modal_alt_clientes_div_RG">
						<label class="control-label" for="modal_alt_clientes_input_RG">RG</label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_RG">
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4" id="modal_alt_clientes_CPF">
					<div class="form-group" id="modal_alt_clientes_div_CPF">
						<label class="control-label" for="modal_alt_clientes_input_CPF">CPF <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_CPF">
					</div>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8" id="modal_alt_clientes_div_divEmail">
					<div class="form-group" id="modal_alt_clientes_div_Email">
						<label class="control-label" for="modal_alt_clientes_input_Email">E-Mail</label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_Email">
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4" id="modal_alt_clientes_div_divTelefone">
					<div class="form-group" id="modal_alt_clientes_div_Telefone">
						<label class="control-label" for="modal_alt_clientes_input_Telefone">Telefone <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_Telefone">
					</div>
				</div>
				<div class="col-xs-6 col-sm-3 col-md-3" id="modal_alt_clientes_div_divCEP">
					<div class="form-group" id="modal_alt_clientes_div_CEP">
						<label class="control-label" for="modal_alt_clientes_input_CEP">CEP <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_CEP">
					</div>
				</div>
				<div class="col-xs-12 col-sm-5 col-md-5" id="modal_alt_clientes_div_divCidade">
					<div class="form-group" id="modal_alt_clientes_div_Cidade">
						<label class="control-label" for="modal_alt_clientes_input_Cidade">Cidade <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_Cidade">
					</div>
				</div>
				<div class="col-xs-3 col-sm-2 col-md-2" id="modal_alt_clientes_div_divUF">
					<div class="form-group" id="modal_alt_clientes_div_UF">
						<label class="control-label" for="modal_alt_clientes_input_UF">UF <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_UF">
					</div>
				</div>
				<div class="col-xs-9 col-sm-10 col-md-10" id="modal_alt_clientes_div_divBairro">
					<div class="form-group" id="modal_alt_clientes_div_Bairro">
						<label class="control-label" for="modal_alt_clientes_input_Bairro">Bairro <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_Bairro">
					</div>
				</div>
				<div class="col-xs-9 col-sm-10 col-md-10" id="modal_alt_clientes_div_divEndereco">
					<div class="form-group" id="modal_alt_clientes_div_Endereco">
						<label class="control-label" for="modal_alt_clientes_input_Endereco">Endereço <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_Endereco">
					</div>
				</div>
				<div class="col-xs-3 col-sm-2 col-md-2" id="modal_alt_clientes_div_divNumero">
					<div class="form-group" id="modal_alt_clientes_div_Numero">
						<label class="control-label" for="modal_alt_clientes_input_Numero">Nº <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_clientes_input_Numero">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-alt_clientes"><i class="fa fa-pencil"></i> Alterar</button>
				</div>
			</div>
		</div>
	</div>
</form>