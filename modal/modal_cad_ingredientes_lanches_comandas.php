<form id="form_cad_Ingredientes_Lanches_Comandas">
	<input type="hidden" name="modal_cad_ingredientes_lanches_comandas_input_Codigo">
	<div id="modal_cad_Ingredientes_Lanches_Comandas" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-8 col-md-8" id="modal_cad_ingredientes_lanches_comandas_div_divIngrediente">
					<div class="form-group" id="modal_cad_ingredientes_lanches_comandas_div_Ingrediente">
						<label class="control-label" for="modal_cad_ingredientes_lanches_comandas_input_Ingrediente">Ingrediente <span class="text-danger">*</span></label>
						<select class="form-control" name="modal_cad_ingredientes_lanches_comandas_input_Ingrediente">
						</select>
					</div>
				</div>
				<div class="col-sm-8 col-md-8" id="modal_cad_ingredientes_lanches_comandas_div_divQuantidade">
					<div class="form-group" id="modal_cad_ingredientes_lanches_comandas_div_Quantidade">
						<label class="control-label" for="modal_cad_ingredientes_lanches_comandas_input_Quantidade">Quantidade <span class="text-danger">*</span></label>
						<input class="form-control" name="modal_cad_ingredientes_lanches_comandas_input_Quantidade">
					</div>
				</div>
				<div class="col-sm-12 visible-xs visible-sm">
					<br />
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-cad_ingredientes_lanches_comandas"><i class="fa fa-plus"></i> Adicionar</button>
				</div>
			</div>
		</div>
	</div>
</form>