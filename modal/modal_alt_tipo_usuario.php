<form id="form_alt_TipoUsuario">
	<input type="hidden" name="modal_alt_tipoUsuario_input_Codigo" value="">
	<div id="modal_alt_TipoUsuario" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-4" id="modal_alt_tipoUsuario_div_divDescricao">
					<div class="form-group" id="modal_alt_tipoUsuario_div_Descricao">
						<label class="control-label" for="modal_alt_tipoUsuario_input_Descricao">Descrição <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_tipoUsuario_input_Descricao" onKeyDown="if (event.keyCode == '13'){ return false }">
					</div>
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-alt_tipoUsuario"><i class="fa fa-pencil"></i> Alterar</button>
				</div>
			</div>
		</div>
	</div>
</form>