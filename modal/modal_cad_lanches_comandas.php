<form id="form_cad_Lanches_Comandas">
	<input type="hidden" name="modal_cad_lanches_comandas_input_Codigo">
	<div id="modal_cad_Lanches_Comandas" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-12" id="modal_cad_lanches_comandas_div_divLanche">
					<div class="form-group" id="modal_cad_lanches_comandas_div_Lanche">
						<label class="control-label" for="modal_cad_lanches_comandas_input_Lanche">Lanche <span class="text-danger">*</span></label>
						<select class="form-control" name="modal_cad_lanches_comandas_input_Lanche">
						</select>
					</div>
				</div>
				<div class="col-sm-12 visible-xs visible-sm">
					<br />
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-cad_lanches_comandas"><i class="fa fa-plus"></i> Adicionar</button>
				</div>
			</div>
		</div>
	</div>
</form>