<form id="form_alt_EntradaAvulsa">
	<input type="hidden" name="modal_alt_entradaAvulsa_input_Codigo">
	<input type="hidden" name="modal_alt_entradaAvulsa_input_TipoVeiculoAnterior">
	<input type="hidden" name="modal_alt_entradaAvulsa_input_VagaPreferencialAnterior">
	<input type="hidden" name="modal_alt_entradaAvulsa_input_Mensalista">
	<input type="hidden" name="modal_alt_entradaAvulsa_input_MensalistaAnterior">
	<div id="modal_alt_EntradaAvulsa" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6 col-md-6" id="modal_alt_entradaAvulsa_MensalistaNome" style="display: none;">
					<div class="form-group" id="modal_alt_entradaAvulsa_div_MensalistaNome">
						<label class="control-label" for="modal_alt_entradaAvulsa_input_MensalistaNome">Mensalista <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_entradaAvulsa_input_MensalistaNome" disabled>
					</div>
				</div>
				<div class="col-sm-12 col-md-5" id="modal_alt_entradaAvulsa_div_divDescricao">
					<div class="form-group" id="modal_alt_entradaAvulsa_div_Descricao">
						<label class="control-label" for="modal_alt_entradaAvulsa_input_Descricao">Descricao</label>
						<input type="text" class="form-control" name="modal_alt_entradaAvulsa_input_Descricao">
					</div>
				</div>
				<div class="col-sm-12 col-md-3" id="modal_alt_entradaAvulsa_div_divPlaca">
					<div class="form-group" id="modal_alt_entradaAvulsa_div_Placa">
						<label class="control-label" for="modal_alt_entradaAvulsa_input_Placa">Placa <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_entradaAvulsa_input_Placa" style="text-transform: uppercase;">
					</div>
				</div>
				<div class="col-sm-4 col-md-4" id="modal_alt_entradaAvulsa_TipoVeiculo">
					<div class="form-group" id="modal_alt_entradaAvulsa_div_TipoVeiculo">
						<label class="control-label" for="modal_alt_entradaAvulsa_input_TipoVeiculo">Tipo de Veiculo <span class="text-danger">*</span></label>
						<select class="form-control" data-live-search="true" name="modal_alt_entradaAvulsa_input_TipoVeiculo">
						</select>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_alt_entradaAvulsa_div_divDataEntrada">
					<div class="form-group" id="modal_alt_entradaAvulsa_div_DataEntrada">
						<label class="control-label" for="modal_alt_entradaAvulsa_input_DataEntrada">Data Entrada <span class="text-danger">*</span></label>
						<input type="date" class="form-control" name="modal_alt_entradaAvulsa_input_DataEntrada">
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_alt_entradaAvulsa_div_divHorarioEntrada">
					<div class="bootstrap-timepicker">
						<div class="form-group" id="modal_alt_entradaAvulsa_div_HorarioEntrada">
							<label class="control-label" for="modal_alt_entradaAvulsa_input_HorarioEntrada">Horario Entrada <span class="text-danger">*</span></label>
							<input type="text" class="form-control timepicker" name="modal_alt_entradaAvulsa_input_HorarioEntrada">
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4" id="modal_alt_entradaAvulsa_div_divVagaPreferencial">
					<div class="form-group" id="modal_alt_entradaAvulsa_div_VagaPreferencial">
						<div class="row">
							<div class="col-md-12">
								<label class="control-label" for="modal_alt_entradaAvulsa_input_VagaPreferencial"><span class="btn btn-xs btn-primary"><i class="fa fa-wheelchair"></i></span> Vaga Preferêncial?</label>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px;padding-bottom: 10px;">
								<input type="radio" name="modal_alt_entradaAvulsa_input_VagaPreferencial" data-value="1"> Sim
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-center" style="padding-top: 3px;padding-bottom: 10px;">
								<input type="radio" name="modal_alt_entradaAvulsa_input_VagaPreferencial" data-value="2" checked> Não
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-alt_entradaAvulsa"><i class="fa fa-pencil"></i> Alterar</button>
				</div>
			</div>
		</div>
	</div>
</form>