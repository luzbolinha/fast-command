<form id="form_alt_Lanches">
	<input type="hidden" name="modal_alt_lanches_input_Codigo">
	<input type="hidden" name="modal_alt_lanches_input_NomeAnterior">
	<div id="modal_alt_Lanches" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-12" id="modal_alt_lanches_Nome">
					<div class="form-group" id="modal_alt_lanches_div_Nome">
						<label class="control-label" for="modal_alt_lanches_input_Nome">Nome <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_lanches_input_Nome">
					</div>
				</div>
				<div class="col-sm-12 col-md-12" id="modal_alt_lanches_div_divDescricao">
					<div class="form-group" id="modal_alt_lanches_div_Descricao">
						<label class="control-label" for="modal_alt_lanches_input_Descricao">Descricao</label>
						<input type="text" class="form-control" name="modal_alt_lanches_input_Descricao">
					</div>
				</div>
				<div class="col-sm-6 col-md-6" id="modal_alt_lanches_div_divValorVenda">
					<div class="form-group" id="modal_alt_lanches_div_ValorVenda">
						<label class="control-label" for="modal_alt_lanches_input_ValorVenda">Valor de Venda <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_alt_lanches_input_ValorVenda">
					</div>
				</div>
				<div class="col-sm-6 col-md-6" id="modal_alt_lanches_div_divValorProducao">
					<div class="form-group" id="modal_alt_lanches_div_ValorProducao">
						<label class="control-label" for="modal_alt_lanches_input_ValorProducao">Valor de Produção</label>
						<input type="text" class="form-control" name="modal_alt_lanches_input_ValorProducao">
					</div>
				</div>
				<div class="col-sm-12 visible-xs visible-sm">
					<br />
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-alt_lanches"><i class="fa fa-plus"></i> Alterar</button>
				</div>
			</div>
		</div>
	</div>
</form>