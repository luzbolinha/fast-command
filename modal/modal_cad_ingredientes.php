<form id="form_cad_Ingredientes">
	<div id="modal_cad_Ingredientes" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-12" id="modal_cad_ingredientes_Nome">
					<div class="form-group" id="modal_cad_ingredientes_div_Nome">
						<label class="control-label" for="modal_cad_ingredientes_input_Nome">Nome <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_cad_ingredientes_input_Nome">
					</div>
				</div>
				<div class="col-sm-12 col-md-12" id="modal_cad_ingredientes_div_divDescricao">
					<div class="form-group" id="modal_cad_ingredientes_div_Descricao">
						<label class="control-label" for="modal_cad_ingredientes_input_Descricao">Descricao</label>
						<input type="text" class="form-control" name="modal_cad_ingredientes_input_Descricao">
					</div>
				</div>
				<div class="col-sm-6 col-md-6" id="modal_cad_ingredientes_div_divValorVenda">
					<div class="form-group" id="modal_cad_ingredientes_div_ValorVenda">
						<label class="control-label" for="modal_cad_ingredientes_input_ValorVenda">Valor de Venda <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_cad_ingredientes_input_ValorVenda">
					</div>
				</div>
				<div class="col-sm-6 col-md-6" id="modal_cad_ingredientes_div_divValorCompra">
					<div class="form-group" id="modal_cad_ingredientes_div_ValorCompra">
						<label class="control-label" for="modal_cad_ingredientes_input_ValorCompra">Valor de Compra</label>
						<input type="text" class="form-control" name="modal_cad_ingredientes_input_ValorCompra">
					</div>
				</div>
				<div class="col-sm-12 visible-xs visible-sm">
					<br />
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-cad_ingredientes"><i class="fa fa-plus"></i> Cadastrar</button>
				</div>
			</div>
		</div>
	</div>
</form>