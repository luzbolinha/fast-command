<form id="form_alt_Usuario" enctype="multipart/form-data">
  <input type="hidden" name="modal_alt_usuario_input_Codigo">
  <input type="hidden" name="modal_alt_usuario_input_NomeAnterior">
  <input type="hidden" name="modal_alt_usuario_input_EmailAnterior">
  <div id="modal_alt_Usuario" style="display: none;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-md-6">
          <div class="row">
            <div class="col-xs-12 col-md-12 text-center" id="modal_alt_usuario_div_PreVisualizacaoImagemPerfil" style="padding-top: 10px;padding-bottom: 10px;">
              <img src="images/default-user.png" width="280px" height="280px" id="modal_alt_usuario_PreVisualizacaoImagemPerfil" class="img-fluid" style="border: 2px solid #DDD;border-radius: 100%;">
            </div>
            <div class="col-xs-12 col-md-12" id="modal_alt_usuario_ImagemPerfil">
              <div class="form-group" id="modal_alt_usuario_div_ImagemPerfil">
                <label class="control-label" for="modal_alt_usuario_input_ImagemPerfil">Imagem de Perfil (280 x 280)</label>
                <input type="file" class="form-control" name="modal_alt_usuario_input_ImagemPerfil">
                <label class="control-label pull-right label label-default" for="modal_alt_usuario_input_ImagemPerfil">Tamanho Máximo: <span id="modal_alt_usuario_input_ImagemPerfil_sizelabel">10MB</span></label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-6">

          <div class="row">
            <div class="col-sm-12 col-md-12" id="modal_alt_usuario_Nome">
              <div class="form-group" id="modal_alt_usuario_div_Nome">
                <label class="control-label" for="modal_alt_usuario_input_Nome">Nome Completo <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="modal_alt_usuario_input_Nome">
              </div>
            </div>
            <div class="col-sm-12 col-md-12" id="modal_alt_usuario_Email">
              <div class="form-group" id="modal_alt_usuario_div_Email">
                <label class="control-label" for="modal_alt_usuario_input_Email">E-Mail <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="modal_alt_usuario_input_Email">
              </div>
            </div>
            <div class="col-sm-12 col-md-12" id="modal_alt_usuario_TipoUsuario">
              <div class="form-group" id="modal_alt_usuario_div_TipoUsuario">
                <label class="control-label" for="modal_alt_usuario_input_TipoUsuario">Tipo de Usuário <span class="text-danger">*</span></label>
                <select class="form-control" data-live-search="true" name="modal_alt_usuario_input_TipoUsuario">
                </select>
              </div>
            </div>
            <div class="col-sm-12 col-md-12" id="modal_alt_usuario_Senha">
              <div class="form-group" id="modal_alt_usuario_div_Senha">
                <label class="control-label" for="modal_alt_usuario_input_Senha">Senha <span class="text-danger">*</span></label>
                <input type="password" class="form-control" name="modal_alt_usuario_input_Senha">
              </div>
            </div>
            <div class="col-sm-12 col-md-12" id="modal_alt_usuario_ConfirmaSenha">
              <div class="form-group" id="modal_alt_usuario_div_ConfirmaSenha">
                <label class="control-label" for="modal_alt_usuario_input_ConfirmaSenha">Confirme a Senha <span class="text-danger">*</span></label>
                <input type="password" class="form-control" name="modal_alt_usuario_input_ConfirmaSenha">
              </div>
            </div>
          </div>

        </div>
        <div class="col-xs-12 col-sm-12 visible-xs visible-sm">
          <br />
        </div>
        <div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
          <button type="button" class="btn btn-primary" id="btn-modal-alt_usuario"><i class="fa fa-pencil"></i> Alterar</button>
        </div>
      </div>
    </div>
  </div>
</form>