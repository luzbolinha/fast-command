<form id="form_cad_Comandas">
	<input type="hidden" name="modal_cad_comandas_input_Codigo">
	<input type="hidden" name="modal_cad_comandas_input_ClienteAnterior">
	<div id="modal_cad_Comandas" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-12" id="modal_cad_comandas_Cliente">
					<div class="form-group" id="modal_cad_comandas_div_Cliente">
						<label class="control-label" for="modal_cad_comandas_input_Cliente">Cliente <span class="text-danger">*</span></label>
						<select class="form-control" name="modal_cad_comandas_input_Cliente">
						</select>
					</div>
				</div>
				<div class="col-sm-12 col-md-12" id="modal_cad_comandas_div_divItensComanda">
					<div class="col-sm-4"></div>
					<button type="button" class="col-sm-4 btn btn-info" id="btn-modal-cad_comandas-add-lanche"><i class="fa fa-plus"></i> Adicionar Lanche</button>
					<div class="col-sm-4"></div>
				</div>
				<div class="col-sm-12 col-md-12" style="padding-top: 10px;" id="modal_cad_comandas_div_divItensComanda">
					<table id="modal_cad_comandas_tabItensComanda" class="table table-hover table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center">Lanche</th>
                          <th class="text-center">Adicionais</th>
                          <th class="text-center">Total</th>
                          <th class="text-center">Opções</th>
                        </tr>
                      </thead>
                      <tbody id="modal_cad_comandas_registrosItensComanda">
                      </tbody>
                      <tfoot>
                        <tr>
                          <th class="text-center">Lanche</th>
                          <th class="text-center">Adicionais</th>
                          <th class="text-center">Total</th>
                          <th class="text-center">Opções</th>
                        </tr>
                      </tfoot>
                    </table>
				</div>
				<div class="col-sm-12 col-md-12" id="modal_cad_comandas_div_divPromocoes">
					<table id="modal_cad_comandas_tabPromocoes" class="table table-hover table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center">Promoção</th>
                          <th class="text-center">%</th>
                          <th class="text-center">Desconto</th>
                        </tr>
                      </thead>
                      <tbody id="modal_cad_comandas_registrosPromocoes">
                      </tbody>
                      <tfoot>
                        <tr>
                          <th class="text-center">Promoção</th>
                          <th class="text-center">%</th>
                          <th class="text-center">Desconto</th>
                        </tr>
                      </tfoot>
                    </table>
				</div>
				<div class="col-sm-4 col-md-4" id="modal_cad_comandas_div_divTotal">
					<div class="form-group" id="modal_cad_comandas_div_Total">
						<label class="control-label" for="modal_cad_comandas_input_Total">Total <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_cad_comandas_input_Total" disabled>
					</div>
				</div>
				<div class="col-sm-4 col-md-4" id="modal_cad_comandas_div_divPago">
					<div class="form-group" id="modal_cad_comandas_div_Pago">
						<label class="control-label" for="modal_cad_comandas_input_Pago">Pago</label>
						<input type="text" class="form-control" name="modal_cad_comandas_input_Pago" disabled>
					</div>
				</div>
				<div class="col-sm-4 col-md-4" id="modal_cad_comandas_div_divTroco">
					<div class="form-group" id="modal_cad_comandas_div_Troco">
						<label class="control-label" for="modal_cad_comandas_input_Troco">Troco</label>
						<input type="text" class="form-control" name="modal_cad_comandas_input_Troco" disabled>
					</div>
				</div>
				<div class="col-sm-12 visible-xs visible-sm">
					<br />
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-cad_comandas"><i class="fa fa-plus"></i> Cadastrar</button>
				</div>
			</div>
		</div>
	</div>
</form>