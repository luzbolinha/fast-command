<form id="form_cad_TipoUsuario">
	<div id="modal_cad_TipoUsuario" style="display: none;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-12" id="modal_cad_tipoUsuario_div_divDescricao">
					<div class="form-group" id="modal_cad_tipoUsuario_div_Descricao">
						<label class="control-label" for="modal_cad_tipoUsuario_input_Descricao">Descrição <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="modal_cad_tipoUsuario_input_Descricao" onKeyDown="if (event.keyCode == '13'){ return false }">
					</div>
				</div>
				<div class="col-sm-12 col-md-12" style="padding-bottom: 10px;" align="center">
					<button type="button" class="btn btn-primary" id="btn-modal-cad_tipoUsuario"><i class="fa fa-plus"></i> Cadastrar</button>
				</div>
			</div>
		</div>
	</div>
</form>