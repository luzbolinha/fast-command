//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                  JavaScript da Página : [PATH]/cardapio.php		   	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerar o cache do Estacionamento

		function geraCache(){
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

				var Lanches = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 100,
			    	registrosPagina : 0,
			    	totalRegistros : 0,
			    	filtros : {
			    		estruturaPesquisa : "",
			    		estruturaCampos : {}
			    	}
			    }

				var Ingredientes = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 100,
			    	registrosPagina : 0,
			    	totalRegistros : 0,
			    	filtros : {
			    		estruturaPesquisa : "",
			    		estruturaCampos : {}
			    	}
				}

			// Grava no localStorage

				localStorage.setItem("Lanches", JSON.stringify(Lanches));
				localStorage.setItem("Ingredientes", JSON.stringify(Ingredientes));
		}

		geraCache();

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Lanches

		function CarregaLanches(){

			// Define a Promise

				return new Promise((resolve, reject)=>{
			
					// Verificação de Permissão de Modulo

						// if(AcessoModulo(Sessao.Permissoes) == false){
						// 	return false;
						// }

					// Verifica se ja existe cache

						var cache = JSON.parse(localStorage.getItem("Lanches"));

					// Declaração das Variaveis

						var Filtros = cache.filtros.estruturaCampos;
						var RegistrosPorPaginas = 100;
						var PaginaRegistro = 0;
						if(cache == null){
							PaginaRegistro = 0;
						} else if(cache.hasOwnProperty("paginaAtual")){
							if(cache.paginaAtual > 0){
								PaginaRegistro = (cache.paginaAtual * RegistrosPorPaginas) - 100;
							} else {
								PaginaRegistro = 0;
							}
						}

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'filtros' : (Object.keys(Filtros)).length == 0 ? "" : Filtros,
								'registros_paginas' : RegistrosPorPaginas,
								'pagina_registro' : PaginaRegistro
							},
							url: 'api/lanches',
							beforeSend: function(data){
								swal({
									title: 'Carregando Lanches',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								}).then((data)=>{
									reject();
								})
							},		
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return reject();
									}
								
								// Declara as Variaveis
								
									var TotalLanches = parseInt(retorno.total_lanches);
									var PonteiroLanches = 0;
									var Codigo = 0;
									var Nome = 0;
									var Descricao = 0;
									var ValorVenda = 0;
									var ValorProducao = 0;
									var Status = 0;
									var LinhaHTML = 0;
									var AntigoHTML = 0;
									var TotalRegistrosPagina = retorno.lanches.length;
									var data_lanches = 0;
								
								// Remove todas as Pessoas Fisicas
								
									$("#registrosLanches").html("");
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroLanches=0;PonteiroLanches<TotalRegistrosPagina;PonteiroLanches++){
									
										Codigo = retorno.lanches[PonteiroLanches].codigo;
										Nome = retorno.lanches[PonteiroLanches].nome;
										Descricao = retorno.lanches[PonteiroLanches].descricao;
										ValorVenda = (retorno.lanches[PonteiroLanches].valor_venda).replace(/\,/g, "").replace(/\./g, ",");
										ValorProducao = (retorno.lanches[PonteiroLanches].valor_producao).replace(/\,/g, "").replace(/\./g, ",");
										Status = parseInt(retorno.lanches[PonteiroLanches].cod_status);

										LinhaHTML = "<tr>"
										LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
										LinhaHTML += "<td class='text-center'>" + Nome + "</td>"
										LinhaHTML += "<td class='text-center'>" + Descricao + "</td>"
										LinhaHTML += "<td class='text-center'> R$ " + ValorProducao + "</td>"
										LinhaHTML += "<td class='text-center'> R$ " + ValorVenda + "</td>"
										LinhaHTML += "<td class='text-center'>"
										if(Status == 1){
											LinhaHTML += "<label class='label label-success'>Ativo</label>";
										} else {
											LinhaHTML += "<label class='label label-default'>Inativo</label>";
										}
										LinhaHTML += "</td>"
										LinhaHTML += "<td class='text-center'>"
										LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-lanches' data-lanches='"+JSON.stringify(retorno.lanches[PonteiroLanches])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
										LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-lanches' data-lanches='"+JSON.stringify(retorno.lanches[PonteiroLanches])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
										if(Status == 1){
											LinhaHTML += " | <button type='button' class='btn btn-xs btn-default' data-toggle='tooltip' id='btn-desativar-lanches' data-lanches='"+JSON.stringify(retorno.lanches[PonteiroLanches])+"' data-placement='left' title='Desativar'><i class='fa fa-thumbs-o-down'></i></button>"
										} else {
											LinhaHTML += " | <button type='button' class='btn btn-xs btn-success' data-toggle='tooltip' id='btn-ativar-lanches' data-lanches='"+JSON.stringify(retorno.lanches[PonteiroLanches])+"' data-placement='left' title='Ativar'><i class='fa fa-thumbs-o-up'></i></button>"
										}
										LinhaHTML += "</td>"
										LinhaHTML += "</tr>"
										
										AntigoHTML = $("#registrosLanches").html();
										$("#registrosLanches").html(AntigoHTML+LinhaHTML);
										
									}
									
								// Faz o calculo de total de registros
									
									$("#badge-TotalRegistrosLanches").html(TotalLanches);

								// Seta o valor total de Lanches

									cache.totalRegistros = TotalLanches;
									cache.registrosPagina = TotalRegistrosPagina;

								// Grava no LocalStorage

			        				localStorage.setItem("Lanches", JSON.stringify(cache));

			        			// Gera a Paginacao

									geraEstruturaPaginacao("#pgn-lanches", "Lanches")

								// Libera a tela
							
									swal.close();

								// Retorna o resolve

									resolve();

							}
						});

				})

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Ingredientes

		function CarregaIngredientes(){

			// Define a Promise

				return new Promise((resolve, reject)=>{
			
					// Verificação de Permissão de Modulo

						// if(AcessoModulo(Sessao.Permissoes) == false){
						// 	return false;
						// }

					// Verifica se ja existe cache

						var cache = JSON.parse(localStorage.getItem("Lanches"));

					// Declaração das Variaveis

						var Filtros = cache.filtros.estruturaCampos;
						var RegistrosPorPaginas = 100;
						var PaginaRegistro = 0;
						if(cache == null){
							PaginaRegistro = 0;
						} else if(cache.hasOwnProperty("paginaAtual")){
							if(cache.paginaAtual > 0){
								PaginaRegistro = (cache.paginaAtual * RegistrosPorPaginas) - 100;
							} else {
								PaginaRegistro = 0;
							}
						}

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'filtros' : (Object.keys(Filtros)).length == 0 ? "" : Filtros,
								'registros_paginas' : RegistrosPorPaginas,
								'pagina_registro' : PaginaRegistro
							},
							url: 'api/ingredientes',
							beforeSend: function(data){
								swal({
									title: 'Carregando Ingredientes',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								}).then((data)=>{
									reject();
								})
							},		
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return reject();
									}
								
								// Declara as Variaveis
								
									var TotalIngredientes = parseInt(retorno.total_ingredientes);
									var PonteiroIngredientes = 0;
									var Codigo = 0;
									var Nome = 0;
									var Descricao = 0;
									var ValorVenda = 0;
									var ValorCompra = 0;
									var Adicional = 0;
									var Quantidade = 0;
									var Status = 0;
									var LinhaHTML = 0;
									var AntigoHTML = 0;
									var TotalRegistrosPagina = retorno.ingredientes.length;
									var data_ingredientes = 0;
								
								// Remove todas as Pessoas Fisicas
								
									$("#registrosIngredientes").html("");
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroIngredientes=0;PonteiroIngredientes<TotalRegistrosPagina;PonteiroIngredientes++){
									
										Codigo = retorno.ingredientes[PonteiroIngredientes].codigo;
										Nome = retorno.ingredientes[PonteiroIngredientes].nome;
										Descricao = retorno.ingredientes[PonteiroIngredientes].descricao;
										ValorVenda = (retorno.ingredientes[PonteiroIngredientes].valor_venda).replace(/\,/g, "").replace(/\./g, ",");
										ValorCompra = retorno.ingredientes[PonteiroIngredientes].valor_compra != null ? (retorno.ingredientes[PonteiroIngredientes].valor_compra).replace(/\,/g, "").replace(/\./g, ",") : "0,00";
										Status = parseInt(retorno.ingredientes[PonteiroIngredientes].cod_status);
										HabilitarAdicional = parseInt(retorno.ingredientes[PonteiroIngredientes].habilitar_adicional);

										LinhaHTML = "<tr>"
										LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
										LinhaHTML += "<td class='text-center'>" + Nome + "</td>"
										LinhaHTML += "<td class='text-center'>" + Descricao + "</td>"
										LinhaHTML += "<td class='text-center'> R$ " + ValorCompra + "</td>"
										LinhaHTML += "<td class='text-center'> R$ " + ValorVenda + "</td>"
										LinhaHTML += "<td class='text-center'>"
										if(HabilitarAdicional == 1){
											LinhaHTML += "<label class='label label-success'>Sim</label>";
										} else {
											LinhaHTML += "<label class='label label-default'>Não</label>";
										}
										LinhaHTML += "</td>"
										LinhaHTML += "<td class='text-center'>"
										if(Status == 1){
											LinhaHTML += "<label class='label label-success'>Ativo</label>";
										} else {
											LinhaHTML += "<label class='label label-default'>Inativo</label>";
										}
										LinhaHTML += "</td>"
										LinhaHTML += "<td class='text-center'>"
										LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-ingredientes' data-ingredientes='"+JSON.stringify(retorno.ingredientes[PonteiroIngredientes])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
										LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-ingredientes' data-ingredientes='"+JSON.stringify(retorno.ingredientes[PonteiroIngredientes])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
										if(Status == 1){
											LinhaHTML += " | <button type='button' class='btn btn-xs btn-default' data-toggle='tooltip' id='btn-desativar-ingredientes' data-ingredientes='"+JSON.stringify(retorno.ingredientes[PonteiroIngredientes])+"' data-placement='left' title='Desativar'><i class='fa fa-thumbs-o-down'></i></button>"
										} else {
											LinhaHTML += " | <button type='button' class='btn btn-xs btn-success' data-toggle='tooltip' id='btn-ativar-ingredientes' data-ingredientes='"+JSON.stringify(retorno.ingredientes[PonteiroIngredientes])+"' data-placement='left' title='Ativar'><i class='fa fa-thumbs-o-up'></i></button>"
										}
										LinhaHTML += "</td>"
										LinhaHTML += "</tr>"
										
										AntigoHTML = $("#registrosIngredientes").html();
										$("#registrosIngredientes").html(AntigoHTML+LinhaHTML);
										
									}
									
								// Faz o calculo de total de registros
									
									$("#badge-TotalRegistrosIngredientes").html(TotalIngredientes);

								// Seta o valor total de Ingredientes

									cache.totalRegistros = TotalIngredientes;
									cache.registrosPagina = TotalRegistrosPagina;

								// Grava no LocalStorage

			        				localStorage.setItem("Ingredientes", JSON.stringify(cache));

			        			// Gera a Paginacao

									geraEstruturaPaginacao("#pgn-ingredientes", "Ingredientes")

								// Libera a tela
							
									swal.close();

								// Retorna o resolve

									resolve();

							}
						});

				})

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao finalizar o carregamento da pagina

		$(document).ready(()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

		    // Executa as Promises de Configurações

		    	CarregaLanches().then((data)=>{
		    		CarregaIngredientes()
		    	});

		    // Gerencia o Menu

	        	// gerenciaMenu();

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao fechar ou ir para outra pagina

		$(window).on('beforeunload', function(){
		   localStorage.clear();
		});	

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});