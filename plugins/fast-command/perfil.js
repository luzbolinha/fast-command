//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                   	jQuery da Página : [PATH]/home.asp		    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao finalizar o carregamento da pagina

		$(document).ready(()=>{

			// Renderiza as Modals

				renderizaModal("#modal_perfil", "<span class='text-muted'><i class='fa fa-pencil'></i> Perfil</span>");

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o procedimento de Login

		$(document).on("click", "#btn-perfil, #nav-ul-ImagemPerfil, #menu-ImagemPerfil", ()=>{

			// Declaração das Variaveis

				var Funcao = 4;

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/tipoUsuario_ajax.asp',
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}

						// Limpa o Formulario

							limpaFormulario("#form_perfil");

						// Trata o select modal_perfil_input_TipoUsuario

							$("select[name='modal_perfil_input_TipoUsuario']").empty()
							$.each(retorno.TipoUsuario, (i, item)=>{
								$("select[name='modal_perfil_input_TipoUsuario']").append($('<option>', {
									value: item.Codigo,
									text: item.Descricao
								}));
							});

						// Seta os valores dos Campos

							$("input[name='modal_perfil_input_Codigo']").val(Sessao.Usuario);
							$("input[name='modal_perfil_input_NomeAnterior']").val(Sessao.Nome);
							$("input[name='modal_perfil_input_EmailAnterior']").val(Sessao.Email);
							$("input[name='modal_perfil_input_Nome']").val(Sessao.Nome);
							$("input[name='modal_perfil_input_Email']").val(Sessao.Email);
							$("select[name='modal_perfil_input_TipoUsuario']").val(Sessao.TipoUsuario);

						// Limpa as Classes de Erros

							$("#modal_perfil_div_ImagemPerfil").removeClass("has-error");
							$("#modal_perfil_div_Nome").removeClass("has-error");
							$("#modal_perfil_div_Email").removeClass("has-error");
							$("#modal_perfil_div_TipoUsuario").removeClass("has-error");
							$("#modal_perfil_div_Senha").removeClass("has-error");
							$("#modal_perfil_div_Senha").removeClass("has-warning");
							$("#modal_perfil_div_ConfirmaSenha").removeClass("has-error");

						// Define o Tamanho Maximo de Arquivo na Label

							$("#modal_perfil_input_ImagemPerfil_sizelabel").html(upload.MaxSize+" MB")

						// Verifica a imagem do usuário para gerar a pré visualização

							if(Sessao.ImagemPerfil != undefined && Sessao.ImagemPerfil != ""){
								$("#modal_perfil_PreVisualizacaoImagemPerfil").prop("src", "uploads/imagens/"+Sessao.ImagemPerfil);
							} else {
								$("#modal_perfil_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
							}

						// Abre a Modal

							abriraModal("#modal_perfil");

					}
				});

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_perfil_input_ImagemPerfil

		$(document).on('change', "input[name='modal_perfil_input_ImagemPerfil']", (event)=>{

			// Verifica o Status da Modal

				var statusModal = $("#modal_perfil").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ImagemPerfil = $("input[name='modal_perfil_input_ImagemPerfil']");

					// Carrega a Imagem e retorna Parametros

						var CarregaImagem = readFile(this.activeElement, "#modal_perfil_PreVisualizacaoImagemPerfil");

					// Verifica se a array Retornou Erro

						if(CarregaImagem.hasOwnProperty("error")){
							$("#modal_perfil_div_ImagemPerfil").addClass("has-error");
							toastr.error(CarregaImagem.error);
							ImagemPerfil.val("");
							$("#modal_perfil_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
						} else {
							$("#modal_perfil_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
							$("#modal_perfil_div_ImagemPerfil").removeClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_perfil_input_Nome

		$(document).on('change keyup', "input[name='modal_perfil_input_Nome']", ()=>{

			// Verifica o Status da Modal

				var statusModal = $("#modal_perfil").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_perfil_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_perfil_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome Completo do Usuário precisa ser preenchido', 'Ops!');
							$("#modal_perfil_div_Nome").addClass("has-error");
						}

				}

		})


//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_perfil_input_Email

		$(document).on('change', "input[name='modal_perfil_input_Email']", ()=>{

			// Verifica o Status da Modal

				var statusModal = $("#modal_perfil").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Email = $("input[name='modal_perfil_input_Email']");
						var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;

					// Realiza a Validação

						if((Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val())){
							$("#modal_perfil_div_Email").removeClass("has-error");
						} else {
							toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
							$("#modal_perfil_div_Email").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_perfil_input_TipoUsuario

		$(document).on('change', "select[name='modal_perfil_input_TipoUsuario']", ()=>{

			// Verifica o Status da Modal

				var statusModal = $("#modal_perfil").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var TipoUsuario = $("select[name='modal_perfil_input_TipoUsuario']");

					// Realiza a Validação

						if(TipoUsuario.val() != undefined && TipoUsuario.val() != null && TipoUsuario.val() != "" && TipoUsuario.val() != " "){
							$("#modal_perfil_div_TipoUsuario").removeClass("has-error");
						} else {
							toastr.error('O Tipo de Usuário precisa ser preenchido', 'Ops!');
							$("#modal_perfil_div_TipoUsuario").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_perfil_input_ConfirmaSenha

		$(document).on('change', "input[name='modal_perfil_input_ConfirmaSenha']", ()=>{

			// Verifica o Status da Modal

				var statusModal = $("#modal_perfil").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Senha = $("input[name='modal_perfil_input_Senha']");
						var ConfirmaSenha = $("input[name='modal_perfil_input_ConfirmaSenha']");

					// Realiza a Validação

						if(ConfirmaSenha.val() == Senha.val()){
							$("#modal_perfil_div_Senha").removeClass("has-warning");
							$("#modal_perfil_div_ConfirmaSenha").removeClass("has-error");
						} else {
							toastr.error('O Campo Confirme a Senha deve ser igual ao campo Senha', 'Ops!');
							$("#modal_perfil_div_Senha").addClass("has-warning");
							$("#modal_perfil_div_ConfirmaSenha").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o Processo de Inserção

		$(document).on("click", "#btn-modal-perfil", ()=>{

			// Declara as variaveis

				var Codigo = $("input[name='modal_perfil_input_Codigo']");
				var ImagemPerfil = $("input[name='modal_perfil_input_ImagemPerfil']");
				var NomeAnterior = $("input[name='modal_perfil_input_NomeAnterior']");
				var EmailAnterior = $("input[name='modal_perfil_input_EmailAnterior']");
				var Nome = $("input[name='modal_perfil_input_Nome']");
				var Email = $("input[name='modal_perfil_input_Email']");
				var TipoUsuario = $("select[name='modal_perfil_input_TipoUsuario']");
				var Senha = $("input[name='modal_perfil_input_Senha']");
				var ConfirmaSenha = $("input[name='modal_perfil_input_ConfirmaSenha']");
				var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
				var BotaoAltUsuario = $("#btn-modal-perfil");
				var Funcao = 2;

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = (ImagemPerfil.val() != undefined && ImagemPerfil.val() != null && ImagemPerfil.val() != "" && ImagemPerfil.val() != " ") ? (((ImagemPerfil.get(0).files[0].size / 1024) / 1024) <= upload.MaxSize && upload.Types.indexOf(ImagemPerfil.get(0).files[0].name.split('.').pop().toLowerCase()) > -1) : true;
					validacao[1] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[2] = (Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val());
					validacao[3] = TipoUsuario.val() != undefined && TipoUsuario.val() != null && TipoUsuario.val() != "" && TipoUsuario.val() != " ";
					validacao[4] = ConfirmaSenha.val() == Senha.val();

				var verificaValidacao = validacao[0] && validacao[1] && validacao[2] && validacao[3] && validacao[4];

			// Desativa o botão

				BotaoAltUsuario.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					// Define a estrutura do form

						if(ImagemPerfil.val() != undefined && ImagemPerfil.val() != null && ImagemPerfil.val() != "" && ImagemPerfil.val() != " "){
							var form = new FormData();
								form.append('ImagemPerfilAlias', ImagemPerfil.get(0).files[0])
						}

						var URL = 'ajax/usuarios_ajax.asp?Codigo='+Codigo.val()+'&NomeAnterior='+NomeAnterior.val()+'&EmailAnterior='+EmailAnterior.val()+'&Nome='+Nome.val()+'&Email='+Email.val()+'&TipoUsuario='+TipoUsuario.val()+'&Senha='+Senha.val()+'&Status='+0+'&Funcao='+Funcao+'&ImagemPerfil='+ImagemPerfil.val();

					// Envia o Ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: form,
					        cache: false,
					        processData: false,
					        contentType: false,
					        headers: {
					        	'Content-Type' : undefined
					        },
							url: URL,
							beforeSend: function(data){
								data.setRequestHeader("Authorization", Sessao.Token);
								swal({
									title: 'Alterando seus dados',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
								BotaoAltUsuario.prop("disabled", false);
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									}).then(()=>{

										// Fecha a Modal

											fecharModal("#modal_perfil");

										// Ativa o botão modal-tipoVeiculo

											BotaoAltUsuario.prop("disabled", false);
									
									})
							}
						});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							toastr.error('Verifique o campo Imagem Perfil', 'Ops!');
							$("#modal_perfil_div_ImagemPerfil").addClass("has-error");
						}

						if(!validacao[1]){
							toastr.error('O Nome Completo do Usuário precisa ser preenchido', 'Ops!');
							$("#modal_perfil_div_Nome").addClass("has-error");
						}

						if(!validacao[2]){
							toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
							$("#modal_perfil_div_Email").addClass("has-error");
						}

						if(!validacao[3]){
							toastr.error('O Tipo de Usuário precisa ser preenchido', 'Ops!');
							$("#modal_perfil_div_TipoUsuario").addClass("has-error");
						}

						if(!validacao[4]){
							if(ConfirmaSenha.val() != Senha.val()){
								toastr.error('O Campo Confirme a Senha deve ser igual ao campo Senha', 'Ops!');
								$("#modal_perfil_div_Senha").addClass("has-warning");
								$("#modal_perfil_div_ConfirmaSenha").addClass("has-error");
							} else {
								toastr.error('O Campo Confirme a Senha precisa ser preenchido', 'Ops!');
								$("#modal_perfil_div_ConfirmaSenha").addClass("has-error");
							}
						}
									
					// Ativa o Botão
					
						BotaoAltUsuario.prop("disabled", false);

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});