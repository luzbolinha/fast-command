//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                  JavaScript da Página : [PATH]/cardapio.php		   	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao finalizar o carregamento da pagina

		$(document).ready(()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Renderiza as Modals
				
				renderizaModal("#modal_cad_Lanches", "<span class='text-muted'><i class='fa fa-plus'></i> Adicionar Novo Lanche</span>");
				renderizaModal("#modal_alt_Lanches", "<span class='text-muted'><i class='fa fa-pencil'></i> Editar Lanche</span>");

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao fechar ou ir para outra pagina

		$(window).on('beforeunload', function(){
		   localStorage.clear();
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a troca de paginas da tabela de Lanche

		$(document).on('click', "#btn-pgn-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Chama a função para troca de paginas

				trocaPagina(this, "Lanches");

			// Carrega as Entradas Avulsas
		
				CarregaLanches();

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-tab-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
				
			// Carrega as Entradas Avulsas

				CarregaLanches();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-collapse-filtros-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache na variavel

				var cache = JSON.parse(localStorage.getItem("TipoVeiculo"));

			// Carrega o Cache em variavel Local

				var cacheFiltros = JSON.parse(localStorage.getItem("Lanches"));
					cacheFiltros = cacheFiltros.filtros.estruturaCampos;

			// Limpa o Formulario

				limpaFormulario("#form_filtros_lanches");

			// Verifica se ja existe cache de filtros

				if(cacheFiltros != undefined && cacheFiltros != "" && cacheFiltros != " "){

					// Verifica se existe filtro para cada Campo

						if(cacheFiltros.hasOwnProperty("codigo")){
							$("input[name='filtros_lanches_input_Codigo']").val(cacheFiltros.codigo);
						}

						if(cacheFiltros.hasOwnProperty("nome")){
							$("select[name='filtros_lanches_input_Nome']").val(cacheFiltros.nome);
						}

				} else {

					// Parametriza os Campos

						$("input[name='filtros_lanches_input_Codigo']").val();
						$("input[name='filtros_lanches_input_Nome']").val();

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-filtrar-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Lanches"));
					cache.filtros.estruturaCampos = {};

			// Declara as Variaveis

				var Codigo = $("input[name='filtros_lanches_input_Codigo']");
				var Nome = $("input[name='filtros_lanches_input_Nome']");

			// Monta a estrutura de pesquisa

				if(Codigo.val() != "" && Codigo.val() != " " && Codigo.val() != undefined && parseInt(Codigo.val()) > 0){
					cache.filtros.estruturaCampos.Codigo = Codigo.val();
					estruturaPesquisa += "&codigo = " + Codigo.val();
				}

				if(Nome.val() != "" && Nome.val() != " " && Nome.val() != undefined && parseInt(Nome.val()) > 0){
					cache.filtros.estruturaCampos.Nome = Nome.val();
					estruturaPesquisa += "&codmensalista = " + Nome.val();
				}

			// Define os filtros no cache

				cache.paginaAtual = 1;

			// Grava no cache

				localStorage.setItem("Lanches", JSON.stringify(cache));

			// Exibe o Botão de Remover Filtro

				$("#div-btn-remover-filtros-lanches").show();

			// Carrega as Entradas Avulsas
		
				CarregaLanches();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-remover-filtros-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Lanches"));

			// Limpa o cache

				cache.filtros.estruturaCampos = "";

			// Grava no cache

				localStorage.setItem("Lanches", JSON.stringify(cache));

			// Limpa o Formulario

				limpaFormulario("#form_filtros_lanches");

			// Oculta o Botão de Remover Filtro

				$("#div-btn-remover-filtros-lanches").hide();

			// Carrega as Entradas Avulsas
		
				CarregaLanches();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-cad-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Define as Mascaras

				$("input[name='modal_cad_lanches_input_ValorVenda']").mask('000.000.000.000.000,00', {reverse: true});
				$("input[name='modal_cad_lanches_input_ValorProducao']").mask('000.000.000.000.000,00', {reverse: true});

			// Limpa o Formulario

				limpaFormulario("#form_cad_Lanches");

			// Remove as classes de erros

				$("#modal_cad_lanches_div_Nome").removeClass("has-error");
				$("#modal_cad_lanches_div_Descricao").removeClass("has-error");
				$("#modal_cad_lanches_div_ValorVenda").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_cad_Lanches");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_lanches_input_Nome

		$(document).on('change', "input[name='modal_cad_lanches_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Lanches").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_cad_lanches_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_cad_lanches_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome do lanche precisa ser preenchido', 'Ops!');
							$("#modal_cad_lanches_div_Nome").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_lanches_input_ValorVenda

		$(document).on('change', "input[name='modal_cad_lanches_input_ValorVenda']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Lanches").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ValorVenda = $("input[name='modal_cad_lanches_input_ValorVenda']").val();
							ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
							ValorVenda = parseFloat(ValorVenda);

					// Realiza a Validação

						if(ValorVenda > 0){
							$("#modal_cad_lanches_div_ValorVenda").removeClass("has-error");
						} else {
							toastr.error('Especifique o Valor para Venda do Lanche', 'Ops!');
							$("#modal_cad_lanches_div_ValorVenda").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o cadastro da Nova Lanche

		$(document).on('click', "#btn-modal-cad_lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Nome = $("input[name='modal_cad_lanches_input_Nome']");
				var Descricao = $("input[name='modal_cad_lanches_input_Descricao']");
				var ValorVenda = $("input[name='modal_cad_lanches_input_ValorVenda']").val();
					ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
					ValorVenda = parseFloat(ValorVenda);
				var ValorProducao = $("input[name='modal_cad_lanches_input_ValorProducao']");
				var BotaoModalCadLanches = $("#btn-modal-cad_lanches");

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = ValorVenda > 0;

				var verificaValidacao = validacao[0] && validacao[1];

			// Desativa o botão

				BotaoModalCadLanches.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'nome' : Nome.val(),
							'descricao' : Descricao.val(),
							'valor_venda' : ValorVenda,
							'valor_producao' : parseFloat((ValorProducao.val()).replace(/\./g, "").replace(/\,/g, "."))
						},
						url: 'api/lanches/inserir',
						beforeSend: function(data){
							swal({
								title: 'Cadastrando Novo Lanche',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
							BotaoModalCadLanches.prop("disabled", false);
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								// if(retorno.hasOwnProperty("Status")){
								// 	swal.close();
								// 	swal({
								// 		title: retorno.mensagem_titulo,
								// 		text: retorno.mensagem_texto,
								// 		type: retorno.mensagem_tipo,
								// 		allowOutsideClick: false
								// 	})

								// 	BotaoModalCadLanches.prop("disabled", false);

								// 	return
								// }

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_cad_Lanches");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaLanches();

									// Ativa o botão modal-lanches

										BotaoModalCadLanches.prop("disabled", false);

								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_cad_lanches_div_Nome").addClass("has-error");
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
						}
						
						if(!validacao[1]){
							$("#modal_cad_lanches_div_ValorVenda").addClass("has-error");
							toastr.error('O Valor de Venda precisa ser preenchido', 'Ops!');
						}
									
					// Ativa o Botão
					
						BotaoModalCadLanches.prop("disabled", false);

				}
		});
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-alt-lanches", ()=>{

			// Renderiza o Icheck

		        renderizaICheck('input[name="modal_alt_lanches_input_VagaPreferencial"]',1);
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Pega os Dados da Lanche

				var DadosLanches = JSON.parse(this.activeElement.getAttribute("data-lanches"));
					DadosLanches.valor_venda = (DadosLanches.valor_venda).replace(/\,/g, "").replace(/\./g, ",");
					DadosLanches.valor_producao = (DadosLanches.valor_producao).replace(/\,/g, "").replace(/\./g, ",");

			// Define as Mascaras

				$("input[name='modal_alt_lanches_input_ValorVenda']").mask('000.000.000.000.000,00', {reverse: true});
				$("input[name='modal_alt_lanches_input_ValorProducao']").mask('000.000.000.000.000,00', {reverse: true});

			// Limpa o Formulario

				limpaFormulario("#form_alt_Lanches");

			// Define o valor dos campos

				$("input[name='modal_alt_lanches_input_Codigo']").val(DadosLanches.codigo);
				$("input[name='modal_alt_lanches_input_Nome']").val(DadosLanches.nome);
				$("input[name='modal_alt_lanches_input_NomeAnterior']").val(DadosLanches.nome);
				$("input[name='modal_alt_lanches_input_Descricao']").val(DadosLanches.descricao);
				$("input[name='modal_alt_lanches_input_ValorVenda']").val(DadosLanches.valor_venda);
				$("input[name='modal_alt_lanches_input_ValorProducao']").val(DadosLanches.valor_producao);
			// Remove as classes de erros

				$("#modal_alt_lanches_div_Nome").removeClass("has-error");
				$("#modal_alt_lanches_div_Descricao").removeClass("has-error");
				$("#modal_alt_lanches_div_ValorVenda").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_alt_Lanches");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_lanches_input_Nome

		$(document).on('change', "input[name='modal_alt_lanches_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Lanches").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_alt_lanches_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_alt_lanches_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome do lanche precisa ser preenchido', 'Ops!');
							$("#modal_alt_lanches_div_Nome").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_lanches_input_ValorVenda

		$(document).on('change', "input[name='modal_alt_lanches_input_ValorVenda']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Lanches").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ValorVenda = $("input[name='modal_alt_lanches_input_ValorVenda']").val();
							ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
							ValorVenda = parseFloat(ValorVenda);

					// Realiza a Validação

						if(ValorVenda > 0){
							$("#modal_alt_lanches_div_ValorVenda").removeClass("has-error");
						} else {
							toastr.error('Especifique o Valor para Venda do Lanche', 'Ops!');
							$("#modal_alt_lanches_div_ValorVenda").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a alteração da Lanche

		$(document).on('click', "#btn-modal-alt_lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Codigo = $("input[name='modal_alt_lanches_input_Codigo']");
				var Nome = $("input[name='modal_alt_lanches_input_Nome']");
				var NomeAnterior = $("input[name='modal_alt_lanches_input_NomeAnterior']");
				var Descricao = $("input[name='modal_alt_lanches_input_Descricao']");
				var ValorVenda = $("input[name='modal_alt_lanches_input_ValorVenda']").val();
					ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
					ValorVenda = parseFloat(ValorVenda);
				var ValorProducao = $("input[name='modal_alt_lanches_input_ValorProducao']");
				var BotaoModalAltLanches = $("#btn-modal-alt_lanches");

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = ValorVenda > 0;

				var verificaValidacao = validacao[0] && validacao[1];

			// Desativa o botão

				BotaoModalAltLanches.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'codigo' : Codigo.val(),
							'nome' : Nome.val(),
							'nome_anterior' : NomeAnterior.val(),
							'descricao' : Descricao.val(),
							'valor_venda' : ValorVenda,
							'valor_producao' : parseFloat((ValorProducao.val()).replace(/\./g, "").replace(/\,/g, "."))
						},
						url: 'api/lanches/atualizar',
						beforeSend: function(data){
							swal({
								title: 'Alterando Lanche',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
							BotaoModalAltLanches.prop("disabled", false);
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								// if(retorno.hasOwnProperty("Status")){
								// 	swal.close();
								// 	swal({
								// 		title: retorno.mensagem_titulo,
								// 		text: retorno.mensagem_texto,
								// 		type: retorno.mensagem_tipo,
								// 		allowOutsideClick: false
								// 	})

								// 	BotaoModalAltLanches.prop("disabled", false);

								// 	return
								// }

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_alt_Lanches");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaLanches();

									// Ativa o botão modal-lanches

										BotaoModalAltLanches.prop("disabled", false);
								
								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_alt_lanches_div_Nome").addClass("has-error");
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
						}
						
						if(!validacao[1]){
							$("#modal_alt_lanches_div_ValorVenda").addClass("has-error");
							toastr.error('O Valor de Venda precisa ser preenchido', 'Ops!');
						}
									
					// Ativa o Botão
					
						BotaoModalAltLanches.prop("disabled", false);

				}
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Lanche

		$(document).on('click', "#btn-del-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var DadosLanches = JSON.parse(this.activeElement.getAttribute("data-lanches"));
				var Codigo = DadosLanches.codigo;

			// Verifica se realmente irá excluir o registro e se for verdadeira executa o ajax

				swal({
					title: 'Deseja prosseguir com a exclusão deste Lanche?',
					text: "Lembramos que após excluir, não havera volta!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Deletar',
					cancelButtonText: 'Cancelar',
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: true,
					allowOutsideClick: false
				}).then(function(data){

					if (data.value != undefined) {

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'codigo' : Codigo
							},
							url: 'api/lanches/excluir',
							beforeSend: function(data){
								swal({
									title: 'Excluindo Lanche',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									// if(retorno.hasOwnProperty("Status")){
									// 	swal.close();
									// 	swal({
									// 		title: retorno.mensagem_titulo,
									// 		text: retorno.mensagem_texto,
									// 		type: retorno.mensagem_tipo,
									// 		allowOutsideClick: false
									// 	})

									// 	return
									// }

								// Promisse do sweet alert

									swal({
										title: retorno.mensagem_titulo,
										text: retorno.mensagem_texto,
										type: retorno.mensagem_tipo,
										allowOutsideClick: false
									}).then(()=>{

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaLanches();
									
									})
							}
						});

					} 

				});
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Lanche

		$(document).on('click', "#btn-ativar-lanches, #btn-desativar-lanches", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var DadosLanches = JSON.parse(this.activeElement.getAttribute("data-lanches"));
				var Codigo = DadosLanches.codigo;

				switch((this.activeElement.id).split("-")[1]){
					case "ativar":
						var Status = 1;
						break;
					case "desativar":
						var Status = 2;
						break;
				}

			// executa o ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'codigo' : Codigo,
						'cod_status' : Status
					},
					url: 'api/lanches/atualizar',
					beforeSend: function(data){
						swal({
							title: 'Atualizando Status do Lanche',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},		
					success: function(retorno) {

						// Trata o retorno transformando em array

							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							// if(retorno.hasOwnProperty("Status")){
							// 	swal.close();
							// 	swal({
							// 		title: retorno.mensagem_titulo,
							// 		text: retorno.mensagem_texto,
							// 		type: retorno.mensagem_tipo,
							// 		allowOutsideClick: false
							// 	})

							// 	return
							// }

						// Promisse do sweet alert

							swal({
								title: retorno.mensagem_titulo,
								text: retorno.mensagem_texto,
								type: retorno.mensagem_tipo,
								allowOutsideClick: false
							}).then(()=>{

								// Chama a função para Carregar as Entradas Avulsas
								
									CarregaLanches();
							
							})
					}
				});

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});