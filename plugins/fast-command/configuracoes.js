//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                  JavaScript da Página : [PATH]/home.asp		    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerar o cache do Painel

		function geraCacheConfiguracoes(){
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as Variaveis de Cache

				var Configuracoes = {
			    	codigo : 0,
					razao : 0,
					cnpj : 0,
					telefoneResidencial : 0,
					telefoneComercial : 0,
					telefoneCorporativo : 0,
					email : 0,
					cep : 0,
					bairro : 0,
					endereco : 0,
					cidade : 0,
					uf : 0,
					numero : 0,
					habilitarFracao : 0,
					duracaoFracao : 0,
					habilitarTolerancia : 0,
					duracaoTolerancia : 0,
					habilitarSMS : 0,
			    }

			    var ConfigCupomAvulsoEntrada = {}

			    var ConfigCupomAvulsoSaida = {}

			    var ConfigCupomMensalidade = {}

			    var TipoVeiculoPaginacao = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 5,
			    	registrosPagina : 0,
			    	totalRegistros : 0,
			    	registros: 0
			    }

			    var TipoVeiculo = {};

			    var TipoUsuarioPaginacao = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 5,
			    	registrosPagina : 0,
			    	totalRegistros : 0,
			    	registros: 0
			    }

			    var TipoUsuario = {};

			    var UsuariosPaginacao = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 5,
			    	registrosPagina : 0,
			    	totalRegistros : 0,
			    	registros: 0
			    }

			    var Usuarios = {};

				var EstacionamentoPaginacao = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 5,
			    	registrosPagina : 0,
			    	totalRegistros : 0
				}

			    var Estacionamento = {};

				var PlanoPaginacao = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 5,
			    	registrosPagina : 0,
			    	totalRegistros : 0
				}

			    var Plano = {};

				var ConfigSMSPaginacao = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 5,
			    	registrosPagina : 0,
			    	totalRegistros : 0
				}

			    var ConfigSMS = {};

			    var ConfigSMSCupom = {};

			    var TipoAutenticacao = {};

			// Grava no localStorage

				localStorage.setItem("Configuracoes", JSON.stringify(Configuracoes));
				localStorage.setItem("ConfigCupomAvulsoEntrada", JSON.stringify(ConfigCupomAvulsoEntrada));
				localStorage.setItem("ConfigCupomAvulsoSaida", JSON.stringify(ConfigCupomAvulsoSaida));
				localStorage.setItem("ConfigCupomMensalidade", JSON.stringify(ConfigCupomMensalidade));
				localStorage.setItem("ConfigSMSCupom", JSON.stringify(ConfigSMSCupom));
				localStorage.setItem("TipoVeiculoPaginacao", JSON.stringify(TipoVeiculoPaginacao));
				localStorage.setItem("TipoVeiculo", JSON.stringify(TipoVeiculo));
				localStorage.setItem("TipoUsuarioPaginacao", JSON.stringify(TipoUsuarioPaginacao));
				localStorage.setItem("TipoUsuario", JSON.stringify(TipoUsuario));
				localStorage.setItem("UsuariosPaginacao", JSON.stringify(UsuariosPaginacao));
				localStorage.setItem("Usuarios", JSON.stringify(Usuarios));
				localStorage.setItem("EstacionamentoPaginacao", JSON.stringify(EstacionamentoPaginacao));
				localStorage.setItem("Estacionamento", JSON.stringify(Estacionamento));
				localStorage.setItem("PlanoPaginacao", JSON.stringify(PlanoPaginacao));
				localStorage.setItem("Plano", JSON.stringify(Plano));
				localStorage.setItem("ConfigSMSPaginacao", JSON.stringify(ConfigSMSPaginacao));
				localStorage.setItem("ConfigSMS", JSON.stringify(ConfigSMS));
				localStorage.setItem("TipoAutenticacao", JSON.stringify(TipoAutenticacao));
		}

		geraCacheConfiguracoes();

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                   	jQuery da Página : [PATH]/home.asp		    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Tipos de Autenticação

		function CarregaTipoAutenticacaoCache(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("TipoAutenticacao"));

			// Declaração das Variaveis

				var Funcao = 4;

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/tipoAutenticacao_ajax.asp',
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Verifica se ja existe cache

							var cache = JSON.parse(localStorage.getItem("TipoAutenticacao"));

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cache = retorno.TipoAutenticacao;

						// Grava o Cache

							localStorage.setItem("TipoAutenticacao", JSON.stringify(cache));

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Planos

		function CarregaPlanoCache(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("Plano"));

			// Declaração das Variaveis

				var Funcao = 4;

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/planos_ajax.asp',
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Verifica se ja existe cache

							var cache = JSON.parse(localStorage.getItem("Plano"));

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cache = retorno.Planos;

						// Grava o Cache

							localStorage.setItem("Plano", JSON.stringify(cache));

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Tipos de Veiculos

		function CarregaTipoVeiculoCache(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("TipoVeiculo"));

			// Declaração das Variaveis

				var Funcao = 4;

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/tipoVeiculo_ajax.asp',
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Verifica se ja existe cache

							var cache = JSON.parse(localStorage.getItem("TipoVeiculo"));

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cache = retorno.TipoVeiculo;

						// Grava o Cache

							localStorage.setItem("TipoVeiculo", JSON.stringify(cache));

					}
				});
		}
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Tipos de Usuarios

		function CarregaTipoUsuarioCache(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("TipoUsuario"));

			// Declaração das Variaveis

				var Funcao = 4;

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/tipoUsuario_ajax.asp',
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Verifica se ja existe cache

							var cache = JSON.parse(localStorage.getItem("TipoUsuario"));

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cache = retorno.TipoUsuario;

						// Grava o Cache

							localStorage.setItem("TipoUsuario", JSON.stringify(cache));

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Usuários

		function CarregaUsuariosCache(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("Usuarios"));

			// Declaração das Variaveis

				var Funcao = 4;

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/tipoUsuario_ajax.asp',
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Verifica se ja existe cache

							var cache = JSON.parse(localStorage.getItem("Usuarios"));

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cache = retorno.Usuarios;

						// Grava o Cache

							localStorage.setItem("Usuarios", JSON.stringify(cache));

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Informações do Estacionamento

		function CarregaEstacionamento(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cachePaginacao = JSON.parse(localStorage.getItem("EstacionamentoPaginacao"));
				var cache = JSON.parse(localStorage.getItem("Estacionamento"));

			// Declaração das Variaveis

				var Funcao = 5;
				var RegistrosPorPaginas = 5;
				var PaginaRegistro = 0;
				if(cachePaginacao == null){
					PaginaRegistro = 0;
				} else if(cachePaginacao.hasOwnProperty("paginaAtual")){
					if(cachePaginacao.paginaAtual > 0){
						PaginaRegistro = (cachePaginacao.paginaAtual * RegistrosPorPaginas) - 5;
					} else {
						PaginaRegistro = 0;
					}
				}

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao,
						'RegistrosPorPaginas' : RegistrosPorPaginas,
						'PaginaRegistro' : PaginaRegistro
					},
					url: 'ajax/estacionamento_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Estacionamento',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Declara as Variaveis
						
							var TotalEstacionamento = parseInt(retorno.TotalEstacionamento);
							var PonteiroEstacionamento = 0;
							var Codigo = 0;
							var Icone = 0;
							var TipoVeiculo = 0;
							var TotalVagas = 0;
							var VagasDisponiveis = 0;
							var VagasIndisponiveis = 0;
							var ValorHora = 0;
							var ValorFracao = 0;
							var VagaPreferencial = 0;
							var LinhaHTML = 0;
							var AntigoHTML = 0;
							var TotalRegistrosPagina = retorno.Estacionamento.length;
						
						// Remove todas as Pessoas Fisicas
						
							$("#registrosEstacionamento").html("");
						
						// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
				
							for(PonteiroEstacionamento=0;PonteiroEstacionamento<TotalRegistrosPagina;PonteiroEstacionamento++){
							
								Codigo = retorno.Estacionamento[PonteiroEstacionamento].Codigo;
								Icone = retorno.Estacionamento[PonteiroEstacionamento].Icone;
								TipoVeiculo = parseInt(retorno.Estacionamento[PonteiroEstacionamento].TipoVeiculo);
								TotalVagas = retorno.Estacionamento[PonteiroEstacionamento].TotalVagas;
								VagasDisponiveis = retorno.Estacionamento[PonteiroEstacionamento].VagasDisponiveis;
								VagasIndisponiveis = retorno.Estacionamento[PonteiroEstacionamento].VagasIndisponiveis;
								ValorHora = retorno.Estacionamento[PonteiroEstacionamento].ValorHora;
								ValorFracao = retorno.Estacionamento[PonteiroEstacionamento].ValorFracao;
								VagaPreferencial = retorno.Estacionamento[PonteiroEstacionamento].VagaPreferencial;

								LinhaHTML = "<tr>"
								LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
								LinhaHTML += "<td class='text-center'><i class='" + Icone + "'></i></td>"
								LinhaHTML += "<td class='text-center'>" + ExtraiTipoVeiculo(TipoVeiculo, "TipoVeiculo") + "</td>"
								LinhaHTML += "<td class='text-center'>" + TotalVagas + "</td>"
								LinhaHTML += "<td class='text-center'>" + VagasDisponiveis + "</td>"
								LinhaHTML += "<td class='text-center'>" + VagasIndisponiveis + "</td>"
								LinhaHTML += "<td class='text-center'>" + ValorHora + "</td>"
								LinhaHTML += "<td class='text-center'>" + ValorFracao + "</td>"
								if(VagaPreferencial == 2){
									LinhaHTML += "<td class='text-center'><i class='fa fa-times-circle text-danger'></i></td>"
								} else {
									LinhaHTML += "<td class='text-center'><i class='fa fa-check-circle text-success'></i></td>"
								}
								LinhaHTML += "<td class='text-center'>"
								// LinhaHTML += "<button type='button' class='btn btn-xs btn-success' data-toggle='tooltip' id='btn-vincular-vaga' data-vagasEstacionamento='"+JSON.stringify(retorno.Estacionamento[PonteiroEstacionamento])+"' data-placement='left' title='Vincular Vaga'><i class='fa fa-clone'></i></button>"
								LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-vagasEstacionamento' data-vagasEstacionamento='"+JSON.stringify(retorno.Estacionamento[PonteiroEstacionamento])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
								LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-vagasEstacionamento' data-vagasEstacionamento='"+JSON.stringify(retorno.Estacionamento[PonteiroEstacionamento])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
								LinhaHTML += "</td>"
								LinhaHTML += "</tr>"
								
								AntigoHTML = $("#registrosEstacionamento").html();
								$("#registrosEstacionamento").html(AntigoHTML+LinhaHTML);
								
							}

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cachePaginacao.totalRegistros = TotalEstacionamento;
							cachePaginacao.registrosPagina = TotalRegistrosPagina;
							cache = retorno.Estacionamento;

						// Fecha o SweetAlert

							swal.close();

						// Grava o Cache

							localStorage.setItem("EstacionamentoPaginacao", JSON.stringify(cachePaginacao));
							localStorage.setItem("Estacionamento", JSON.stringify(cache));

	        			// Gera a Paginacao

							geraEstruturaPaginacao("#pgn-estacionamento", "EstacionamentoPaginacao");

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Informações do Estacionamento

		function CarregaPlanos(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cachePaginacao = JSON.parse(localStorage.getItem("PlanoPaginacao"));
				var cache = JSON.parse(localStorage.getItem("Plano"));

			// Declaração das Variaveis

				var Funcao = 5;
				var RegistrosPorPaginas = 5;
				var PaginaRegistro = 0;
				if(cachePaginacao == null){
					PaginaRegistro = 0;
				} else if(cachePaginacao.hasOwnProperty("paginaAtual")){
					if(cachePaginacao.paginaAtual > 0){
						PaginaRegistro = (cachePaginacao.paginaAtual * RegistrosPorPaginas) - 5;
					} else {
						PaginaRegistro = 0;
					}
				}

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao,
						'RegistrosPorPaginas' : RegistrosPorPaginas,
						'PaginaRegistro' : PaginaRegistro
					},
					url: 'ajax/planos_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Tipos de Veiculos',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Declara as Variaveis
						
							var TotalPlanos = parseInt(retorno.TotalPlanos);
							var PonteiroPlanos = 0;
							var Codigo = 0;
							var Descricao = 0;
							var Valor = 0;
							var Vagas = 0;
							var Horas = 0;
							var LinhaHTML = 0;
							var AntigoHTML = 0;
							var TotalRegistrosPagina = retorno.Planos.length;
						
						// Remove todas as Pessoas Fisicas
						
							$("#registrosPlanos").html("");
						
						// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
				
							for(PonteiroPlanos=0;PonteiroPlanos<TotalRegistrosPagina;PonteiroPlanos++){
							
								Codigo = retorno.Planos[PonteiroPlanos].Codigo;
								Descricao = retorno.Planos[PonteiroPlanos].Descricao;
								Valor = retorno.Planos[PonteiroPlanos].Valor;
								Vagas = retorno.Planos[PonteiroPlanos].Vagas;
								Horas = retorno.Planos[PonteiroPlanos].Horas;

								LinhaHTML = "<tr>"
								LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
								LinhaHTML += "<td class='text-center'>" + Descricao + "</td>"
								LinhaHTML += "<td class='text-center'>" + Valor + "</td>"
								LinhaHTML += "<td class='text-center'>" + Vagas + "</td>"
								LinhaHTML += "<td class='text-center'>" + Horas + "</td>"
								LinhaHTML += "<td class='text-center'>"
								LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-plano' data-plano='"+JSON.stringify(retorno.Planos[PonteiroPlanos])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
								LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-plano' data-plano='"+JSON.stringify(retorno.Planos[PonteiroPlanos])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
								LinhaHTML += "</td>"
								LinhaHTML += "</tr>"
								
								AntigoHTML = $("#registrosPlanos").html();
								$("#registrosPlanos").html(AntigoHTML+LinhaHTML);
								
							}

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cachePaginacao.totalRegistros = TotalPlanos;
							cachePaginacao.registrosPagina = TotalRegistrosPagina;
							cache = retorno.Planos;

						// Fecha o SweetAlert

							swal.close();

						// Grava o Cache

							localStorage.setItem("PlanoPaginacao", JSON.stringify(cachePaginacao));
							localStorage.setItem("Plano", JSON.stringify(cache));

	        			// Gera a Paginacao

							geraEstruturaPaginacao("#pgn-planos", "PlanoPaginacao");

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Informações do Estacionamento

		function CarregaTipoVeiculo(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cachePaginacao = JSON.parse(localStorage.getItem("TipoVeiculoPaginacao"));
				var cache = JSON.parse(localStorage.getItem("TipoVeiculo"));

			// Declaração das Variaveis

				var Funcao = 5;
				var RegistrosPorPaginas = 5;
				var PaginaRegistro = 0;
				if(cachePaginacao == null){
					PaginaRegistro = 0;
				} else if(cachePaginacao.hasOwnProperty("paginaAtual")){
					if(cachePaginacao.paginaAtual > 0){
						PaginaRegistro = (cachePaginacao.paginaAtual * RegistrosPorPaginas) - 5;
					} else {
						PaginaRegistro = 0;
					}
				}

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao,
						'RegistrosPorPaginas' : RegistrosPorPaginas,
						'PaginaRegistro' : PaginaRegistro
					},
					url: 'ajax/tipoVeiculo_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Tipos de Veiculos',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Declara as Variaveis
						
							var TotalTipoVeiculo = parseInt(retorno.TotalTipoVeiculo);
							var PonteiroTipoVeiculo = 0;
							var Codigo = 0;
							var Descricao = 0;
							var LinhaHTML = 0;
							var AntigoHTML = 0;
							var TotalRegistrosPagina = retorno.TipoVeiculo.length;
						
						// Remove todas as Pessoas Fisicas
						
							$("#registrosTipoVeiculo").html("");
						
						// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
				
							for(PonteiroTipoVeiculo=0;PonteiroTipoVeiculo<TotalRegistrosPagina;PonteiroTipoVeiculo++){
							
								Codigo = retorno.TipoVeiculo[PonteiroTipoVeiculo].Codigo;
								Descricao = retorno.TipoVeiculo[PonteiroTipoVeiculo].Descricao;

								LinhaHTML = "<tr>"
								LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
								LinhaHTML += "<td class='text-center'>" + Descricao + "</td>"
								LinhaHTML += "<td class='text-center'>"
								LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-tipoVeiculo' data-tipoVeiculo='"+JSON.stringify(retorno.TipoVeiculo[PonteiroTipoVeiculo])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
								LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-tipoVeiculo' data-tipoVeiculo='"+JSON.stringify(retorno.TipoVeiculo[PonteiroTipoVeiculo])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
								LinhaHTML += "</td>"
								LinhaHTML += "</tr>"
								
								AntigoHTML = $("#registrosTipoVeiculo").html();
								$("#registrosTipoVeiculo").html(AntigoHTML+LinhaHTML);
								
							}

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cachePaginacao.totalRegistros = TotalTipoVeiculo;
							cachePaginacao.registrosPagina = TotalRegistrosPagina;
							cache = retorno.TipoVeiculo;

						// Fecha o SweetAlert

							swal.close();

						// Grava o Cache

							localStorage.setItem("TipoVeiculoPaginacao", JSON.stringify(cachePaginacao));
							localStorage.setItem("TipoVeiculo", JSON.stringify(cache));

	        			// Gera a Paginacao

							geraEstruturaPaginacao("#pgn-tipoVeiculo", "TipoVeiculoPaginacao");

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Informações do Estacionamento

		function CarregaTipoUsuario(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verificação de Permissão de Desenvolvedor

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cachePaginacao = JSON.parse(localStorage.getItem("TipoUsuarioPaginacao"));
				var cache = JSON.parse(localStorage.getItem("TipoUsuario"));

			// Declaração das Variaveis

				var Funcao = 5;
				var RegistrosPorPaginas = 5;
				var PaginaRegistro = 0;
				if(cachePaginacao == null){
					PaginaRegistro = 0;
				} else if(cachePaginacao.hasOwnProperty("paginaAtual")){
					if(cachePaginacao.paginaAtual > 0){
						PaginaRegistro = (cachePaginacao.paginaAtual * RegistrosPorPaginas) - 5;
					} else {
						PaginaRegistro = 0;
					}
				}

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao,
						'RegistrosPorPaginas' : RegistrosPorPaginas,
						'PaginaRegistro' : PaginaRegistro
					},
					url: 'ajax/tipoUsuario_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Tipos de Usuários',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Declara as Variaveis
						
							var TotalTipoUsuario = parseInt(retorno.TotalTipoUsuario);
							var PonteiroTipoUsuario = 0;
							var Codigo = 0;
							var Descricao = 0;
							var LinhaHTML = 0;
							var AntigoHTML = 0;
							var TotalRegistrosPagina = retorno.TipoUsuario.length;
						
						// Remove todas as Pessoas Fisicas
						
							$("#registrosTiposUsuarios").html("");
						
						// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
				
							for(PonteiroTipoUsuario=0;PonteiroTipoUsuario<TotalRegistrosPagina;PonteiroTipoUsuario++){
							
								Codigo = retorno.TipoUsuario[PonteiroTipoUsuario].Codigo;
								Descricao = retorno.TipoUsuario[PonteiroTipoUsuario].Descricao;

								LinhaHTML = "<tr>"
								LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
								LinhaHTML += "<td class='text-center'>" + Descricao + "</td>"
								LinhaHTML += "<td class='text-center'>"
								LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-tipoUsuario' data-tipoUsuario='"+JSON.stringify(retorno.TipoUsuario[PonteiroTipoUsuario])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
								LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-tipoUsuario' data-tipoUsuario='"+JSON.stringify(retorno.TipoUsuario[PonteiroTipoUsuario])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
								LinhaHTML += "</td>"
								LinhaHTML += "</tr>"
								
								AntigoHTML = $("#registrosTiposUsuarios").html();
								$("#registrosTiposUsuarios").html(AntigoHTML+LinhaHTML);
								
							}

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cachePaginacao.totalRegistros = TotalTipoUsuario;
							cachePaginacao.registrosPagina = TotalRegistrosPagina;
							cache = retorno.TipoUsuario;

						// Fecha o SweetAlert

							swal.close();

						// Grava o Cache

							localStorage.setItem("TipoUsuarioPaginacao", JSON.stringify(cachePaginacao));
							localStorage.setItem("TipoUsuario", JSON.stringify(cache));

	        			// Gera a Paginacao

							geraEstruturaPaginacao("#pgn-tipoUsuario", "TipoUsuarioPaginacao");

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Informações do Estacionamento

		function CarregaUsuarios(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cachePaginacao = JSON.parse(localStorage.getItem("UsuariosPaginacao"));
				var cache = JSON.parse(localStorage.getItem("Usuarios"));

			// Declaração das Variaveis

				var Funcao = 5;
				var RegistrosPorPaginas = 5;
				var PaginaRegistro = 0;
				if(cachePaginacao == null){
					PaginaRegistro = 0;
				} else if(cachePaginacao.hasOwnProperty("paginaAtual")){
					if(cachePaginacao.paginaAtual > 0){
						PaginaRegistro = (cachePaginacao.paginaAtual * RegistrosPorPaginas) - 5;
					} else {
						PaginaRegistro = 0;
					}
				}

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao,
						'RegistrosPorPaginas' : RegistrosPorPaginas,
						'PaginaRegistro' : PaginaRegistro
					},
					url: 'ajax/usuarios_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Usuários',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Declara as Variaveis
						
							var TotalUsuarios = parseInt(retorno.TotalUsuarios);
							var PonteiroUsuarios = 0;
							var Codigo = 0;
							var Nome = 0;
							var Email = 0;
							var Status = 0;
							var LinhaHTML = 0;
							var AntigoHTML = 0;
							var TotalRegistrosPagina = retorno.Usuarios.length;
						
						// Remove todas as Pessoas Fisicas
						
							$("#registrosUsuarios").html("");
						
						// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
				
							for(PonteiroUsuarios=0;PonteiroUsuarios<TotalRegistrosPagina;PonteiroUsuarios++){
							
								Codigo = retorno.Usuarios[PonteiroUsuarios].Codigo;
								Nome = retorno.Usuarios[PonteiroUsuarios].Nome;
								Email = retorno.Usuarios[PonteiroUsuarios].Email;
								TipoUsuario = retorno.Usuarios[PonteiroUsuarios].TipoUsuario;
								Status = retorno.Usuarios[PonteiroUsuarios].Status;

								if(AcessoDesenvolvedor(Sessao.Permissoes) == false && parseInt(TipoUsuario) != 1 || AcessoDesenvolvedor(Sessao.Permissoes)){

									LinhaHTML = "<tr>"
									LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
									LinhaHTML += "<td class='text-center'>" + Nome + "</td>"
									LinhaHTML += "<td class='text-center'>" + Email + "</td>"
									LinhaHTML += "<td class='text-center'>" + ExtraiUsuario(TipoUsuario, "TipoUsuario") + "</td>"
									if(Status == 1){
										LinhaHTML += "<td class='text-center'><label class='label label-success'>Online</label></td>"
									} else {
										LinhaHTML += "<td class='text-center'><label class='label label-default'>Offline</label></td>"
									}
									LinhaHTML += "<td class='text-center'>"
									LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-usuario' data-usuario='"+JSON.stringify(retorno.Usuarios[PonteiroUsuarios])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
									if(Status == 1){
										LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-loggout-usuario' data-usuario='"+JSON.stringify(retorno.Usuarios[PonteiroUsuarios])+"' data-placement='left' title='Desconectar'><i class='fa fa-power-off'></i></button>"
									} else {
										LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-usuario' data-usuario='"+JSON.stringify(retorno.Usuarios[PonteiroUsuarios])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
									}
									LinhaHTML += "</td>"
									LinhaHTML += "</tr>"
									
									AntigoHTML = $("#registrosUsuarios").html();
									$("#registrosUsuarios").html(AntigoHTML+LinhaHTML);
									
								}

							}

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cachePaginacao.totalRegistros = TotalUsuarios;
							cachePaginacao.registrosPagina = TotalRegistrosPagina;
							cache = retorno.Usuarios;

						// Fecha o SweetAlert

							swal.close();

						// Grava o Cache

							localStorage.setItem("UsuariosPaginacao", JSON.stringify(cachePaginacao));
							localStorage.setItem("Usuarios", JSON.stringify(cache));

	        			// Gera a Paginacao

							geraEstruturaPaginacao("#pgn-usuarios", "UsuariosPaginacao");

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Configurações

		function CarregaConfiguracoes(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("Configuracoes"));

			// Declaração das Variaveis

				var Funcao = 2;
				var CodConfiguracoes = $("input[name='CodConfiguracoes']");
				var Razao = $("input[name='Razao']");
				var CNPJ = $("input[name='CNPJ']");
				var TelefoneResidencial = $("input[name='TelefoneResidencial']");
				var TelefoneComercial = $("input[name='TelefoneComercial']");
				var TelefoneCorporativo = $("input[name='TelefoneCorporativo']");
				var Email = $("input[name='Email']");
				var CEP = $("input[name='CEP']");
				var Bairro = $("input[name='Bairro']");
				var Endereco = $("input[name='Endereco']");
				var Cidade = $("input[name='Cidade']");
				var UF = $("input[name='UF']");
				var Numero = $("input[name='Numero']");
				var HabilitarFracao = $("input[name='HabilitarFracao']");
				var DuracaoFracao = $("input[name='DuracaoFracao']");
				var HabilitarTolerancia = $("input[name='HabilitarTolerancia']");
				var DuracaoTolerancia = $("input[name='DuracaoTolerancia']");
				var HabilitarSMS = $("input[name='HabilitarSMS']");

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/configuracoes_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Configurações Geral',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se o retorno esta vazio

							if(retorno == undefined || retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							} else {

								// Declara as Variaveis

									var TotalConfiguracoes = retorno.Configuracoes.length;
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroConfiguracoes=0;PonteiroConfiguracoes<TotalConfiguracoes;PonteiroConfiguracoes++){
									
										// Grava as informações na array cache

											cache.codigo = retorno.Configuracoes[PonteiroConfiguracoes].Codigo;
											cache.razao = retorno.Configuracoes[PonteiroConfiguracoes].Razao;
											cache.cnpj = retorno.Configuracoes[PonteiroConfiguracoes].CNPJ;
											cache.telefoneResidencial = retorno.Configuracoes[PonteiroConfiguracoes].TelefoneResidencial;
											cache.telefoneComercial = retorno.Configuracoes[PonteiroConfiguracoes].TelefoneComercial;
											cache.telefoneCorporativo = retorno.Configuracoes[PonteiroConfiguracoes].TelefoneCorporativo;
											cache.email = retorno.Configuracoes[PonteiroConfiguracoes].Email;
											cache.cep = retorno.Configuracoes[PonteiroConfiguracoes].CEP;
											cache.bairro = retorno.Configuracoes[PonteiroConfiguracoes].Bairro;
											cache.endereco = retorno.Configuracoes[PonteiroConfiguracoes].Endereco;
											cache.cidade = retorno.Configuracoes[PonteiroConfiguracoes].Cidade;
											cache.uf = retorno.Configuracoes[PonteiroConfiguracoes].UF;
											cache.numero = retorno.Configuracoes[PonteiroConfiguracoes].Numero;
											cache.habilitarFracao = parseInt(retorno.Configuracoes[PonteiroConfiguracoes].HabilitarFracao);
											cache.duracaoFracao = retorno.Configuracoes[PonteiroConfiguracoes].DuracaoFracao;
											cache.habilitarTolerancia = parseInt(retorno.Configuracoes[PonteiroConfiguracoes].HabilitarTolerancia);
											cache.duracaoTolerancia = retorno.Configuracoes[PonteiroConfiguracoes].DuracaoTolerancia;
											cache.habilitarSMS = retorno.Configuracoes[PonteiroConfiguracoes].HabilitarSMS;

										// Seta os valores dos campos

											Razao.val(retorno.Configuracoes[PonteiroConfiguracoes].Razao);
											CNPJ.val(retorno.Configuracoes[PonteiroConfiguracoes].CNPJ);
											TelefoneResidencial.val(retorno.Configuracoes[PonteiroConfiguracoes].TelefoneResidencial);
											TelefoneComercial.val(retorno.Configuracoes[PonteiroConfiguracoes].TelefoneComercial);
											TelefoneCorporativo.val(retorno.Configuracoes[PonteiroConfiguracoes].TelefoneCorporativo);
											Email.val(retorno.Configuracoes[PonteiroConfiguracoes].Email);
											CEP.val(retorno.Configuracoes[PonteiroConfiguracoes].CEP);
											Bairro.val(retorno.Configuracoes[PonteiroConfiguracoes].Bairro);
											Endereco.val(retorno.Configuracoes[PonteiroConfiguracoes].Endereco);
											Cidade.val(retorno.Configuracoes[PonteiroConfiguracoes].Cidade);
											UF.val(retorno.Configuracoes[PonteiroConfiguracoes].UF);
											Numero.val(retorno.Configuracoes[PonteiroConfiguracoes].Numero);

										// Seta o valor do Codigo

											CodConfiguracoes.val(cache.codigo);

										// Habilita / Desabilita Fração

											if(cache.habilitarFracao == 1){
												HabilitarFracao.bootstrapToggle('on');
											} else {
												HabilitarFracao.bootstrapToggle('off');
											}

										// Seta a duração da Fração

											DuracaoFracao.val(cache.duracaoFracao);

										// Habilita / Desabilita Tolerância

											if(cache.habilitarTolerancia == 1){
												HabilitarTolerancia.bootstrapToggle('on');
											} else {
												HabilitarTolerancia.bootstrapToggle('off');
											}

										// Seta a duração da Tolerância

											DuracaoTolerancia.val(cache.duracaoTolerancia);
										
										// Habilita / Desabilita SMS

											if(cache.habilitarSMS == 1){
												HabilitarSMS.bootstrapToggle('on');
												$("#btn-tab-configSMS").show();
												$("#linha-configSMSCupom").show();
												$("#configSMSCupom").show();
											} else {
												HabilitarSMS.bootstrapToggle('off');
												$("#btn-tab-configSMS").hide();
												$("#linha-configSMSCupom").hide();
												$("#configSMSCupom").hide();
											}

									}

								// Fecha oo sweet alert

									swal.close();

								// Grava o Cache

									localStorage.setItem("Configuracoes", JSON.stringify(cache));

							}

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Configurações

		function CarregaConfigCupomAvulsoEntrada(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("ConfigCupomAvulsoEntrada"));

			// Declaração das Variaveis

				var Funcao = 2;
				var CodConfigCupomAvulsoEntrada = $("input[name='CodConfigCupomAvulsoEntrada']");
				var HabilitarTopo = $("input[name='CupomAvulsoEntrada_HabilitarTopo']");
				var HabilitarInfoEmpresa = $("input[name='CupomAvulsoEntrada_HabilitarInfoEmpresa']");
				var HabilitarInfoVeiculo = $("input[name='CupomAvulsoEntrada_HabilitarInfoVeiculo']");
				var HabilitarInfoServico = $("input[name='CupomAvulsoEntrada_HabilitarInfoServico']");

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/configCupomAvulsoEntrada_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Configurações do Cupom Avulso de Entrada',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se o retorno esta vazio

							if(retorno == undefined || retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})
								return
							} else {

								// Declara as Variaveis

									var TotalConfigCupomAvulsoEntrada = retorno.ConfigCupomAvulsoEntrada.length;
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroConfigCupomAvulsoEntrada=0;PonteiroConfigCupomAvulsoEntrada<TotalConfigCupomAvulsoEntrada;PonteiroConfigCupomAvulsoEntrada++){
									
										// Grava as informações na array cache

											cache = retorno.ConfigCupomAvulsoEntrada[PonteiroConfigCupomAvulsoEntrada];

										// Seta o valor do Codigo

											CodConfigCupomAvulsoEntrada.val(cache.Codigo);

										// Habilita / Desabilita Topo

											if(cache.HabilitarTopo == 1){
												HabilitarTopo.bootstrapToggle('on');
											} else {
												HabilitarTopo.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoEmpresa

											if(cache.HabilitarInfoEmpresa == 1){
												HabilitarInfoEmpresa.bootstrapToggle('on');
											} else {
												HabilitarInfoEmpresa.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoVeiculo

											if(cache.HabilitarInfoVeiculo == 1){
												HabilitarInfoVeiculo.bootstrapToggle('on');
											} else {
												HabilitarInfoVeiculo.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoServico

											if(cache.HabilitarInfoServico == 1){
												HabilitarInfoServico.bootstrapToggle('on');
											} else {
												HabilitarInfoServico.bootstrapToggle('off');
											}
										
									}

								// Fecha oo sweet alert

									swal.close();

								// Grava o Cache

									localStorage.setItem("ConfigCupomAvulsoEntrada", JSON.stringify(cache));

							}

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Configurações

		function CarregaConfigCupomAvulsoSaida(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("ConfigCupomAvulsoSaida"));

			// Declaração das Variaveis

				var Funcao = 2;
				var CodConfigCupomAvulsoSaida = $("input[name='CodConfigCupomAvulsoSaida']");
				var HabilitarTopo = $("input[name='CupomAvulsoSaida_HabilitarTopo']");
				var HabilitarInfoEmpresa = $("input[name='CupomAvulsoSaida_HabilitarInfoEmpresa']");
				var HabilitarInfoVeiculo = $("input[name='CupomAvulsoSaida_HabilitarInfoVeiculo']");
				var HabilitarInfoServico = $("input[name='CupomAvulsoSaida_HabilitarInfoServico']");
				var HabilitarInfoCaixa = $("input[name='CupomAvulsoSaida_HabilitarInfoCaixa']");

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/configCupomAvulsoSaida_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Configurações do Cupom Avulso de Saida',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se o retorno esta vazio

							if(retorno == undefined || retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})
								return
							} else {

								// Declara as Variaveis

									var TotalConfigCupomAvulsoSaida = retorno.ConfigCupomAvulsoSaida.length;
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroConfigCupomAvulsoSaida=0;PonteiroConfigCupomAvulsoSaida<TotalConfigCupomAvulsoSaida;PonteiroConfigCupomAvulsoSaida++){
									
										// Grava as informações na array cache

											cache = retorno.ConfigCupomAvulsoSaida[PonteiroConfigCupomAvulsoSaida];

										// Seta o valor do Codigo

											CodConfigCupomAvulsoSaida.val(cache.Codigo);

										// Habilita / Desabilita Topo

											if(cache.HabilitarTopo == 1){
												HabilitarTopo.bootstrapToggle('on');
											} else {
												HabilitarTopo.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoEmpresa

											if(cache.HabilitarInfoEmpresa == 1){
												HabilitarInfoEmpresa.bootstrapToggle('on');
											} else {
												HabilitarInfoEmpresa.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoVeiculo

											if(cache.HabilitarInfoVeiculo == 1){
												HabilitarInfoVeiculo.bootstrapToggle('on');
											} else {
												HabilitarInfoVeiculo.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoServico

											if(cache.HabilitarInfoServico == 1){
												HabilitarInfoServico.bootstrapToggle('on');
											} else {
												HabilitarInfoServico.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoCaixa

											if(cache.HabilitarInfoCaixa == 1){
												HabilitarInfoCaixa.bootstrapToggle('on');
											} else {
												HabilitarInfoCaixa.bootstrapToggle('off');
											}
										
									}

								// Fecha oo sweet alert

									swal.close();

								// Grava o Cache

									localStorage.setItem("ConfigCupomAvulsoSaida", JSON.stringify(cache));

							}

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Configurações

		function CarregaConfigCupomMensalidade(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("ConfigCupomMensalidade"));

			// Declaração das Variaveis

				var Funcao = 2;
				var CodConfigCupomMensalidade = $("input[name='CodConfigCupomMensalidade']");
				var HabilitarTopo = $("input[name='CupomMensalidade_HabilitarTopo']");
				var HabilitarInfoEmpresa = $("input[name='CupomMensalidade_HabilitarInfoEmpresa']");
				var HabilitarInfoMensalista = $("input[name='CupomMensalidade_HabilitarInfoMensalista']");
				var HabilitarInfoMensalidade = $("input[name='CupomMensalidade_HabilitarInfoMensalidade']");
				var HabilitarInfoCaixa = $("input[name='CupomMensalidade_HabilitarInfoCaixa']");

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/configCupomMensalidade_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Configurações do Cupom de Mensalidade',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se o retorno esta vazio

							if(retorno == undefined || retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})
								return
							} else {

								// Declara as Variaveis

									var TotalConfigCupomMensalidade = retorno.ConfigCupomMensalidade.length;
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroConfigCupomMensalidade=0;PonteiroConfigCupomMensalidade<TotalConfigCupomMensalidade;PonteiroConfigCupomMensalidade++){
									
										// Grava as informações na array cache

											cache = retorno.ConfigCupomMensalidade[PonteiroConfigCupomMensalidade];

										// Seta o valor do Codigo

											CodConfigCupomMensalidade.val(cache.Codigo);

										// Habilita / Desabilita Topo

											if(cache.HabilitarTopo == 1){
												HabilitarTopo.bootstrapToggle('on');
											} else {
												HabilitarTopo.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoEmpresa

											if(cache.HabilitarInfoEmpresa == 1){
												HabilitarInfoEmpresa.bootstrapToggle('on');
											} else {
												HabilitarInfoEmpresa.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoMensalista

											if(cache.HabilitarInfoMensalista == 1){
												HabilitarInfoMensalista.bootstrapToggle('on');
											} else {
												HabilitarInfoMensalista.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoMensalidade

											if(cache.HabilitarInfoMensalidade == 1){
												HabilitarInfoMensalidade.bootstrapToggle('on');
											} else {
												HabilitarInfoMensalidade.bootstrapToggle('off');
											}

										// Habilita / Desabilita InfoCaixa

											if(cache.HabilitarInfoCaixa == 1){
												HabilitarInfoCaixa.bootstrapToggle('on');
											} else {
												HabilitarInfoCaixa.bootstrapToggle('off');
											}
										
									}

								// Fecha oo sweet alert

									swal.close();

								// Grava o Cache

									localStorage.setItem("ConfigCupomMensalidade", JSON.stringify(cache));

							}

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Configurações

		function CarregaConfigSMSCupom(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cache = JSON.parse(localStorage.getItem("ConfigSMSCupom"));

			// Declaração das Variaveis

				var Funcao = 2;
				var CodConfigSMSCupom = $("input[name='CodConfigSMSCupom']");
				var MensagemCupomAvulsoEntrada = $("textarea[name='ConfigSMSCupom_MensagemCupomAvulsoEntrada']");
				var MensagemCupomAvulsoSaida = $("textarea[name='ConfigSMSCupom_MensagemCupomAvulsoSaida']");
				var MensagemCupomMensalidade = $("textarea[name='ConfigSMSCupom_MensagemCupomMensalidade']");
				var LabelTotalCaracteres_MensagemCupomAvulsoEntrada = $("#label-ConfigSMSCupom_MensagemCupomAvulsoEntrada-totalCaracteres");
				var LabelTotalCaracteres_MensagemCupomAvulsoSaida = $("#label-ConfigSMSCupom_MensagemCupomAvulsoSaida-totalCaracteres");
				var LabelTotalCaracteres_MensagemCupomMensalidade = $("#label-ConfigSMSCupom_MensagemCupomMensalidade-totalCaracteres");

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao
					},
					url: 'ajax/configSMSCupom_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Configurações de Mensagens para Cupom via SMS',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se o retorno esta vazio

							if(retorno == undefined || retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})
								return
							} else {

								// Declara as Variaveis

									var TotalConfigSMSCupom = retorno.ConfigSMSCupom.length;
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroConfigSMSCupom=0;PonteiroConfigSMSCupom<TotalConfigSMSCupom;PonteiroConfigSMSCupom++){
									
										// Grava as informações na array cache

											cache = retorno.ConfigSMSCupom[PonteiroConfigSMSCupom];
											cache.MensagemCupomAvulsoEntrada = TrataTexto(cache.MensagemCupomAvulsoEntrada);
											cache.MensagemCupomAvulsoSaida = TrataTexto(cache.MensagemCupomAvulsoSaida);
											cache.MensagemCupomMensalidade = TrataTexto(cache.MensagemCupomMensalidade);

										// Seta os valores dos campos

											CodConfigSMSCupom.val(cache.Codigo);
											MensagemCupomAvulsoEntrada.val(cache.MensagemCupomAvulsoEntrada);
											MensagemCupomAvulsoSaida.val(cache.MensagemCupomAvulsoSaida);
											MensagemCupomMensalidade.val(cache.MensagemCupomMensalidade);

										// Declara as Variaveis

											var TotalCaracteres_MensagemCupomAvulsoEntrada = (MensagemCupomAvulsoEntrada.val()).length;
											var UtilizaVariaveis_MensagemCupomAvulsoEntrada = false;
											var QuaisVariaveis_MensagemCupomAvulsoEntrada = [];
											var Maxlength_MensagemCupomAvulsoEntrada = MensagemCupomAvulsoEntrada.attr("data-maxlength");

											var TotalCaracteres_MensagemCupomAvulsoSaida = (MensagemCupomAvulsoSaida.val()).length;
											var UtilizaVariaveis_MensagemCupomAvulsoSaida = false;
											var QuaisVariaveis_MensagemCupomAvulsoSaida = [];
											var Maxlength_MensagemCupomAvulsoSaida = MensagemCupomAvulsoSaida.attr("data-maxlength");

											var TotalCaracteres_MensagemCupomMensalidade = (MensagemCupomMensalidade.val()).length;
											var UtilizaVariaveis_MensagemCupomMensalidade = false;
											var QuaisVariaveis_MensagemCupomMensalidade = [];
											var Maxlength_MensagemCupomMensalidade = MensagemCupomMensalidade.attr("data-maxlength");

											var Variaveis_MensagemCupomAvulsoEntrada = Object.keys(cupomSMS.MensagemCupomAvulsoEntrada.variaveis);
											var Variaveis_MensagemCupomAvulsoSaida = Object.keys(cupomSMS.MensagemCupomAvulsoSaida.variaveis);
											var Variaveis_MensagemCupomMensalidade = Object.keys(cupomSMS.MensagemCupomMensalidade.variaveis);

										// Verifica se á Variaveis pré definidas na mensagem

											function cupomSMS_MensagemCupomAvulsoEntrada_forEach(element, index, array) {
											    if((MensagemCupomAvulsoEntrada.val()).indexOf(element) > -1){
											    	UtilizaVariaveis_MensagemCupomAvulsoEntrada = true;
											    	TotalCaracteres_MensagemCupomAvulsoEntrada = TotalCaracteres_MensagemCupomAvulsoEntrada - element.length;
											    	TotalCaracteres_MensagemCupomAvulsoEntrada += 1;
											    	QuaisVariaveis_MensagemCupomAvulsoEntrada.push(element);
											    }
											}

											function cupomSMS_MensagemCupomAvulsoSaida_forEach(element, index, array) {
											    if((MensagemCupomAvulsoSaida.val()).indexOf(element) > -1){
											    	UtilizaVariaveis_MensagemCupomAvulsoSaida = true;
											    	TotalCaracteres_MensagemCupomAvulsoSaida = TotalCaracteres_MensagemCupomAvulsoSaida - element.length;
											    	TotalCaracteres_MensagemCupomAvulsoSaida += 1;
											    	QuaisVariaveis_MensagemCupomAvulsoSaida.push(element);
											    }
											}

											function cupomSMS_MensagemCupomMensalidade_forEach(element, index, array) {
											    if((MensagemCupomMensalidade.val()).indexOf(element) > -1){
											    	UtilizaVariaveis_MensagemCupomMensalidade = true;
											    	TotalCaracteres_MensagemCupomMensalidade = TotalCaracteres_MensagemCupomMensalidade - element.length;
											    	TotalCaracteres_MensagemCupomMensalidade += 1;
											    	QuaisVariaveis_MensagemCupomMensalidade.push(element);
											    }
											}

											Variaveis_MensagemCupomAvulsoEntrada.forEach(cupomSMS_MensagemCupomAvulsoEntrada_forEach);
											Variaveis_MensagemCupomAvulsoSaida.forEach(cupomSMS_MensagemCupomAvulsoSaida_forEach);
											Variaveis_MensagemCupomMensalidade.forEach(cupomSMS_MensagemCupomMensalidade_forEach);

										// Verifica se a mensagem de entrada avulsa contém variaveis

											if(UtilizaVariaveis_MensagemCupomAvulsoEntrada){
												
												// Exibe a div que contem as variaveis utilizadas na mensagem

													$("#div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada").show();
													$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada").html("");

												// Realiza o foreach

													function quaisVariaveis_forEach(element, index, array) {
														if((QuaisVariaveis_MensagemCupomAvulsoEntrada.length-1) == index){
															var label = "<label class='label label-primary' data-toggle='tooltip' title='"+cupomSMS.MensagemCupomAvulsoEntrada.variaveis[element]+"'>"+element+"</label>"
														} else {
															var label = "<label class='label label-primary' data-toggle='tooltip' title='"+cupomSMS.MensagemCupomAvulsoEntrada.variaveis[element]+"'>"+element+"</label> ; "
														}
														var htmlAntigo = $("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada").html();
														$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada").html(htmlAntigo+label);
													}

													QuaisVariaveis_MensagemCupomAvulsoEntrada.forEach(quaisVariaveis_forEach);

											} else {

												$("#div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada").hide();
												$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada").html("");
											}

										// Verifica se a mensagem de saida da entrada avulsa contém variaveis

											if(UtilizaVariaveis_MensagemCupomAvulsoSaida){
												
												// Exibe a div que contem as variaveis utilizadas na mensagem

													$("#div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida").show();
													$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida").html("");

												// Realiza o foreach

													function quaisVariaveis_forEach(element, index, array) {
														if((QuaisVariaveis_MensagemCupomAvulsoSaida.length-1) == index){
															var label = "<label class='label label-primary' data-toggle='tooltip' title='"+cupomSMS.MensagemCupomAvulsoSaida.variaveis[element]+"'>"+element+"</label>"
														} else {
															var label = "<label class='label label-primary' data-toggle='tooltip' title='"+cupomSMS.MensagemCupomAvulsoSaida.variaveis[element]+"'>"+element+"</label> ; "
														}
														var htmlAntigo = $("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida").html();
														$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida").html(htmlAntigo+label);
													}

													QuaisVariaveis_MensagemCupomAvulsoSaida.forEach(quaisVariaveis_forEach);

											} else {

												$("#div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida").hide();
												$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida").html("");
											}

										// Verifica se a mensagem de mensalidade contém variaveis

											if(UtilizaVariaveis_MensagemCupomMensalidade){
												
												// Exibe a div que contem as variaveis utilizadas na mensagem

													$("#div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade").show();
													$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade").html("");

												// Realiza o foreach

													function quaisVariaveis_forEach(element, index, array) {
														if((QuaisVariaveis_MensagemCupomMensalidade.length-1) == index){
															var label = "<label class='label label-primary' data-toggle='tooltip' title='"+cupomSMS.MensagemCupomMensalidade.variaveis[element]+"'>"+element+"</label>"
														} else {
															var label = "<label class='label label-primary' data-toggle='tooltip' title='"+cupomSMS.MensagemCupomMensalidade.variaveis[element]+"'>"+element+"</label> ; "
														}
														var htmlAntigo = $("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade").html();
														$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade").html(htmlAntigo+label);
													}

													QuaisVariaveis_MensagemCupomMensalidade.forEach(quaisVariaveis_forEach);

											} else {

												$("#div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade").hide();
												$("#variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade").html("");
											}

										// Verifica se o campo MensagemCupomAvulsoEntrada possui o atributo de maxlength

											if(Maxlength_MensagemCupomAvulsoEntrada != undefined && Maxlength_MensagemCupomAvulsoEntrada != null && Maxlength_MensagemCupomAvulsoEntrada != "" && Maxlength_MensagemCupomAvulsoEntrada != " "){
												Maxlength_MensagemCupomAvulsoEntrada = parseInt(Maxlength_MensagemCupomAvulsoEntrada);
												MensagemCupomAvulsoEntrada.attr("maxlength", Maxlength_MensagemCupomAvulsoEntrada);
											} else {
												MensagemCupomAvulsoEntrada.removeAttr("maxlength");
											}

										// Verifica se o campo MensagemCupomAvulsoSaida possui o atributo de maxlength

											if(Maxlength_MensagemCupomAvulsoSaida != undefined && Maxlength_MensagemCupomAvulsoSaida != null && Maxlength_MensagemCupomAvulsoSaida != "" && Maxlength_MensagemCupomAvulsoSaida != " "){
												Maxlength_MensagemCupomAvulsoSaida = parseInt(Maxlength_MensagemCupomAvulsoSaida);
												MensagemCupomAvulsoSaida.attr("maxlength", Maxlength_MensagemCupomAvulsoSaida);
											} else {
												MensagemCupomAvulsoSaida.removeAttr("maxlength");
											}

										// Verifica se o campo possui o atributo de maxlength

											if(Maxlength_MensagemCupomMensalidade != undefined && Maxlength_MensagemCupomMensalidade != null && Maxlength_MensagemCupomMensalidade != "" && Maxlength_MensagemCupomMensalidade != " "){
												Maxlength_MensagemCupomMensalidade = parseInt(Maxlength_MensagemCupomMensalidade);
												MensagemCupomMensalidade.attr("maxlength", Maxlength_MensagemCupomMensalidade);
											} else {
												MensagemCupomMensalidade.removeAttr("maxlength");
											}

										// Seta os valore das Labels

											LabelTotalCaracteres_MensagemCupomAvulsoEntrada.html(TotalCaracteres_MensagemCupomAvulsoEntrada);
											LabelTotalCaracteres_MensagemCupomAvulsoSaida.html(TotalCaracteres_MensagemCupomAvulsoSaida);
											LabelTotalCaracteres_MensagemCupomMensalidade.html(TotalCaracteres_MensagemCupomMensalidade);
										
									}

								// Fecha oo sweet alert

									swal.close();

								// Grava o Cache

									localStorage.setItem("ConfigSMSCupom", JSON.stringify(cache));

							}

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Informações do Estacionamento

		function CarregaConfiguracoesSMS(){

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica se ja existe cache

				var cachePaginacao = JSON.parse(localStorage.getItem("ConfigSMSPaginacao"));
				var cache = JSON.parse(localStorage.getItem("ConfigSMS"));

			// Declaração das Variaveis

				var Funcao = 5;
				var RegistrosPorPaginas = 5;
				var PaginaRegistro = 0;
				if(cachePaginacao == null){
					PaginaRegistro = 0;
				} else if(cachePaginacao.hasOwnProperty("paginaAtual")){
					if(cachePaginacao.paginaAtual > 0){
						PaginaRegistro = (cachePaginacao.paginaAtual * RegistrosPorPaginas) - 5;
					} else {
						PaginaRegistro = 0;
					}
				}

			// Realiza a chamada do ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'Funcao' : Funcao,
						'RegistrosPorPaginas' : RegistrosPorPaginas,
						'PaginaRegistro' : PaginaRegistro
					},
					url: 'ajax/configSMS_ajax.asp',
					beforeSend: function(data){
						swal({
							title: 'Carregando Configurações de SMS',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},
					success: function(retorno) {

						// Converte o retorno de HTML para JSON
		
							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							if(retorno.hasOwnProperty("Status")){
								swal.close();
								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								})

								return
							}
						
						// Declara as Variaveis
						
							var TotalConfigSMS = parseInt(retorno.TotalConfigSMS);
							var PonteiroConfigSMS = 0;
							var Codigo = 0;
							var Descricao = 0;
							var LinkAPI = 0;
							var LinhaHTML = 0;
							var AntigoHTML = 0;
							var TotalRegistrosPagina = retorno.ConfigSMS.length;
						
						// Remove todas as Pessoas Fisicas
						
							$("#registrosConfigSMS").html("");
						
						// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
				
							for(PonteiroConfigSMS=0;PonteiroConfigSMS<TotalRegistrosPagina;PonteiroConfigSMS++){
							
								Codigo = retorno.ConfigSMS[PonteiroConfigSMS].Codigo;
								Descricao = retorno.ConfigSMS[PonteiroConfigSMS].Descricao;
								LinkAPI = retorno.ConfigSMS[PonteiroConfigSMS].LinkAPI;
								Situacao = parseInt(retorno.ConfigSMS[PonteiroConfigSMS].Situacao);

								LinhaHTML = "<tr>"
								LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
								LinhaHTML += "<td class='text-center'>" + Descricao + "</td>"
								LinhaHTML += "<td class='text-center'>" + LinkAPI + "</td>"
								LinhaHTML += "<td class='text-center'>"
								if(Situacao == 1){
									LinhaHTML += "<label class='label label-success'>Ativado</label>"
								} else {
									LinhaHTML += "<label class='label label-danger'>Desativado</label>"
								}
								LinhaHTML += "</td>"
								LinhaHTML += "<td class='text-center'>"
								if(Situacao == 1){
									LinhaHTML += "<button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-onoff-configSMS' data-configSMS='"+JSON.stringify(retorno.ConfigSMS[PonteiroConfigSMS])+"' data-placement='left' title='Desativar'><i class='fa fa-toggle-off'></i></button>"
								} else {
									LinhaHTML += "<button type='button' class='btn btn-xs btn-success' data-toggle='tooltip' id='btn-onoff-configSMS' data-configSMS='"+JSON.stringify(retorno.ConfigSMS[PonteiroConfigSMS])+"' data-placement='left' title='Ativar'><i class='fa fa-toggle-on'></i></button>"
								}
								LinhaHTML += " | <button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-configSMS' data-configSMS='"+JSON.stringify(retorno.ConfigSMS[PonteiroConfigSMS])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
								LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-configSMS' data-configSMS='"+JSON.stringify(retorno.ConfigSMS[PonteiroConfigSMS])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
								LinhaHTML += "</td>"
								LinhaHTML += "</tr>"
								
								AntigoHTML = $("#registrosConfigSMS").html();
								$("#registrosConfigSMS").html(AntigoHTML+LinhaHTML);
								
							}

						// Verifica se existe o indice e caso sim insere os tipos de veiculos

							cachePaginacao.totalRegistros = TotalConfigSMS;
							cachePaginacao.registrosPagina = TotalRegistrosPagina;
							cache = retorno.ConfigSMS;

						// Fecha o SweetAlert

							swal.close();

						// Grava o Cache

							localStorage.setItem("ConfigSMSPaginacao", JSON.stringify(cachePaginacao));
							localStorage.setItem("ConfigSMS", JSON.stringify(cache));

	        			// Gera a Paginacao

							geraEstruturaPaginacao("#pgn-configSMS", "ConfigSMSPaginacao");

					}
				});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao finalizar o carregamento da pagina

		$(document).ready(()=>{

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			$("input[name='DuracaoFracao']").mask('99:99');
			$("input[name='DuracaoTolerancia']").mask('99:99');

	        gerenciaMenu();
	        
	        CarregaTipoAutenticacaoCache();
	        CarregaTipoVeiculoCache();
	        CarregaPlanoCache();
	        CarregaTipoUsuarioCache();
	        
	        CarregaConfiguracoes();

			$("input[name='CNPJ']").mask("00.000.000/0000-00");

			$("input[name='TelefoneResidencial']").mask(SPMaskBehavior, spOptions);
			$("input[name='TelefoneComercial']").mask(SPMaskBehavior, spOptions);
			$("input[name='TelefoneCorporativo']").mask(SPMaskBehavior, spOptions);
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao fechar ou ir para outra pagina

		$(window).on('beforeunload', ()=>{
		   localStorage.clear();
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das configurações geral

		$(document).on("click", "#btn-tab-geral", ()=>{

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}


			$("input[name='CNPJ']").mask("00.000.000/0000-00");

			$("input[name='TelefoneResidencial']").mask(SPMaskBehavior, spOptions);
			$("input[name='TelefoneComercial']").mask(SPMaskBehavior, spOptions);
			$("input[name='TelefoneCorporativo']").mask(SPMaskBehavior, spOptions);

			CarregaConfiguracoes();
			reiniciaBootstrapToogle("input[name='HabilitarFracao']");
			reiniciaBootstrapToogle("input[name='HabilitarTolerancia']");
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a Consulta do CEP

		$(document).on('change', "input[name='CEP']", ()=>{

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}


			// Carrega o CEP

				var CEP = $("input[name='CEP']");
				var Bairro = $("input[name='Bairro']");
				var Endereco = $("input[name='Endereco']");
				var Cidade = $("input[name='Cidade']");
				var UF = $("input[name='UF']");
				var ChecaCEP = CEP.val();

			// Trata o CEP recebido

				ChecaCEP = ChecaCEP.replace(/\-/g,"");
				ChecaCEP = ChecaCEP.replace(/\./g,"");

			// Verifica se o cep possui 8 digitos

				if(ChecaCEP.length == 8){

					$.ajax({
						type: "get",
						dataType: "html",
						url: "https://viacep.com.br/ws/"+ChecaCEP+"/json/",
						beforeSend: function(data){
							swal({
								title: 'Consultando CEP',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								var CEPArray = jQuery.parseJSON(retorno);

							// Define o valor dos campos

								Bairro.val(CEPArray.bairro);
								Endereco.val(CEPArray.logradouro);
								Cidade.val(CEPArray.localidade);
								UF.val(CEPArray.uf);

							// Fecha o SweetAlert

								swal.close();

						}
					});

				} else {

					// Exibe Mensagem de Erro

						toastr.error('O CEP especificado esta incorreto, verifique e tente novamente', 'Ops!');

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Atualiza as Configurações Geral

		$(document).on("click", "#btn-tab-geral-salvar", ()=>{

			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}


			// Declaração das Variaveis

				var CodConfiguracoes = $("input[name='CodConfiguracoes']");
				var Razao = $("input[name='Razao']");
				var CNPJ = $("input[name='CNPJ']");
				var TelefoneResidencial = $("input[name='TelefoneResidencial']");
				var TelefoneComercial = $("input[name='TelefoneComercial']");
				var TelefoneCorporativo = $("input[name='TelefoneCorporativo']");
				var Email = $("input[name='Email']");
				var CEP = $("input[name='CEP']");
				var Bairro = $("input[name='Bairro']");
				var Endereco = $("input[name='Endereco']");
				var Cidade = $("input[name='Cidade']");
				var UF = $("input[name='UF']");
				var Numero = $("input[name='Numero']");
				var HabilitarFracao = $("input[name='HabilitarFracao']");
				var DuracaoFracao = $("input[name='DuracaoFracao']");
				var HabilitarTolerancia = $("input[name='HabilitarTolerancia']");
				var DuracaoTolerancia = $("input[name='DuracaoTolerancia']");
				var HabilitarSMS = $("input[name='HabilitarSMS']");
				var Funcao = 1;

			// Realiza as validações

				var validacao = []
					validacao[0] = HabilitarFracao.prop("checked") ? HabilitarFracao.prop("checked") && DuracaoFracao.val() != "" : true
					validacao[1] = HabilitarTolerancia.prop("checked") ? HabilitarTolerancia.prop("checked") && DuracaoTolerancia.val() != "" : true

				var verificaValidacao = validacao[0] && validacao[1];

			// Retira as Classes de Erros

				if(validacao[0]){
					$("#div_DuracaoFracao").removeClass("has-error");
				}
				
				if(validacao[1]){
					$("#div_DuracaoTolerancia").removeClass("has-error");
				}

			// Confere a validação

				if(verificaValidacao == true){

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'Codigo' : CodConfiguracoes.val(),
								'Razao' : Razao.val(),
								'CNPJ' : CNPJ.val(),
								'TelefoneResidencial' : TelefoneResidencial.val(),
								'TelefoneComercial' : TelefoneComercial.val(),
								'TelefoneCorporativo' : TelefoneCorporativo.val(),
								'Email' : Email.val(),
								'CEP' : CEP.val(),
								'Bairro' : Bairro.val(),
								'Endereco' : Endereco.val(),
								'Cidade' : Cidade.val(),
								'UF' : UF.val(),
								'Numero' : Numero.val(),
								'HabilitarFracao' : HabilitarFracao.prop("checked") == true ? 1 : 0,
								'DuracaoFracao' : DuracaoFracao.val(),
								'HabilitarTolerancia' : HabilitarTolerancia.prop("checked") == true ? 1 : 0,
								'DuracaoTolerancia' : DuracaoTolerancia.val(),
								'HabilitarSMS' : HabilitarSMS.prop("checked") == true ? 1 : 0,
								'Funcao' : Funcao
							},
							url: 'ajax/configuracoes_ajax.asp',
							beforeSend: function(data){
								swal({
									title: 'Atualizando Configurações',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								}).then(()=>{

									// Chama a função para Carregar as Configurações
									
										CarregaConfiguracoes();
								
								})

							}

						});

				} else {

					if(!validacao[0]){
						$("#div_DuracaoFracao").addClass("has-error");
						toastr.error('A Duração da Fração precisa ser especificada', 'Ops!');
					}
					
					if(!validacao[1]){
						$("#div_DuracaoTolerancia").addClass("has-error");
						toastr.error('A Duração da Tolerância precisa ser especificada', 'Ops!');
					}

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});