//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica se Existe a sessão de usuário ativa

		var SessaoUsuario = sessionStorage.getItem("Usuario");
		var SessaoTipoUsuario = sessionStorage.getItem("TipoUsuario");
		var SessaoNome = sessionStorage.getItem("Nome");
		var SessaoEmail = sessionStorage.getItem("Email");
		var SessaoToken = sessionStorage.getItem("Token");
		var SessaoPermissoes = sessionStorage.getItem("Permissoes");

		if(SessaoNome != undefined && SessaoEmail != undefined && SessaoToken != undefined && SessaoUsuario != undefined && SessaoTipoUsuario != undefined && SessaoPermissoes != undefined){

			// Redireciona para a pagina principal

				window.location.href = 'estacionamento.asp';

		} else {

			// Limpa o Session Stoage e LocalStorage

				localStorage.clear()
				sessionStorage.clear()

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------