//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das configurações geral

		$(document).on("click", "#btn-tab-tipoUsuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Renderiza as Modals
				
				renderizaModal("#modal_cad_TipoUsuario", "<span class='text-muted'><i class='fa fa-plus'></i> Cadastrar Novo Tipo de Usuário</span>", {width: 300});
				renderizaModal("#modal_alt_TipoUsuario", "<span class='text-muted'><i class='fa fa-pencil'></i> Editar Tipo de Usuário</span>");

			// Carrega os planos

				CarregaTipoUsuario();

		});
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a troca de paginas da tabela de Entrada Avulsa

		$(document).on('click', "#btn-pgn-tiposUsuarios", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Chama a função para troca de paginas

				trocaPagina(this, "TiposUsuariosPaginacao");

			// Carrega as Entradas Avulsas
		
				CarregaTiposUsuarios();
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Abre a Modal de Cadastro de Vaga

		$(document).on("click", "#btn-cad-tipoUsuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Limpa o Formulario

				limpaFormulario("#form_cad_TipoUsuario");

			// Limpa as Classes de Erros

				$("#modal_cad_tipoUsuario_div_Descricao").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_cad_TipoUsuario");

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_tipoUsuario_input_ValorFracao

		$(document).on('change', "input[name='modal_cad_tipoUsuario_input_Descricao']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_TipoUsuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Descricao = $("input[name='modal_cad_tipoUsuario_input_Descricao']");

					// Realiza a Validação

						if(Descricao.val() != undefined && Descricao.val() != null && Descricao.val() != "" && Descricao.val() != " "){
							$("#modal_cad_tipoUsuario_div_Descricao").removeClass("has-error");
						} else {
							toastr.error('A Descrição precisa ser preenchida', 'Ops!');
							$("#modal_cad_tipoUsuario_div_Descricao").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Abre a Modal de Cadastro de Vaga

		$(document).on("click", "#btn-modal-cad_tipoUsuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as variaveis
				
				var Descricao = $("input[name='modal_cad_tipoUsuario_input_Descricao']");
				var BotaoCadTipoUsuario = $("#btn-modal-cad_tipoUsuario");
				var Funcao = 1;

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Descricao.val() != undefined && Descricao.val() != null && Descricao.val() != "" && Descricao.val() != " ";

				var verificaValidacao = validacao[0];

			// Desativa o botão

				BotaoCadTipoUsuario.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'Descricao' : Descricao.val(),
							'Funcao' : Funcao
						},
						url: 'ajax/tipoUsuario_ajax.asp',
						beforeSend: function(data){
							swal({
								title: 'Cadastrando Tipo de Usuário',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								if(retorno.hasOwnProperty("Status")){
									swal.close();
									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									})

									return
								}

							// Promisse do sweet alert

								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_cad_TipoUsuario");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaTipoUsuario();

									// Ativa o botão modal-tipoUsuario

										BotaoCadTipoUsuario.prop("disabled", false);
								
								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_cad_tipoUsuario_div_Descricao").addClass("has-error");
							toastr.error('A Descrição precisa ser preenchida', 'Ops!');
						}

									
					// Ativa o Botão
					
						BotaoCadTipoUsuario.prop("disabled", false);

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Abre a Modal de Cadastro de Vaga

		$(document).on("click", "#btn-alt-tipoUsuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Pega os Dados do Tipo de Usuário

				var DadosTipoUsuario = JSON.parse(this.activeElement.getAttribute("data-tipoUsuario"));

			// Limpa o Formulario

				limpaFormulario("#form_alt_TipoUsuario");

			// Limpa as Classes de Erros

				$("#modal_alt_tipoUsuario_div_Descricao").removeClass("has-error");
				$("input[name='modal_alt_tipoUsuario_input_Codigo']").val(DadosTipoUsuario.Codigo);
				$("input[name='modal_alt_tipoUsuario_input_Descricao']").val(DadosTipoUsuario.Descricao);

			// Abre a Modal

				abriraModal("#modal_alt_TipoUsuario");

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_tipoUsuario_input_ValorFracao

		$(document).on('change', "input[name='modal_alt_tipoUsuario_input_Descricao']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_TipoUsuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Descricao = $("input[name='modal_alt_tipoUsuario_input_Descricao']");

					// Realiza a Validação

						if(Descricao.val() != undefined && Descricao.val() != null && Descricao.val() != "" && Descricao.val() != " "){
							$("#modal_alt_tipoUsuario_div_Descricao").removeClass("has-error");
						} else {
							toastr.error('A Descrição precisa ser preenchida', 'Ops!');
							$("#modal_alt_tipoUsuario_div_Descricao").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Abre a Modal de Cadastro de Vaga

		$(document).on("click", "#btn-modal-alt_tipoUsuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as variaveis

				var Codigo = $("input[name='modal_alt_tipoUsuario_input_Codigo']");
				var Descricao = $("input[name='modal_alt_tipoUsuario_input_Descricao']");
				var BotaoCadTipoUsuario = $("#btn-modal-alt_tipoUsuario");
				var Funcao = 2;

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Descricao.val() != undefined && Descricao.val() != null && Descricao.val() != "" && Descricao.val() != " ";

				var verificaValidacao = validacao[0];

			// Desativa o botão

				BotaoCadTipoUsuario.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'Codigo' : Codigo.val(),
							'Descricao' : Descricao.val(),
							'Funcao' : Funcao
						},
						url: 'ajax/tipoUsuario_ajax.asp',
						beforeSend: function(data){
							swal({
								title: 'Alterando Tipo de Usuário',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								if(retorno.hasOwnProperty("Status")){
									swal.close();
									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									})

									return
								}

							// Promisse do sweet alert

								swal({
									title: retorno.MensagemTitulo,
									text: retorno.MensagemTexto,
									type: retorno.MensagemTipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_alt_TipoUsuario");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaTipoUsuario();

									// Ativa o botão modal-tipoUsuario

										BotaoCadTipoUsuario.prop("disabled", false);
								
								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_alt_tipoUsuario_div_Descricao").addClass("has-error");
							toastr.error('A Descrição precisa ser preenchida', 'Ops!');
						}

									
					// Ativa o Botão
					
						BotaoCadTipoUsuario.prop("disabled", false);

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Entrada Avulsa

		$(document).on('click', "#btn-del-tipoUsuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}
			
			// Verificação de Permissão de Modulo

				if(AcessoDesenvolvedor(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as variaveis
				
				var DadosTipoUsuario = JSON.parse(this.activeElement.getAttribute("data-tipoUsuario"));
				var Codigo = DadosTipoUsuario.Codigo;
				var Funcao = 3;

			// Verifica se realmente irá excluir o registro e se for verdadeira executa o ajax

				swal({
					title: 'Deseja prosseguir com a exclusão deste Tipo de Usuário?',
					text: "Lembramos que após excluir, não havera volta!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Deletar',
					cancelButtonText: 'Cancelar',
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: true,
					allowOutsideClick: false
				}).then(function(data){

					if (data.value != undefined) {

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'Codigo' : Codigo,
								'Funcao' : Funcao
							},
							url: 'ajax/tipoUsuario_ajax.asp',
							beforeSend: function(data){
								swal({
									title: 'Excluindo Tipo de Usuário',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									}).then(()=>{

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaTipoUsuario();
									
									})
							}
						});

					} 

				});
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

})