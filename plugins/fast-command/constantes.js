// Constante com estrutura da Sessão

	const Sessao = {
		Usuario: sessionStorage.getItem("Usuario"),
		TipoUsuario: sessionStorage.getItem("TipoUsuario"),
		Nome: sessionStorage.getItem("Nome"),
		Email: sessionStorage.getItem("Email"),
		Token: sessionStorage.getItem("Token"),
		ImagemPerfil: sessionStorage.getItem("ImagemPerfil"),
		Permissoes: JSON.parse(sessionStorage.getItem("Permissoes")),
		checaSessao: null
	}

// Constante com estrutura do cache do Menu

	const menu = {
		estacionamento : {
			active : false,
			id : "#menu-estacionamento"
		},
		mensalistas : {
			active : false,
			id : "#menu-mensalistas"
		},
		entradaavulsa : {
			active : false,
			id : "#menu-entradaAvulsa"
		},
		configuracoes : {
			active : false,
			id : "#menu-configuracoes"
		},
		relatorios : {
			active : false,
			id : "#menu-relatorios"
		},
		fluxocaixa : {
			active : false,
			id : "#menu-fluxoCaixa"
		}
		// configuracoes : {
		// 	active : false,
		// 	id : "#menu-configuracoes",
		// 	submenu : {	
		// 		geral : {
		// 			active : false,
		// 			id : "#menu-configuracoes_geral"
		// 		},
		// 		estacionamento : {
		// 			active : false,
		// 			id : "#menu-configuracoes_estacionamento"
		// 		},
		// 	}
		// }
	}

// Constante com estrutura do cache do Cupom Fiscal Avulso

	const cupomFiscalAvulso = {
		dadosEmpresa : {
			nome_estacionamento: "",
			razao : "",
			cnpj : "",
			contato : "",
			cep : "",
			bairro : "",
			cidade : "",
			endereco : ""
		},
		dadosVeiculo : {
			codigo : "",
			descricao : "",
			placa : "",
			tipoVeiculo : ""
		},
		dadosServico : {
			emissao : "",
			tipo : "",
			entrada : "",
			saida : "",
			tempoTotal : ""
		},
		dadosCaixa : {
			formaPagamento : "",
			total : "",
			pago : "",
			troco : ""
		},
		configCupom : {}
	}

// Constante com estrutura do cache do Cupom Fiscal Avulso

	const cupomFiscalMensalidade = {
		dadosEmpresa : {
			razao : "",
			cnpj : "",
			contato : "",
			cep : "",
			bairro : "",
			cidade : "",
			endereco : ""
		},
		dadosMensalista : {
			codigo : "",
			nomerazao : "",
			cpfcnpj : ""
		},
		dadosMensalidade : {
			mensalidademes : "",
			mensalidadeano : "",
			datavencimento : "",
			principal : ""
		},
		dadosCaixa : {
			datapagamento : "",
			desconto : "",
			multa : "",
			total : "",
			formaPagamento : "",
			pago : "",
			troco : ""
		},
		configCupom : {}
	}

// Constante com estrutura das Variaveis que poderão ser utilizadas nas mensagens de cupons via sms

	const cupomSMS = {
		Funcoes: {
			variaveis: {	
				"---" : "Delimitador de SMS", 
			}
		},
		MensagemCupomAvulsoEntrada : {
			variaveis : {
				"[codigo]" : "Código da Entrada Avulsa",
				"[nome_estacionamento]" : "Nome do Estacionamento",
				"[descricao_veiculo]" : "Descrição do Veículo inserida na Entrada Avulsa",
				"[tipo_veiculo]" : "Tipo do Veículo inserido na Entrada Avulsa",
				"[placa]" : "Placa do Veículo inserida na Entrada Avulsa",
				"[entrada]" : "Data e Horário da Entrada do Veículo inserida na Entrada Avulsa"
			},
			dePara : {
				"[codigo]" : "dadosVeiculo.codigo",
				"[nome_estacionamento]" : "dadosEmpresa.nome_estacionamento",
				"[descricao_veiculo]" : "dadosVeiculo.descricao",
				"[tipo_veiculo]" : "dadosVeiculo.tipoVeiculo",
				"[placa]" : "dadosVeiculo.placa",
				"[entrada]" : "dadosServico.entrada"
			}
		},
		MensagemCupomAvulsoSaida : {
			variaveis : {
				"[codigo]" : "Código da Entrada Avulsa",
				"[nome_estacionamento]" : "Nome do Estacionamento",
				"[descricao_veiculo]" : "Descrição do Veículo inserida na Entrada Avulsa",
				"[tipo_veiculo]" : "Tipo do Veículo inserido na Entrada Avulsa",
				"[placa]" : "Placa do Veículo inserida na Entrada Avulsa",
				"[entrada]" : "Data e Horário da Entrada do Veículo inserida na Entrada Avulsa",
				"[saida]" : "Data e Horário da Saída do Veículo inserida na Entrada Avulsa",
				"[tempo_total]" : "Tempo Total no qual o Veículo ficou no estacionamento",
				"[troco]" : "Valor do Troco",
				"[pago]" : "Valor pago pelo cliente",
				"[total]" : "Total que o cliente deverá pagar",
				"[forma_pagamento]" : "Forma de Pagamento que o cliente utilizou"
			},
			dePara : {
				"[codigo]" : "dadosVeiculo.codigo",
				"[nome_estacionamento]" : "dadosEmpresa.nome_estacionamento",
				"[descricao_veiculo]" : "dadosVeiculo.descricao",
				"[tipo_veiculo]" : "dadosVeiculo.tipoVeiculo",
				"[placa]" : "dadosVeiculo.placa",
				"[entrada]" : "dadosServico.entrada",
				"[saida]" : "dadosServico.saida",
				"[tempo_total]" : "dadosServico.tempoTotal",
				"[troco]" : "dadosCaixa.troco",
				"[pago]" : "dadosCaixa.pago",
				"[total]" : "dadosCaixa.total",
				"[forma_pagamento]" : "dadosCaixa.formaPagamento",
			}
		},
		MensagemCupomMensalidade : {
			variaveis : {
				"[mensalista]" : "Nome ou Razão Social do Mensalista",
				"[nome_estacionamento]" : "Nome do Estacionamento",
				"[mensalidade_mes]" : "Mês da Mensalidade que o cliente irá pagar",
				"[mensalidade_ano]" : "Ano da Mensalidade que o cliente irá pagar",
				"[data_vencimento]" : "Data do vecimento da mensalidade",
				"[data_pagamento]" : "Data em que foi realizado o pagamento da mensalidade",
				"[principal]" : "Valor total da mensalidade sem desconto e sem acréscimo de jurós e multa",
				"[desconto]" : "Desconto aplicado na mensalidade",
				"[juros]" : "Júros aplicado na mensalidade",
				"[multa]" : "Multa aplicado na mensalidade",
				"[forma_pagamento]" : "Forma de Pagamento que foi realizado o pagamento",
				"[troco]" : "Valor do Troco",
				"[pago]" : "Valor pago pelo cliente",
				"[total]" : "Total que o cliente deverá pagar"
			},
			dePara : {
				"[mensalista]" : "dadosMensalista.nomerazao",
				"[nome_estacionamento]" : "",
				"[mensalidade_mes]" : "dadosMensalidade.mensalidademes",
				"[mensalidade_ano]" : "dadosMensalidade.mensalidadeano",
				"[data_vencimento]" : "dadosMensalidade.datavencimento",
				"[data_pagamento]" : "dadosCaixa.datapagamento",
				"[principal]" : "dadosMensalidade.principal",
				"[desconto]" : "dadosCaixa.desconto",
				"[juros]" : "dadosCaixa.juros",
				"[multa]" : "dadosCaixa.multa",
				"[forma_pagamento]" : "dadosCaixa.formaPagamento",
				"[troco]" : "dadosCaixa.troco",
				"[pago]" : "dadosCaixa.pago",
				"[total]" : "dadosCaixa.total"
			}
		}
	}

// Constante com as URLS

	const URLs = {
		cupomFiscalAvulsoEntrada : "cupomFiscalAvulsoEntrada.asp",
		cupomFiscalAvulsoSaida : "cupomFiscalAvulsoSaida.asp",
		cupomFiscalMensalidade : "cupomFiscalMensalidade.asp"
	}

// Constate com Tipos de Imagens aceitaveis por upload

	const upload = {
		Types : ['jpg', 'jpeg', 'png', 'bmp'],
		MaxSize : 5.00
	};

// Constate com parametros da tabela ascii

	const tabelaASCII = {
		LetrasMaiusculas : {
			inicio : 65,
			fim : 90
		},
		LetrasMinusculas : {
			inicio : 97,
			fim : 122
		},
		Numeros : {
			inicio : 48,
			fim : 57
		},
		CaracteresEspeciais1 : {
			inicio : 33,
			fim : 47
		},
		CaracteresEspeciais2 : {
			inicio : 58,
			fim : 64
		},
		CaracteresEspeciais3 : {
			inicio : 91,
			fim : 96
		},
		CaracteresEspeciais4 : {
			inicio : 123,
			fim : 126
		}
	};
