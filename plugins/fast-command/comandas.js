//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                  JavaScript da Página : [PATH]/cardapio.php		   	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerar o cache do Estacionamento

		function geraCache(){
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

				var Lanches = {};

				var Ingredientes = {};

				var Clientes = {};

				var Comandas = {
			    	paginaAtual : 1,
			    	totalPaginas : 1,
			    	registrosPorPagina : 5,
			    	registrosPagina : 0,
			    	totalRegistros : 0,
			    	filtros : {
			    		estruturaPesquisa : "",
			    		estruturaCampos : {}
			    	}
		    	};

			// Grava no localStorage

				localStorage.setItem("Lanches", JSON.stringify(Lanches));
				localStorage.setItem("Ingredientes", JSON.stringify(Ingredientes));
				localStorage.setItem("Clientes", JSON.stringify(Clientes));
				localStorage.setItem("Comandas", JSON.stringify(Comandas));
		}

		geraCache();

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Lanches

		function CarregaLanchesCache(){

			// Define a Promise

				return new Promise((resolve, reject)=>{
			
					// Verificação de Permissão de Modulo

						// if(AcessoModulo(Sessao.Permissoes) == false){
						// 	return false;
						// }

					// Verifica se ja existe cache

						var cache = JSON.parse(localStorage.getItem("Lanches"));

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							url: 'api/lanches',
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								}).then((data)=>{
									reject();
								})
							},		
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Seta o valor total de Lanches

									cache = retorno.lanches;

								// Grava no LocalStorage

			        				localStorage.setItem("Lanches", JSON.stringify(cache));

								// Retorna o resolve

									resolve();

							}
						});

				})

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Ingredientes

		function CarregaIngredientesCache(){

			// Define a Promise

				return new Promise((resolve, reject)=>{
			
					// Verificação de Permissão de Modulo

						// if(AcessoModulo(Sessao.Permissoes) == false){
						// 	return false;
						// }

					// Verifica se ja existe cache

						var cache = JSON.parse(localStorage.getItem("Ingredientes"));

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							url: 'api/ingredientes',
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								}).then((data)=>{
									reject();
								})
							},		
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Seta o valor total de Ingredientes

									cache = retorno.ingredientes;

								// Grava no LocalStorage

			        				localStorage.setItem("Ingredientes", JSON.stringify(cache));

								// Retorna o resolve

									resolve();

							}
						});

				})

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Clientes

		function CarregaClientesCache(){

			// Define a Promise

				return new Promise((resolve, reject)=>{
			
					// Verificação de Permissão de Modulo

						// if(AcessoModulo(Sessao.Permissoes) == false){
						// 	return false;
						// }

					// Verifica se ja existe cache

						var cache = JSON.parse(localStorage.getItem("Clientes"));

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							url: 'api/clientes',
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								}).then((data)=>{
									reject();
								})
							},		
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Seta o valor total de Ingredientes

									cache = retorno.clientes;

								// Grava no LocalStorage

			        				localStorage.setItem("Clientes", JSON.stringify(cache));

								// Retorna o resolve

									resolve();

							}
						});

				})

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar as Comandas

		function CarregaComandas(){

			// Define a Promise

				return new Promise((resolve, reject)=>{
			
					// Verificação de Permissão de Modulo

						// if(AcessoModulo(Sessao.Permissoes) == false){
						// 	return false;
						// }

					// Verifica se ja existe cache

						var cache = JSON.parse(localStorage.getItem("Comandas"));

					// Declaração das Variaveis

						var Filtros = cache.filtros.estruturaCampos;
						var RegistrosPorPaginas = 5;
						var PaginaRegistro = 0;
						if(cache == null){
							PaginaRegistro = 0;
						} else if(cache.hasOwnProperty("paginaAtual")){
							if(cache.paginaAtual > 0){
								PaginaRegistro = (cache.paginaAtual * RegistrosPorPaginas) - 5;
							} else {
								PaginaRegistro = 0;
							}
						}

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'filtros' : (Object.keys(Filtros)).length == 0 ? "" : Filtros,
								'registros_paginas' : RegistrosPorPaginas,
								'pagina_registro' : PaginaRegistro
							},
							url: 'api/comandas',
							beforeSend: function(data){
								swal({
									title: 'Carregando Comandas',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								}).then((data)=>{
									reject();
								})
							},		
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica se retorna algum erro

									// if(retorno.hasOwnProperty("Status")){
									// 	swal.close();
									// 	swal({
									// 		title: retorno.mensagem_titulo,
									// 		text: retorno.mensagem_texto,
									// 		type: retorno.mensagem_tipo,
									// 		allowOutsideClick: false
									// 	})

									// 	return reject();
									// }
								
								// Declara as Variaveis
								
									var TotalComandas = parseInt(retorno.total_comandas);
									var PonteiroComandas = 0;
									var Codigo = 0;
									var Cliente = 0;
									var Total = 0;
									var Troco = 0;
									var Pago = 0;
									var LinhaHTML = 0;
									var AntigoHTML = 0;
									var TotalRegistrosPagina = retorno.comandas.length;
								
								// Remove todas as Pessoas Fisicas
								
									$("#registrosComandas").html("");
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroComandas=0;PonteiroComandas<TotalRegistrosPagina;PonteiroComandas++){
									
										Codigo = retorno.comandas[PonteiroComandas].codigo;
										Cliente = retorno.comandas[PonteiroComandas].cod_clientes;
										Total = retorno.comandas[PonteiroComandas].total != null ? (retorno.comandas[PonteiroComandas].total).replace(/\,/g, "").replace(/\./g, ",") : "0,00";
										Pago = retorno.comandas[PonteiroComandas].pago != null ? (retorno.comandas[PonteiroComandas].pago).replace(/\,/g, "").replace(/\./g, ",") : "0,00";
										Troco = retorno.comandas[PonteiroComandas].troco != null ? (retorno.ingredientes[PonteiroComandas].troco).replace(/\,/g, "").replace(/\./g, ",") : "0,00";

										LinhaHTML = "<tr>"
										LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
										LinhaHTML += "<td class='text-center'>" + ExtraiCliente(Cliente, "Clientes") + "</td>"
										LinhaHTML += "<td class='text-center'> R$ " + Total + "</td>"
										LinhaHTML += "<td class='text-center'> R$ " + Pago + "</td>"
										LinhaHTML += "<td class='text-center'> R$ " + Troco + "</td>"
										LinhaHTML += "<td class='text-center'>"
										LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-comandas' data-comandas='"+JSON.stringify(retorno.comandas[PonteiroComandas])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
										LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-comandas' data-comandas='"+JSON.stringify(retorno.comandas[PonteiroComandas])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
										LinhaHTML += "</td>"
										LinhaHTML += "</tr>"
										
										AntigoHTML = $("#registrosComandas").html();
										$("#registrosComandas").html(AntigoHTML+LinhaHTML);
										
									}
									
								// Faz o calculo de total de registros
									
									$("#badge-TotalRegistrosComandas").html(TotalComandas);

								// Seta o valor total de Comandas

									cache.totalRegistros = TotalComandas;
									cache.registrosPagina = TotalRegistrosPagina;

								// Grava no LocalStorage

			        				localStorage.setItem("Comandas", JSON.stringify(cache));

			        			// Gera a Paginacao

									geraEstruturaPaginacao("#pgn-comandas", "Comandas")

								// Libera a tela
							
									swal.close();

								// resolve

									resolve();

							}
						});

				})

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao finalizar o carregamento da pagina

		$(document).ready(()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Chamas as Promises de Cache

				CarregaLanchesCache().then((data)=>{
					CarregaIngredientesCache().then((data1)=>{
						CarregaClientesCache().then((data2)=>{
							CarregaComandas();
						});
					});
				})

			// Renderiza as Modals
				
				renderizaModal("#modal_cad_Comandas", "<span class='text-muted'><i class='fa fa-plus'></i> Adicionar Nova Comanda</span>");
				renderizaModal("#modal_alt_Comandas", "<span class='text-muted'><i class='fa fa-pencil'></i> Editar Comanda</span>");
				renderizaModal("#modal_pag_Comandas", "<span class='text-muted'><i class='fa fa-money'></i> Pagar Pagar Comanda</span>");

		    // Gerencia o Menu

	        	// gerenciaMenu();

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao fechar ou ir para outra pagina

		$(window).on('beforeunload', function(){
		   localStorage.clear();
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-collapse-filtros-comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache na variavel

				var cache = JSON.parse(localStorage.getItem("TipoVeiculo"));

			// Carrega o Cache em variavel Local

				var cacheFiltros = JSON.parse(localStorage.getItem("Comandas"));
					cacheFiltros = cacheFiltros.filtros.estruturaCampos;

			// Limpa o Formulario

				limpaFormulario("#form_filtros_comandas");

			// Verifica se ja existe cache de filtros

				if(cacheFiltros != undefined && cacheFiltros != "" && cacheFiltros != " "){

					// Verifica se existe filtro para cada Campo

						if(cacheFiltros.hasOwnProperty("codigo")){
							$("input[name='filtros_comandas_input_Codigo']").val(cacheFiltros.codigo);
						}

						if(cacheFiltros.hasOwnProperty("nome")){
							$("select[name='filtros_comandas_input_Nome']").val(cacheFiltros.nome);
						}

				} else {

					// Parametriza os Campos

						$("input[name='filtros_comandas_input_Codigo']").val();
						$("input[name='filtros_comandas_input_Nome']").val();

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-filtrar-comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Comandas"));
					cache.filtros.estruturaCampos = {};

			// Declara as Variaveis

				var Codigo = $("input[name='filtros_comandas_input_Codigo']");
				var Nome = $("input[name='filtros_comandas_input_Nome']");

			// Monta a estrutura de pesquisa

				if(Codigo.val() != "" && Codigo.val() != " " && Codigo.val() != undefined && parseInt(Codigo.val()) > 0){
					cache.filtros.estruturaCampos.Codigo = Codigo.val();
					estruturaPesquisa += "&codigo = " + Codigo.val();
				}

				if(Nome.val() != "" && Nome.val() != " " && Nome.val() != undefined && parseInt(Nome.val()) > 0){
					cache.filtros.estruturaCampos.Nome = Nome.val();
					estruturaPesquisa += "&codmensalista = " + Nome.val();
				}

			// Define os filtros no cache

				cache.paginaAtual = 1;

			// Grava no cache

				localStorage.setItem("Comandas", JSON.stringify(cache));

			// Exibe o Botão de Remover Filtro

				$("#div-btn-remover-filtros-comandas").show();

			// Carrega as Entradas Avulsas
		
				CarregaComandas();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-remover-filtros-comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Comandas"));

			// Limpa o cache

				cache.filtros.estruturaCampos = "";

			// Grava no cache

				localStorage.setItem("Comandas", JSON.stringify(cache));

			// Limpa o Formulario

				limpaFormulario("#form_filtros_comandas");

			// Oculta o Botão de Remover Filtro

				$("#div-btn-remover-filtros-comandas").hide();

			// Carrega as Entradas Avulsas
		
				CarregaComandas();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-cad-comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Define as Mascaras

				$("input[name='modal_cad_comandas_input_Total']").mask('000.000.000.000.000,00', {reverse: true});
				$("input[name='modal_cad_comandas_input_Pago']").mask('000.000.000.000.000,00', {reverse: true});
				$("input[name='modal_cad_comandas_input_Troco']").mask('000.000.000.000.000,00', {reverse: true});

			// Limpa o Formulario

				limpaFormulario("#form_cad_Comandas");

			// Carrega o Cache na variavel

				var cacheClientes = JSON.parse(localStorage.getItem("Clientes"));

			// Trata o select modal_alt_usuario_input_TipoUsuario

				$("select[name='modal_cad_comandas_input_Cliente']").empty()
				$.each(cacheClientes, (i, item)=>{
					$("select[name='modal_cad_comandas_input_Cliente']").append($('<option>', {
						value: item.codigo,
						text: item.nome
					}));
				});

			// Remove as classes de erros

				$("#modal_cad_comandas_div_Cliente").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_cad_Comandas");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_comandas_input_Cliente

		$(document).on('change', "select[name='modal_cad_comandas_input_Cliente']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Comandas").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Codigo = $("input[name='modal_cad_comandas_input_Codigo']");
						var ClienteAnterior = $("input[name='modal_cad_comandas_input_ClienteAnterior']");
						var Cliente = $("select[name='modal_cad_comandas_input_Cliente']");

					// Realiza a Validação

						if(Cliente.val() != undefined && Cliente.val() != null && Cliente.val() != "" && Cliente.val() != " " && parseInt(Cliente.val()) > 0){

							// Verifica se a Comanda ja foi cadastrada e se sim apenas atualiza o Cliente

								if(Codigo.val() != ""){
									
									// Executa o ajax para criar a comanda assim que for selecionado o cliente

										$.ajax({
											type: "POST",
											dataType: "html",
											headers: {
												Authorization : Sessao.Token 
											},
											data: {
												'cod_clientes' : Cliente.val()
											},
											url: 'api/comandas/atualizar',
											beforeSend: function(data){
												swal({
													title: 'Atualizando a Comanda',
													text: 'Aguarde',
													onOpen: () => {
													  swal.showLoading()
													},
													allowOutsideClick: false
												})
											},
											error: function(erro) {
												var mensagem = null; 
												if (erro == "timeout") {				        						
													mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
												}
												else {				        						
													mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
												}
												swal.close();
												swal({
													title: "Ops!",
													text: mensagem,
													type: "error",
													allowOutsideClick: false
												})
												BotaoModalCadComandas.prop("disabled", false);
											},		
											success: function(retorno) {

												// Trata o retorno transformando em array

													retorno = jQuery.parseJSON(retorno);

												// Seta os Valores dos Campos de controle da comanda

													ClienteAnterior.val(Cliente.val());

												// Verifica se retornou algum erro

													// if(retorno.hasOwnProperty("Status")){
													// 	swal.close();
													// 	swal({
													// 		title: retorno.mensagem_titulo,
													// 		text: retorno.mensagem_texto,
													// 		type: retorno.mensagem_tipo,
													// 		allowOutsideClick: false
													// 	})

													// 	BotaoModalCadComandas.prop("disabled", false);

													// 	return
													// }

												// Promisse do sweet alert

													swal.close();
											}
										});

								} else {

									// Executa o ajax para criar a comanda assim que for selecionado o cliente

										$.ajax({
											type: "POST",
											dataType: "html",
											headers: {
												Authorization : Sessao.Token 
											},
											data: {
												'cod_clientes' : Cliente.val()
											},
											url: 'api/comandas/inserir',
											beforeSend: function(data){
												swal({
													title: 'Registrando a Comanda',
													text: 'Aguarde',
													onOpen: () => {
													  swal.showLoading()
													},
													allowOutsideClick: false
												})
											},
											error: function(erro) {
												var mensagem = null; 
												if (erro == "timeout") {				        						
													mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
												}
												else {				        						
													mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
												}
												swal.close();
												swal({
													title: "Ops!",
													text: mensagem,
													type: "error",
													allowOutsideClick: false
												})
												BotaoModalCadComandas.prop("disabled", false);
											},		
											success: function(retorno) {

												// Trata o retorno transformando em array

													retorno = jQuery.parseJSON(retorno);

												// Seta os Valores dos Campos de controle da comanda

													Codigo.val(retorno.cod_comanda);
													ClienteAnterior.val(Cliente.val());

												// Verifica se retornou algum erro

													// if(retorno.hasOwnProperty("Status")){
													// 	swal.close();
													// 	swal({
													// 		title: retorno.mensagem_titulo,
													// 		text: retorno.mensagem_texto,
													// 		type: retorno.mensagem_tipo,
													// 		allowOutsideClick: false
													// 	})

													// 	BotaoModalCadComandas.prop("disabled", false);

													// 	return
													// }

												// Promisse do sweet alert

													swal.close();
											}
										});

								}


							// Remove o erro do campo

								$("#modal_cad_comandas_div_Cliente").removeClass("has-error");

						} else {
							toastr.error('O Cliente precisa ser especificado', 'Ops!');
							$("#modal_cad_comandas_div_Cliente").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-modal-cad_comandas-add-lanche", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Limpa o Formulario

				limpaFormulario("#form_cad_Lanches_Comandas");

			// Carrega o Cache na variavel

				var cacheLanches = JSON.parse(localStorage.getItem("Lanches"));

			// Trata o select modal_alt_usuario_input_TipoUsuario

				$("select[name='modal_cad_comandas_input_Lanche']").empty()
				$.each(cacheLanches, (i, item)=>{
					$("select[name='modal_cad_comandas_input_Lanche']").append($('<option>', {
						value: item.codigo,
						text: item.nome
					}));
				});

			// Remove as classes de erros

				$("#modal_cad_lanches_comandas_div_Lanche").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_cad_Lanches_Comandas");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_lanches_comandas_input_Lanche

		$(document).on('change', "select[name='modal_cad_lanches_comandas_input_Lanche']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Lanches_Comandas").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Codigo = $("input[name='modal_cad_comandas_input_Codigo']");
						var Lanche = $("select[name='modal_cad_lanches_comandas_input_Lanche']");

					// Realiza a Validação

						if(Lanche.val() != undefined && Lanche.val() != null && Lanche.val() != "" && Lanche.val() != " " && parseInt(Lanche.val()) > 0){
							$("#modal_cad_lanches_comandas_div_Lanche").removeClass("has-error");
						} else {
							toastr.error('O Lanche precisa ser especificado', 'Ops!');
							$("#modal_cad_lanches_comandas_div_Lanche").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a Inserção do Lanche na Comanda

		$(document).on('click', "#btn-modal-cad_lanches_comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Lanches_Comandas").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Codigo = $("input[name='modal_cad_comandas_input_Codigo']");
						var Lanche = $("select[name='modal_cad_lanches_comandas_input_Lanche']");

					// Realiza a Validação

						if(Cliente.val() != undefined && Cliente.val() != null && Cliente.val() != "" && Cliente.val() != " " && parseInt(Cliente.val()) > 0){
									
							// Executa o ajax para criar a comanda assim que for selecionado o cliente

								$.ajax({
									type: "POST",
									dataType: "html",
									headers: {
										Authorization : Sessao.Token 
									},
									data: {
										'cod_lanche' : Lanche.val()
									},
									url: 'api/comandas/atualizar',
									beforeSend: function(data){
										swal({
											title: 'Atualizando a Comanda',
											text: 'Aguarde',
											onOpen: () => {
											  swal.showLoading()
											},
											allowOutsideClick: false
										})
									},
									error: function(erro) {
										var mensagem = null; 
										if (erro == "timeout") {				        						
											mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
										}
										else {				        						
											mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
										}
										swal.close();
										swal({
											title: "Ops!",
											text: mensagem,
											type: "error",
											allowOutsideClick: false
										})
										BotaoModalCadComandas.prop("disabled", false);
									},		
									success: function(retorno) {

										// Trata o retorno transformando em array

											retorno = jQuery.parseJSON(retorno);

										// Verifica se retornou algum erro

											// if(retorno.hasOwnProperty("Status")){
											// 	swal.close();
											// 	swal({
											// 		title: retorno.mensagem_titulo,
											// 		text: retorno.mensagem_texto,
											// 		type: retorno.mensagem_tipo,
											// 		allowOutsideClick: false
											// 	})

											// 	BotaoModalCadComandas.prop("disabled", false);

											// 	return
											// }

										// Promisse do sweet alert

											swal.close();
									}
								});

							// Remove o erro do campo

								$("#modal_cad_comandas_div_Cliente").removeClass("has-error");

						} else {
							toastr.error('O Cliente precisa ser especificado', 'Ops!');
							$("#modal_cad_comandas_div_Cliente").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o cadastro da Nova Lanche

		$(document).on('click', "#btn-modal-cad_comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Nome = $("input[name='modal_cad_comandas_input_Nome']");
				var Descricao = $("input[name='modal_cad_comandas_input_Descricao']");
				var ValorVenda = $("input[name='modal_cad_comandas_input_ValorVenda']").val();
					ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
					ValorVenda = parseFloat(ValorVenda);
				var ValorProducao = $("input[name='modal_cad_comandas_input_ValorProducao']");
				var BotaoModalCadComandas = $("#btn-modal-cad_comandas");

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = ValorVenda > 0;

				var verificaValidacao = validacao[0] && validacao[1];

			// Desativa o botão

				BotaoModalCadComandas.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'nome' : Nome.val(),
							'descricao' : Descricao.val(),
							'valor_venda' : ValorVenda,
							'valor_producao' : parseFloat((ValorProducao.val()).replace(/\./g, "").replace(/\,/g, "."))
						},
						url: 'api/comandas/inserir',
						beforeSend: function(data){
							swal({
								title: 'Cadastrando Novo Lanche',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
							BotaoModalCadComandas.prop("disabled", false);
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								// if(retorno.hasOwnProperty("Status")){
								// 	swal.close();
								// 	swal({
								// 		title: retorno.mensagem_titulo,
								// 		text: retorno.mensagem_texto,
								// 		type: retorno.mensagem_tipo,
								// 		allowOutsideClick: false
								// 	})

								// 	BotaoModalCadComandas.prop("disabled", false);

								// 	return
								// }

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_cad_Comandas");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaComandas();

									// Ativa o botão modal-comandas

										BotaoModalCadComandas.prop("disabled", false);

								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_cad_comandas_div_Nome").addClass("has-error");
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
						}
						
						if(!validacao[1]){
							$("#modal_cad_comandas_div_ValorVenda").addClass("has-error");
							toastr.error('O Valor de Venda precisa ser preenchido', 'Ops!');
						}
									
					// Ativa o Botão
					
						BotaoModalCadComandas.prop("disabled", false);

				}
		});
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-alt-comandas", ()=>{

			// Renderiza o Icheck

		        renderizaICheck('input[name="modal_alt_comandas_input_VagaPreferencial"]',1);
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Pega os Dados da Lanche

				var DadosComandas = JSON.parse(this.activeElement.getAttribute("data-comandas"));
					DadosComandas.valor_venda = (DadosComandas.valor_venda).replace(/\,/g, "").replace(/\./g, ",");
					DadosComandas.valor_producao = (DadosComandas.valor_producao).replace(/\,/g, "").replace(/\./g, ",");

			// Define as Mascaras

				$("input[name='modal_alt_comandas_input_ValorVenda']").mask('000.000.000.000.000,00', {reverse: true});
				$("input[name='modal_alt_comandas_input_ValorProducao']").mask('000.000.000.000.000,00', {reverse: true});

			// Limpa o Formulario

				limpaFormulario("#form_alt_Comandas");

			// Define o valor dos campos

				$("input[name='modal_alt_comandas_input_Codigo']").val(DadosComandas.codigo);
				$("input[name='modal_alt_comandas_input_Nome']").val(DadosComandas.nome);
				$("input[name='modal_alt_comandas_input_NomeAnterior']").val(DadosComandas.nome);
				$("input[name='modal_alt_comandas_input_Descricao']").val(DadosComandas.descricao);
				$("input[name='modal_alt_comandas_input_ValorVenda']").val(DadosComandas.valor_venda);
				$("input[name='modal_alt_comandas_input_ValorProducao']").val(DadosComandas.valor_producao);
			// Remove as classes de erros

				$("#modal_alt_comandas_div_Nome").removeClass("has-error");
				$("#modal_alt_comandas_div_Descricao").removeClass("has-error");
				$("#modal_alt_comandas_div_ValorVenda").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_alt_Comandas");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_comandas_input_Nome

		$(document).on('change', "input[name='modal_alt_comandas_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Comandas").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_alt_comandas_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_alt_comandas_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome do lanche precisa ser preenchido', 'Ops!');
							$("#modal_alt_comandas_div_Nome").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_comandas_input_ValorVenda

		$(document).on('change', "input[name='modal_alt_comandas_input_ValorVenda']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Comandas").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ValorVenda = $("input[name='modal_alt_comandas_input_ValorVenda']").val();
							ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
							ValorVenda = parseFloat(ValorVenda);

					// Realiza a Validação

						if(ValorVenda > 0){
							$("#modal_alt_comandas_div_ValorVenda").removeClass("has-error");
						} else {
							toastr.error('Especifique o Valor para Venda do Lanche', 'Ops!');
							$("#modal_alt_comandas_div_ValorVenda").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a alteração da Lanche

		$(document).on('click', "#btn-modal-alt_comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Codigo = $("input[name='modal_alt_comandas_input_Codigo']");
				var Nome = $("input[name='modal_alt_comandas_input_Nome']");
				var NomeAnterior = $("input[name='modal_alt_comandas_input_NomeAnterior']");
				var Descricao = $("input[name='modal_alt_comandas_input_Descricao']");
				var ValorVenda = $("input[name='modal_alt_comandas_input_ValorVenda']").val();
					ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
					ValorVenda = parseFloat(ValorVenda);
				var ValorProducao = $("input[name='modal_alt_comandas_input_ValorProducao']");
				var BotaoModalAltComandas = $("#btn-modal-alt_comandas");

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = ValorVenda > 0;

				var verificaValidacao = validacao[0] && validacao[1];

			// Desativa o botão

				BotaoModalAltComandas.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'codigo' : Codigo.val(),
							'nome' : Nome.val(),
							'nome_anterior' : NomeAnterior.val(),
							'descricao' : Descricao.val(),
							'valor_venda' : ValorVenda,
							'valor_producao' : parseFloat((ValorProducao.val()).replace(/\./g, "").replace(/\,/g, "."))
						},
						url: 'api/comandas/atualizar',
						beforeSend: function(data){
							swal({
								title: 'Alterando Lanche',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
							BotaoModalAltComandas.prop("disabled", false);
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								// if(retorno.hasOwnProperty("Status")){
								// 	swal.close();
								// 	swal({
								// 		title: retorno.mensagem_titulo,
								// 		text: retorno.mensagem_texto,
								// 		type: retorno.mensagem_tipo,
								// 		allowOutsideClick: false
								// 	})

								// 	BotaoModalAltComandas.prop("disabled", false);

								// 	return
								// }

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_alt_Comandas");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaComandas();

									// Ativa o botão modal-comandas

										BotaoModalAltComandas.prop("disabled", false);
								
								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_alt_comandas_div_Nome").addClass("has-error");
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
						}
						
						if(!validacao[1]){
							$("#modal_alt_comandas_div_ValorVenda").addClass("has-error");
							toastr.error('O Valor de Venda precisa ser preenchido', 'Ops!');
						}
									
					// Ativa o Botão
					
						BotaoModalAltComandas.prop("disabled", false);

				}
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Lanche

		$(document).on('click', "#btn-del-comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var DadosComandas = JSON.parse(this.activeElement.getAttribute("data-comandas"));
				var Codigo = DadosComandas.codigo;

			// Verifica se realmente irá excluir o registro e se for verdadeira executa o ajax

				swal({
					title: 'Deseja prosseguir com a exclusão deste Lanche?',
					text: "Lembramos que após excluir, não havera volta!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Deletar',
					cancelButtonText: 'Cancelar',
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: true,
					allowOutsideClick: false
				}).then(function(data){

					if (data.value != undefined) {

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'codigo' : Codigo
							},
							url: 'api/comandas/excluir',
							beforeSend: function(data){
								swal({
									title: 'Excluindo Lanche',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									// if(retorno.hasOwnProperty("Status")){
									// 	swal.close();
									// 	swal({
									// 		title: retorno.mensagem_titulo,
									// 		text: retorno.mensagem_texto,
									// 		type: retorno.mensagem_tipo,
									// 		allowOutsideClick: false
									// 	})

									// 	return
									// }

								// Promisse do sweet alert

									swal({
										title: retorno.mensagem_titulo,
										text: retorno.mensagem_texto,
										type: retorno.mensagem_tipo,
										allowOutsideClick: false
									}).then(()=>{

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaComandas();
									
									})
							}
						});

					} 

				});
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Lanche

		$(document).on('click', "#btn-ativar-comandas, #btn-desativar-comandas", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var DadosComandas = JSON.parse(this.activeElement.getAttribute("data-comandas"));
				var Codigo = DadosComandas.codigo;

				switch((this.activeElement.id).split("-")[1]){
					case "ativar":
						var Status = 1;
						break;
					case "desativar":
						var Status = 2;
						break;
				}

			// executa o ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'codigo' : Codigo,
						'cod_status' : Status
					},
					url: 'api/comandas/atualizar',
					beforeSend: function(data){
						swal({
							title: 'Atualizando Status do Lanche',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},		
					success: function(retorno) {

						// Trata o retorno transformando em array

							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							// if(retorno.hasOwnProperty("Status")){
							// 	swal.close();
							// 	swal({
							// 		title: retorno.mensagem_titulo,
							// 		text: retorno.mensagem_texto,
							// 		type: retorno.mensagem_tipo,
							// 		allowOutsideClick: false
							// 	})

							// 	return
							// }

						// Promisse do sweet alert

							swal({
								title: retorno.mensagem_titulo,
								text: retorno.mensagem_texto,
								type: retorno.mensagem_tipo,
								allowOutsideClick: false
							}).then(()=>{

								// Chama a função para Carregar as Entradas Avulsas
								
									CarregaComandas();
							
							})
					}
				});

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});