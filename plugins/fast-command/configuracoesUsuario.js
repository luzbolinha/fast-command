//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das configurações geral

		$(document).on("click", "#btn-tab-usuarios", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Carrega os Caches

	        	CarregaTipoUsuarioCache();

			// Renderiza as Modals
				
				renderizaModal("#modal_cad_Usuario", "<span class='text-muted'><i class='fa fa-plus'></i> Cadastrar Usuário</span>");
				renderizaModal("#modal_alt_Usuario", "<span class='text-muted'><i class='fa fa-pencil'></i> Editar Usuário</span>");

			// Carrega os planos

				CarregaUsuarios();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a troca de paginas da tabela de Entrada Avulsa

		$(document).on('click', "#btn-pgn-usuarios", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Chama a função para troca de paginas

				trocaPagina(this, "UsuariosPaginacao");

			// Carrega as Entradas Avulsas
		
				CarregaUsuarios();
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a troca de paginas da tabela de Entrada Avulsa

		$(document).on('click', "#btn-cad-usuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Carrega o Cache na variavel

				var cache = JSON.parse(localStorage.getItem("TipoUsuario"));

			// Limpa o Formulario

				limpaFormulario("#form_cad_Usuario");

			// Trata o select modal_cad_usuario_input_TipoUsuario

				$("select[name='modal_cad_usuario_input_TipoUsuario']").empty()
				$.each(cache, (i, item)=>{
					$("select[name='modal_cad_usuario_input_TipoUsuario']").append($('<option>', {
						value: item.Codigo,
						text: item.Descricao
					}));
				});

			// Limpa as Classes de Erros

				$("#modal_cad_usuario_div_ImagemPerfil").removeClass("has-error");
				$("#modal_cad_usuario_div_Nome").removeClass("has-error");
				$("#modal_cad_usuario_div_Email").removeClass("has-error");
				$("#modal_cad_usuario_div_TipoUsuario").removeClass("has-error");
				$("#modal_cad_usuario_div_Senha").removeClass("has-error");
				$("#modal_cad_usuario_div_Senha").removeClass("has-warning");
				$("#modal_cad_usuario_div_ConfirmaSenha").removeClass("has-error");

			// Define o Tamanho Maximo de Arquivo na Label

				$("#modal_cad_usuario_input_ImagemPerfil_sizelabel").html(upload.MaxSize+" MB")

			// Reseta a pré visualização da imagem

				$("#modal_cad_usuario_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");

			// Abre a Modal

				abriraModal("#modal_cad_Usuario");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_usuario_input_ImagemPerfil

		$(document).on('change', "input[name='modal_cad_usuario_input_ImagemPerfil']", (event)=>{

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ImagemPerfil = $("input[name='modal_cad_usuario_input_ImagemPerfil']");

					// Carrega a Imagem e retorna Parametros

						var CarregaImagem = readFile(this.activeElement, "#modal_cad_usuario_PreVisualizacaoImagemPerfil");

					// Verifica se a array Retornou Erro

						if(CarregaImagem.hasOwnProperty("error")){
							$("#modal_cad_usuario_div_ImagemPerfil").addClass("has-error");
							toastr.error(CarregaImagem.error);
							ImagemPerfil.val("");
							$("#modal_cad_usuario_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
						} else {
							$("#modal_cad_usuario_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
							$("#modal_cad_usuario_div_ImagemPerfil").removeClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_usuario_input_Nome

		$(document).on('change keyup', "input[name='modal_cad_usuario_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_cad_usuario_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_cad_usuario_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome Completo do Usuário precisa ser preenchido', 'Ops!');
							$("#modal_cad_usuario_div_Nome").addClass("has-error");
						}

				}

		})


//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_usuario_input_Email

		$(document).on('change', "input[name='modal_cad_usuario_input_Email']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Email = $("input[name='modal_cad_usuario_input_Email']");
						var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;

					// Realiza a Validação

						if((Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val())){
							$("#modal_cad_usuario_div_Email").removeClass("has-error");
						} else {
							toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
							$("#modal_cad_usuario_div_Email").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_usuario_input_TipoUsuario

		$(document).on('change', "select[name='modal_cad_usuario_input_TipoUsuario']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var TipoUsuario = $("select[name='modal_cad_usuario_input_TipoUsuario']");

					// Realiza a Validação

						if(TipoUsuario.val() != undefined && TipoUsuario.val() != null && TipoUsuario.val() != "" && TipoUsuario.val() != " "){
							$("#modal_cad_usuario_div_TipoUsuario").removeClass("has-error");
						} else {
							toastr.error('O Tipo de Usuário precisa ser preenchido', 'Ops!');
							$("#modal_cad_usuario_div_TipoUsuario").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_usuario_input_Senha

		$(document).on('change', "input[name='modal_cad_usuario_input_Senha']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Senha = $("input[name='modal_cad_usuario_input_Senha']");
						var ConfirmaSenha = $("input[name='modal_cad_usuario_input_ConfirmaSenha']");

					// Realiza a Validação

						if(Senha.val() != undefined && Senha.val() != null && Senha.val() != "" && Senha.val() != " "){
							$("#modal_cad_usuario_div_Senha").removeClass("has-error");
						} else {
							toastr.error('A Senha precisa ser preenchida', 'Ops!');
							$("#modal_cad_usuario_div_Senha").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_usuario_input_ConfirmaSenha

		$(document).on('change', "input[name='modal_cad_usuario_input_ConfirmaSenha']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Senha = $("input[name='modal_cad_usuario_input_Senha']");
						var ConfirmaSenha = $("input[name='modal_cad_usuario_input_ConfirmaSenha']");

					// Realiza a Validação

						if(ConfirmaSenha.val() != undefined && ConfirmaSenha.val() != null && ConfirmaSenha.val() != "" && ConfirmaSenha.val() != " " && ConfirmaSenha.val() == Senha.val()){
							$("#modal_cad_usuario_div_Senha").removeClass("has-warning");
							$("#modal_cad_usuario_div_ConfirmaSenha").removeClass("has-error");
						} else {
							if(ConfirmaSenha.val() != Senha.val()){
								toastr.error('O Campo Confirme a Senha deve ser igual ao campo Senha', 'Ops!');
								$("#modal_cad_usuario_div_Senha").addClass("has-warning");
								$("#modal_cad_usuario_div_ConfirmaSenha").addClass("has-error");
							} else {
								toastr.error('O Campo Confirme a Senha precisa ser preenchido', 'Ops!');
								$("#modal_cad_usuario_div_ConfirmaSenha").addClass("has-error");
							}
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o Processo de Inserção

		$(document).on("click", "#btn-modal-cad_usuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as variaveis

				var ImagemPerfil = $("input[name='modal_cad_usuario_input_ImagemPerfil']");
				var Nome = $("input[name='modal_cad_usuario_input_Nome']");
				var Email = $("input[name='modal_cad_usuario_input_Email']");
				var TipoUsuario = $("select[name='modal_cad_usuario_input_TipoUsuario']");
				var Senha = $("input[name='modal_cad_usuario_input_Senha']");
				var ConfirmaSenha = $("input[name='modal_cad_usuario_input_ConfirmaSenha']");
				var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
				var BotaoCadUsuario = $("#btn-modal-cad_usuario");
				var Funcao = 1;

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = (ImagemPerfil.val() != undefined && ImagemPerfil.val() != null && ImagemPerfil.val() != "" && ImagemPerfil.val() != " ") ? (((ImagemPerfil.get(0).files[0].size / 1024) / 1024) <= upload.MaxSize && upload.Types.indexOf(ImagemPerfil.get(0).files[0].name.split('.').pop().toLowerCase()) > -1) : true;
					validacao[1] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[2] = (Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val());
					validacao[3] = TipoUsuario.val() != undefined && TipoUsuario.val() != null && TipoUsuario.val() != "" && TipoUsuario.val() != " ";
					validacao[4] = Senha.val() != undefined && Senha.val() != null && Senha.val() != "" && Senha.val() != " ";
					validacao[5] = ConfirmaSenha.val() != undefined && ConfirmaSenha.val() != null && ConfirmaSenha.val() != "" && ConfirmaSenha.val() != " " && ConfirmaSenha.val() == Senha.val();

				var verificaValidacao = validacao[0] && validacao[1] && validacao[2] && validacao[3] && validacao[4] && validacao[5];

			// Desativa o botão

				BotaoCadUsuario.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					// Define a estrutura do form

						if(ImagemPerfil.val() != undefined && ImagemPerfil.val() != null && ImagemPerfil.val() != "" && ImagemPerfil.val() != " "){
							var form = new FormData();
								form.append('ImagemPerfilAlias', ImagemPerfil.get(0).files[0])
						}

						var URL = 'ajax/usuarios_ajax.asp?Nome='+Nome.val()+'&Email='+Email.val()+'&TipoUsuario='+TipoUsuario.val()+'&Senha='+Senha.val()+'&Status='+0+'&Funcao='+Funcao+'&ImagemPerfil='+ImagemPerfil.val();

					// Envia o Ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: form,
					        cache: false,
					        processData: false,
					        contentType: false,
					        headers: {
					        	'Content-Type' : undefined
					        },
							url: URL,
							beforeSend: function(data){
								data.setRequestHeader("Authorization", Sessao.Token);
								swal({
									title: 'Cadastrando Usuário',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
								BotaoCadUsuario.prop("disabled", false);
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									}).then(()=>{

										// Fecha a Modal

											fecharModal("#modal_cad_Usuario");

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaUsuarios();

										// Ativa o botão modal-tipoVeiculo

											BotaoCadUsuario.prop("disabled", false);
									
									})
							}
						});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							toastr.error('Verifique o campo Imagem Perfil', 'Ops!');
							$("#modal_cad_usuario_div_ImagemPerfil").addClass("has-error");
						}

						if(!validacao[1]){
							toastr.error('O Nome Completo do Usuário precisa ser preenchido', 'Ops!');
							$("#modal_cad_usuario_div_Nome").addClass("has-error");
						}

						if(!validacao[2]){
							toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
							$("#modal_cad_usuario_div_Email").addClass("has-error");
						}

						if(!validacao[3]){
							toastr.error('O Tipo de Usuário precisa ser preenchido', 'Ops!');
							$("#modal_cad_usuario_div_TipoUsuario").addClass("has-error");
						}

						if(!validacao[4]){
							toastr.error('A Senha precisa ser preenchida', 'Ops!');
							$("#modal_cad_usuario_div_Senha").addClass("has-error");
						}

						if(!validacao[5]){
							if(ConfirmaSenha.val() != Senha.val()){
								toastr.error('O Campo Confirme a Senha deve ser igual ao campo Senha', 'Ops!');
								$("#modal_cad_usuario_div_Senha").addClass("has-warning");
								$("#modal_cad_usuario_div_ConfirmaSenha").addClass("has-error");
							} else {
								toastr.error('O Campo Confirme a Senha precisa ser preenchido', 'Ops!');
								$("#modal_cad_usuario_div_ConfirmaSenha").addClass("has-error");
							}
						}
									
					// Ativa o Botão
					
						BotaoCadUsuario.prop("disabled", false);

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a troca de paginas da tabela de Entrada Avulsa

		$(document).on('click', "#btn-alt-usuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Pega as Informações do Usuário

				var DadosUsuario = JSON.parse(this.activeElement.getAttribute("data-usuario"));

			// Carrega o Cache na variavel

				var cache = JSON.parse(localStorage.getItem("TipoUsuario"));

			// Limpa o Formulario

				limpaFormulario("#form_alt_Usuario");

			// Trata o select modal_alt_usuario_input_TipoUsuario

				$("select[name='modal_alt_usuario_input_TipoUsuario']").empty()
				$.each(cache, (i, item)=>{
					$("select[name='modal_alt_usuario_input_TipoUsuario']").append($('<option>', {
						value: item.Codigo,
						text: item.Descricao
					}));
				});

			// Seta os valores dos Campos

				$("input[name='modal_alt_usuario_input_Codigo']").val(DadosUsuario.Codigo);
				$("input[name='modal_alt_usuario_input_NomeAnterior']").val(DadosUsuario.Nome);
				$("input[name='modal_alt_usuario_input_EmailAnterior']").val(DadosUsuario.Email);
				$("input[name='modal_alt_usuario_input_Nome']").val(DadosUsuario.Nome);
				$("input[name='modal_alt_usuario_input_Email']").val(DadosUsuario.Email);
				$("select[name='modal_alt_usuario_input_TipoUsuario']").val(DadosUsuario.TipoUsuario);

			// Limpa as Classes de Erros

				$("#modal_alt_usuario_div_ImagemPerfil").removeClass("has-error");
				$("#modal_alt_usuario_div_Nome").removeClass("has-error");
				$("#modal_alt_usuario_div_Email").removeClass("has-error");
				$("#modal_alt_usuario_div_TipoUsuario").removeClass("has-error");
				$("#modal_alt_usuario_div_Senha").removeClass("has-error");
				$("#modal_alt_usuario_div_Senha").removeClass("has-warning");
				$("#modal_alt_usuario_div_ConfirmaSenha").removeClass("has-error");

			// Define o Tamanho Maximo de Arquivo na Label

				$("#modal_alt_usuario_input_ImagemPerfil_sizelabel").html(upload.MaxSize+" MB")

			// Verifica a imagem do usuário para gerar a pré visualização

				if(DadosUsuario.ImagemPerfil != undefined && DadosUsuario.ImagemPerfil != ""){
					$("#modal_alt_usuario_PreVisualizacaoImagemPerfil").prop("src", "uploads/imagens/"+DadosUsuario.ImagemPerfil);
				} else {
					$("#modal_alt_usuario_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
				}

			// Abre a Modal

				abriraModal("#modal_alt_Usuario");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_usuario_input_ImagemPerfil

		$(document).on('change', "input[name='modal_alt_usuario_input_ImagemPerfil']", (event)=>{

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ImagemPerfil = $("input[name='modal_alt_usuario_input_ImagemPerfil']");

					// Carrega a Imagem e retorna Parametros

						var CarregaImagem = readFile(this.activeElement, "#modal_alt_usuario_PreVisualizacaoImagemPerfil");

					// Verifica se a array Retornou Erro

						if(CarregaImagem.hasOwnProperty("error")){
							$("#modal_alt_usuario_div_ImagemPerfil").addClass("has-error");
							toastr.error(CarregaImagem.error);
							ImagemPerfil.val("");
							$("#modal_alt_usuario_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
						} else {
							$("#modal_alt_usuario_PreVisualizacaoImagemPerfil").prop("src", "images/default-user.png");
							$("#modal_alt_usuario_div_ImagemPerfil").removeClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_usuario_input_Nome

		$(document).on('change keyup', "input[name='modal_alt_usuario_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_alt_usuario_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_alt_usuario_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome Completo do Usuário precisa ser preenchido', 'Ops!');
							$("#modal_alt_usuario_div_Nome").addClass("has-error");
						}

				}

		})


//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_usuario_input_Email

		$(document).on('change', "input[name='modal_alt_usuario_input_Email']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Email = $("input[name='modal_alt_usuario_input_Email']");
						var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;

					// Realiza a Validação

						if((Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val())){
							$("#modal_alt_usuario_div_Email").removeClass("has-error");
						} else {
							toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
							$("#modal_alt_usuario_div_Email").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_usuario_input_TipoUsuario

		$(document).on('change', "select[name='modal_alt_usuario_input_TipoUsuario']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var TipoUsuario = $("select[name='modal_alt_usuario_input_TipoUsuario']");

					// Realiza a Validação

						if(TipoUsuario.val() != undefined && TipoUsuario.val() != null && TipoUsuario.val() != "" && TipoUsuario.val() != " "){
							$("#modal_alt_usuario_div_TipoUsuario").removeClass("has-error");
						} else {
							toastr.error('O Tipo de Usuário precisa ser preenchido', 'Ops!');
							$("#modal_alt_usuario_div_TipoUsuario").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_usuario_input_ConfirmaSenha

		$(document).on('change', "input[name='modal_alt_usuario_input_ConfirmaSenha']", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Usuario").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Senha = $("input[name='modal_alt_usuario_input_Senha']");
						var ConfirmaSenha = $("input[name='modal_alt_usuario_input_ConfirmaSenha']");

					// Realiza a Validação

						if(ConfirmaSenha.val() == Senha.val()){
							$("#modal_alt_usuario_div_Senha").removeClass("has-warning");
							$("#modal_alt_usuario_div_ConfirmaSenha").removeClass("has-error");
						} else {
							toastr.error('O Campo Confirme a Senha deve ser igual ao campo Senha', 'Ops!');
							$("#modal_alt_usuario_div_Senha").addClass("has-warning");
							$("#modal_alt_usuario_div_ConfirmaSenha").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o Processo de Inserção

		$(document).on("click", "#btn-modal-alt_usuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as variaveis

				var Codigo = $("input[name='modal_alt_usuario_input_Codigo']");
				var ImagemPerfil = $("input[name='modal_alt_usuario_input_ImagemPerfil']");
				var NomeAnterior = $("input[name='modal_alt_usuario_input_NomeAnterior']");
				var EmailAnterior = $("input[name='modal_alt_usuario_input_EmailAnterior']");
				var Nome = $("input[name='modal_alt_usuario_input_Nome']");
				var Email = $("input[name='modal_alt_usuario_input_Email']");
				var TipoUsuario = $("select[name='modal_alt_usuario_input_TipoUsuario']");
				var Senha = $("input[name='modal_alt_usuario_input_Senha']");
				var ConfirmaSenha = $("input[name='modal_alt_usuario_input_ConfirmaSenha']");
				var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
				var BotaoAltUsuario = $("#btn-modal-alt_usuario");
				var Funcao = 2;

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = (ImagemPerfil.val() != undefined && ImagemPerfil.val() != null && ImagemPerfil.val() != "" && ImagemPerfil.val() != " ") ? (((ImagemPerfil.get(0).files[0].size / 1024) / 1024) <= upload.MaxSize && upload.Types.indexOf(ImagemPerfil.get(0).files[0].name.split('.').pop().toLowerCase()) > -1) : true;
					validacao[1] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[2] = (Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val());
					validacao[3] = TipoUsuario.val() != undefined && TipoUsuario.val() != null && TipoUsuario.val() != "" && TipoUsuario.val() != " ";
					validacao[4] = ConfirmaSenha.val() == Senha.val();

				var verificaValidacao = validacao[0] && validacao[1] && validacao[2] && validacao[3] && validacao[4];

			// Desativa o botão

				BotaoAltUsuario.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					// Define a estrutura do form

						if(ImagemPerfil.val() != undefined && ImagemPerfil.val() != null && ImagemPerfil.val() != "" && ImagemPerfil.val() != " "){
							var form = new FormData();
								form.append('ImagemPerfilAlias', ImagemPerfil.get(0).files[0])
						}

						var URL = 'ajax/usuarios_ajax.asp?Codigo='+Codigo.val()+'&NomeAnterior='+NomeAnterior.val()+'&EmailAnterior='+EmailAnterior.val()+'&Nome='+Nome.val()+'&Email='+Email.val()+'&TipoUsuario='+TipoUsuario.val()+'&Senha='+Senha.val()+'&Status='+0+'&Funcao='+Funcao+'&ImagemPerfil='+ImagemPerfil.val();

					// Envia o Ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: form,
					        cache: false,
					        processData: false,
					        contentType: false,
					        headers: {
					        	'Content-Type' : undefined
					        },
							url: URL,
							beforeSend: function(data){
								data.setRequestHeader("Authorization", Sessao.Token);
								swal({
									title: 'Alterando Usuário',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
								BotaoAltUsuario.prop("disabled", false);
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									}).then(()=>{

										// Fecha a Modal

											fecharModal("#modal_alt_Usuario");

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaUsuarios();

										// Ativa o botão modal-tipoVeiculo

											BotaoAltUsuario.prop("disabled", false);
									
									})
							}
						});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							toastr.error('Verifique o campo Imagem Perfil', 'Ops!');
							$("#modal_alt_usuario_div_ImagemPerfil").addClass("has-error");
						}

						if(!validacao[1]){
							toastr.error('O Nome Completo do Usuário precisa ser preenchido', 'Ops!');
							$("#modal_alt_usuario_div_Nome").addClass("has-error");
						}

						if(!validacao[2]){
							toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
							$("#modal_alt_usuario_div_Email").addClass("has-error");
						}

						if(!validacao[3]){
							toastr.error('O Tipo de Usuário precisa ser preenchido', 'Ops!');
							$("#modal_alt_usuario_div_TipoUsuario").addClass("has-error");
						}

						if(!validacao[4]){
							if(ConfirmaSenha.val() != Senha.val()){
								toastr.error('O Campo Confirme a Senha deve ser igual ao campo Senha', 'Ops!');
								$("#modal_alt_usuario_div_Senha").addClass("has-warning");
								$("#modal_alt_usuario_div_ConfirmaSenha").addClass("has-error");
							} else {
								toastr.error('O Campo Confirme a Senha precisa ser preenchido', 'Ops!');
								$("#modal_alt_usuario_div_ConfirmaSenha").addClass("has-error");
							}
						}
									
					// Ativa o Botão
					
						BotaoAltUsuario.prop("disabled", false);

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Entrada Avulsa

		$(document).on('click', "#btn-del-usuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as variaveis
				
				var DadosUsuario = JSON.parse(this.activeElement.getAttribute("data-usuario"));
				var Codigo = DadosUsuario.Codigo;
				var Funcao = 3;

			// Verifica se realmente irá excluir o registro e se for verdadeira executa o ajax

				swal({
					title: 'Deseja prosseguir com a exclusão deste Usuário?',
					text: "Lembramos que após excluir, não havera volta!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Deletar',
					cancelButtonText: 'Cancelar',
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: true,
					allowOutsideClick: false
				}).then(function(data){

					if (data.value != undefined) {

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'Codigo' : Codigo,
								'Funcao' : Funcao
							},
							url: 'ajax/usuarios_ajax.asp',
							beforeSend: function(data){
								swal({
									title: 'Excluindo Usuário',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									}).then(()=>{

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaUsuarios();
									
									})
							}
						});

					} 

				});
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Entrada Avulsa

		$(document).on('click', "#btn-loggout-usuario", ()=>{
			
			// Verificação de Permissão de Modulo

				if(AcessoModulo(Sessao.Permissoes) == false){
					return false;
				}

			// Declara as variaveis
				
				var DadosUsuario = JSON.parse(this.activeElement.getAttribute("data-usuario"));
				var Codigo = DadosUsuario.Codigo;
				var Funcao = 6;

			// Verifica se realmente irá excluir o registro e se for verdadeira executa o ajax

				swal({
					title: 'Deseja prosseguir com a desconexão deste Usuário?',
					text: "Lembramos que caso o usuário esteja realizando alguma tarefa no sistema o mesmo sera enterrompido!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Desconectar',
					cancelButtonText: 'Cancelar',
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: true,
					allowOutsideClick: false
				}).then(function(data){

					if (data.value != undefined) {

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'Codigo' : Codigo,
								'Funcao' : Funcao
							},
							url: 'ajax/usuarios_ajax.asp',
							beforeSend: function(data){
								swal({
									title: 'Desconectando Usuário',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.MensagemTitulo,
											text: retorno.MensagemTexto,
											type: retorno.MensagemTipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

									swal({
										title: retorno.MensagemTitulo,
										text: retorno.MensagemTexto,
										type: retorno.MensagemTipo,
										allowOutsideClick: false
									}).then(()=>{

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaUsuarios();
									
									})
							}
						});

					} 

				});
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

})