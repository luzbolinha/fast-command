//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                   	jQuery da Página : [PATH]/*.asp			    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o procedimento de Login

		function ChecaSessao(){

			// Encerra o ciclo de checagem da Sessão

				clearInterval(Sessao.checaSessao);

			// Declaração das Variaveis

				var Usuario = Sessao.Usuario;
				var Nome = Sessao.Nome;
				var Email = Sessao.Email;
				var TipoUsuario = Sessao.TipoUsuario;
				var ImagemPerfil = Sessao.ImagemPerfil;
				var Token = Sessao.Token;
				var Permissoes = Sessao.Permissoes;

			// Verifica se existe sessão ativa no session storage

				var verificaSessao = [];
					verificaSessao[0] = Usuario != "" && Usuario != " " && Usuario != undefined;
					verificaSessao[1] = Nome != "" && Nome != " " && Nome != undefined;
					verificaSessao[2] = Email != "" && Email != " " && Email != undefined;
					verificaSessao[3] = TipoUsuario != "" && TipoUsuario != " " && TipoUsuario != undefined;
					verificaSessao[4] = Token != "" && Token != " " && Token != undefined;
					verificaSessao[5] = Permissoes != "" && Permissoes != " " && Permissoes != undefined;

				var validaVerificacao = verificaSessao[0] && verificaSessao[1] && verificaSessao[2] && verificaSessao[3] && verificaSessao[4] && verificaSessao[5];

			// Verifica se ira consultar os dados da sessão ou rejeitar o acesso e direcionar a tela de login

				if(validaVerificacao){

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Token 
							},
							data: {
								'Usuario' : Usuario,
								'Email' : Email,
								'Token' : Token
							},
							url: 'includes/seguranca.asp',
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica qual tipo de status recebeu

									if(parseInt(retorno.Status) == 401){

										// Limpa o session e local storage

											sessionStorage.clear();
											localStorage.clear();

										// Redireciona para a pagina principal

											window.location.href = 'default.asp';
										
									} else {

										// Verifica se o Usuário ainda esta logado

											if(parseInt(retorno.StatusUsuario) == 1){

												// Atualiza os Dados da Sessão

													Sessao.Usuario = retorno.Codigo;
													Sessao.Nome = retorno.Nome;
													Sessao.Email = retorno.Email;
													Sessao.ImagemPerfil = retorno.ImagemPerfil;
													Sessao.TipoUsuario = retorno.TipoUsuario;
													Sessao.Token = retorno.Token;
													Sessao.Permissoes = retorno.Permissoes;

												// Atualiza o Session Storage

													sessionStorage.setItem("Usuario", Sessao.Usuario);
													sessionStorage.setItem("Nome", Sessao.Nome);
													sessionStorage.setItem("Email", Sessao.Email);
													sessionStorage.setItem("ImagemPerfil", Sessao.ImagemPerfil);
													sessionStorage.setItem("TipoUsuario", Sessao.TipoUsuario);
													sessionStorage.setItem("Token", Sessao.Token);
													sessionStorage.setItem("Permissoes", JSON.stringify(Sessao.Permissoes));

												// Verifica se o Usuário possui permissões para acesso a area informada

												// Atualiza as Imagens do usuario do Menu, Nav e Nav UL

													if(Sessao.ImagemPerfil != ""){

														$("#menu-ImagemPerfil").prop("src", "uploads/imagens/"+Sessao.ImagemPerfil);
														$("#nav-ImagemPerfil").prop("src", "uploads/imagens/"+Sessao.ImagemPerfil);
														$("#nav-ul-ImagemPerfil").prop("src", "uploads/imagens/"+Sessao.ImagemPerfil);

													} else {

														$("#menu-ImagemPerfil").prop("src", "images/default-user.png");
														$("#nav-ImagemPerfil").prop("src", "images/default-user.png");
														$("#nav-ul-ImagemPerfil").prop("src", "images/default-user.png");

													}

												// Atualiza o Nome do usuario do Menu, Nav e Nav UL

													$("#menu-Nome").html(Sessao.Nome);
													$("#nav-Nome").html(Sessao.Nome);
													$("#nav-ul-Nome").html(Sessao.Nome);

												// Atualiza o email do usuario do Menu, Nav e Nav UL

													$("#nav-ul-Email").html(Sessao.Email);

												// Chama a função para gerenciar o menu

													gerenciaMenu();

												// Chama a função para gerenciar o modulo que esta acessando

													gerenciaModulo();

												// Verifica se o usuário possui acesso ao modulo acessado

													if(AcessoModulo(Sessao.Permissoes)){

														// Insere o setInterval na sessao

															Sessao.checaSessao = window.setInterval(ChecaSessao, 2000);

													} else {

														// Exibe a mensagem na tela
														
															swal({
																title: "Ops!",
																text: "Você não tem permissão para acessar está área, verifique junto ao administrador",
																type: "error",
																allowOutsideClick: false
															}).then(()=>{

																// Redireciona para a tela principal

																	window.location.href = 'estacionamento.asp';

															})

													}

											} else {

												// Limpa o session e local storage

													sessionStorage.clear();
													localStorage.clear();

												// Redireciona para a pagina principal

													window.location.href = 'default.asp';

											}

									}

							}

						});

				} else {

					// Limpa o session e local storage

						sessionStorage.clear();
						localStorage.clear();

					// Redireciona para a pagina principal

						window.location.href = 'default.asp';

				}


		};

	// Realiza o procedimento verificação

		// ChecaSessao();

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
