//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                   	jQuery da Página : [PATH]/home.asp		    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
		
	// Executa comandos após carregar o arquivo

		$(document).ready(()=>{

			// Renderiza o Icheck

				renderizaICheck('input[name="lembrarme"]',1);

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Valida o campo E-Mail

		$(document).on('change', "input[name='input_login_Email']", ()=>{

			// Declaração das Variaveis

				var Email = $("input[name='input_login_Email']");
				var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;

			// Realiza a Validação

				if((Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val())){
					$("#div_input_login_Email").removeClass("has-error");
				} else {
					toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
					$("#div_input_login_Email").addClass("has-error");
				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Valida o campo Senha

		$(document).on('change', "input[name='input_login_Senha']", ()=>{

			// Declaração das Variaveis

				var Senha = $("input[name='input_login_Senha']");

			// Realiza a Validação

				if((Senha.val()).trim() != "" && (Senha.val()).trim() != " "){
					$("#div_input_login_Senha").removeClass("has-error");
				} else {
					toastr.error('A Senha precisa ser preenchida', 'Ops!');
					$("#div_input_login_Senha").addClass("has-error");
				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o procedimento de Login

		$(document).on("click", "#btn-logar", ()=>{

			// Declaração das Variaveis

				var Email = $("input[name='input_login_Email']");
				var regEmail = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
				var Senha = $("input[name='input_login_Senha']");
				var Token = GeraToken(32, 3);
				var Funcao = 1;

			// Realiza as validações

				var validacao = []
					validacao[0] = (Email.val()).trim() != "" && (Email.val()).trim() != " " && regEmail.test(Email.val());
					validacao[1] = (Senha.val()).trim() != "" && (Senha.val()).trim() != " ";

				var verificaValidacao = validacao[0] && validacao[1];

			// Confere a validação

				if(verificaValidacao == true){

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Token 
							},
							data: {
								'Email' : Email.val(),
								'Senha' : Senha.val(),
								'Token' : Token,
								'Funcao' : Funcao
							},
							url: 'ajax/autenticacao_ajax.asp',
							beforeSend: function(data){
								swal({
									title: 'Realizando login',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica qual tipo de status recebeu

									switch(parseInt(retorno.Status)){
										case 200:

											// Encerra o Sweetalert

												swal.close();

											// Seta os Valores no Session Storage

												sessionStorage.setItem("Usuario", retorno.Codigo);
												sessionStorage.setItem("TipoUsuario", retorno.TipoUsuario);
												sessionStorage.setItem("Nome", retorno.Nome);
												sessionStorage.setItem("Email", retorno.Email);
												sessionStorage.setItem("Token", retorno.Token);
												sessionStorage.setItem("ImagemPerfil", retorno.ImagemPerfil);
												sessionStorage.setItem("Permissoes", JSON.stringify(retorno.Permissoes));

											// Redireciona para a pagina principal

												window.location.href = 'estacionamento.asp';

											break;
										case 404:

											// Sweetalert

												swal({
													title: retorno.MensagemTitulo,
													text: retorno.MensagemTexto,
													type: retorno.MensagemTipo,
													allowOutsideClick: false
												})

											// Break

												break;
									}

							}

						});

				} else {

					if(!validacao[0]){
						toastr.error('O Email precisa ser preenchido e deve ser válido', 'Ops!');
						$("#div_input_login_Email").addClass("has-error");
					}
					
					if(!validacao[1]){
						toastr.error('A Senha precisa ser preenchida', 'Ops!');
						$("#div_input_login_Senha").addClass("has-error");
					}

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});