//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                  JavaScript da Página : [PATH]/clientes.php	    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Declara as Variaveis de Cache

		var Clientes = {
	    	paginaAtual : 1,
	    	totalPaginas : 1,
	    	registrosPorPagina : 5,
	    	registrosPagina : 0,
	    	totalRegistros : 0,
	    	filtros : {
	    		estruturaPesquisa : "",
	    		estruturaCampos : {}
	    	}
		}

	// Função para gerar o cache do Estacionamento

		function geraCache(){
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Grava no localStorage

				localStorage.setItem("Clientes", JSON.stringify(Clientes));

		}

		geraCache();
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                   	jQuery da Página : [PATH]/home.asp		    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar os Clientes

		function CarregaClientes(){

			// Define a Promise

				return new Promise((resolve, reject)=>{
			
					// Verificação de Permissão de Modulo

						// if(AcessoModulo(Sessao.Permissoes) == false){
						// 	return false;
						// }

					// Verifica se ja existe cache

						var cache = JSON.parse(localStorage.getItem("Clientes"));

					// Declaração das Variaveis

						var Filtros = cache.filtros.estruturaCampos;
						var RegistrosPorPaginas = 5;
						var PaginaRegistro = 0;
						if(cache == null){
							PaginaRegistro = 0;
						} else if(cache.hasOwnProperty("paginaAtual")){
							if(cache.paginaAtual > 0){
								PaginaRegistro = (cache.paginaAtual * RegistrosPorPaginas) - 5;
							} else {
								PaginaRegistro = 0;
							}
						}

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'filtros' : (Object.keys(Filtros)).length == 0 ? "" : Filtros,
								'registros_paginas' : RegistrosPorPaginas,
								'pagina_registro' : PaginaRegistro
							},
							url: 'api/clientes',
							beforeSend: function(data){
								swal({
									title: 'Carregando Clientes',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								}).then((data)=>{
									reject();
								})
							},		
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica se retorna algum erro

									// if(retorno.hasOwnProperty("Status")){
									// 	swal.close();
									// 	swal({
									// 		title: retorno.mensagem_titulo,
									// 		text: retorno.mensagem_texto,
									// 		type: retorno.mensagem_tipo,
									// 		allowOutsideClick: false
									// 	})

									// 	return reject();
									// }
								
								// Declara as Variaveis
								
									var TotalClientes = parseInt(retorno.total_clientes);
									var PonteiroClientes = 0;
									var Codigo = 0;
									var Nome = 0;
									var RG = 0;
									var CPF = 0;
									var Telefone = 0;
									var Email = 0;
									var LinhaHTML = 0;
									var AntigoHTML = 0;
									var TotalRegistrosPagina = retorno.clientes.length;
								
								// Remove todas as Pessoas Fisicas
								
									$("#registrosClientes").html("");
								
								// Preenche as Informações da Aba [Contatos] com as Informações carregadas em JSON
						
									for(PonteiroClientes=0;PonteiroClientes<TotalRegistrosPagina;PonteiroClientes++){
									
										Codigo = retorno.clientes[PonteiroClientes].codigo;
										Nome = retorno.clientes[PonteiroClientes].nome;
										RG = retorno.clientes[PonteiroClientes].rg;
										CPF = retorno.clientes[PonteiroClientes].cpf;
										Telefone = retorno.clientes[PonteiroClientes].telefone;
										Email = retorno.clientes[PonteiroClientes].email;

										LinhaHTML = "<tr>"
										LinhaHTML += "<td class='text-center'>" + Codigo + "</td>"
										LinhaHTML += "<td class='text-center'>" + Nome + "</td>"
										LinhaHTML += "<td class='text-center'>" + RG + "</td>"
										LinhaHTML += "<td class='text-center'>" + CPF + "</td>"
										LinhaHTML += "<td class='text-center'>" + Telefone + "</td>"
										LinhaHTML += "<td class='text-center'>" + Email + "</td>"
										LinhaHTML += "<td class='text-center'>"
										LinhaHTML += "<button type='button' class='btn btn-xs btn-primary' data-toggle='tooltip' id='btn-alt-clientes' data-clientes='"+JSON.stringify(retorno.clientes[PonteiroClientes])+"' data-placement='left' title='Editar'><i class='fa fa-pencil'></i></button>"
										LinhaHTML += " | <button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' id='btn-del-clientes' data-clientes='"+JSON.stringify(retorno.clientes[PonteiroClientes])+"' data-placement='left' title='Excluir'><i class='fa fa-trash'></i></button>"
										LinhaHTML += "</td>"
										LinhaHTML += "</tr>"
										
										AntigoHTML = $("#registrosClientes").html();
										$("#registrosClientes").html(AntigoHTML+LinhaHTML);
										
									}
									
								// Faz o calculo de total de registros
									
									$("#badge-TotalRegistrosClientes").html(TotalClientes);

								// Seta o valor total de Clientes

									cache.totalRegistros = TotalClientes;
									cache.registrosPagina = TotalRegistrosPagina;

								// Grava no LocalStorage

			        				localStorage.setItem("Clientes", JSON.stringify(cache));

			        			// Gera a Paginacao

									geraEstruturaPaginacao("#pgn-clientes", "Clientes")

								// Libera a tela
							
									swal.close();

								// resolve

									resolve();

							}
						});

				})

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao finalizar o carregamento da pagina

		$(document).ready(()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Promise da função de Carregar as Configurações

				CarregaClientes();

		    // Gerencia o Menu

	        	// gerenciaMenu();

			// Renderiza as Modals
				
				renderizaModal("#modal_cad_Clientes", "<span class='text-muted'><i class='fa fa-plus'></i> Cadastrar Novo Client</span>");
				renderizaModal("#modal_alt_Clientes", "<span class='text-muted'><i class='fa fa-pencil'></i> Editar Cliente</span>");

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao fechar ou ir para outra pagina

		$(window).on('beforeunload', function(){
		   localStorage.clear();
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a troca de paginas da tabela de Entrada Avulsa

		$(document).on('click', "#btn-pgn-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Chama a função para troca de paginas

				trocaPagina(this, "Clientes");

			// Carrega os registros na tabela
		
				CarregaClientes();

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Clientes

		$(document).on("click", "#btn-tab-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
				
			// Carrega os registros na tabela

				CarregaClientes();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Clientes

		$(document).on("click", "#btn-collapse-filtros-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cacheFiltros = JSON.parse(localStorage.getItem("Clientes"));
					cacheFiltros = cacheFiltros.filtros.estruturaCampos;

			// Limpa o Formulario

				limpaFormulario("#form_filtros_clientes");

			// Verifica se ja existe cache de filtros

				if(cacheFiltros != undefined && cacheFiltros != "" && cacheFiltros != " "){

					// Verifica se existe filtro para cada Campo

						if(cacheFiltros.hasOwnProperty("codigo")){
							$("input[name='filtros_clientes_input_Codigo']").val(cacheFiltros.codigo);
						}

						if(cacheFiltros.hasOwnProperty("nome")){
							$("input[name='filtros_clientes_input_Nome']").val(cacheFiltros.nome);
						}

						if(cacheFiltros.hasOwnProperty("cpf")){
							$("input[name='filtros_clientes_input_CPF']").val(cacheFiltros.cpf);
						}

				} else {

					// Parametriza os campos
					
						$("input[name='filtros_clientes_input_Codigo']").val("");
						$("input[name='filtros_clientes_input_Nome']").val("");
						$("input[name='filtros_clientes_input_CPF']").val("");

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Clientes

		$(document).on("click", "#btn-filtrar-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Clientes"));
					cache.filtros.estruturaCampos = {};

			// Declara as Variaveis

				var Codigo = $("input[name='filtros_clientes_input_Codigo']");
				var Nome = $("input[name='filtros_clientes_input_Nome']");
				var CPF = $("input[name='filtros_clientes_input_CPF']");

			// Monta a estrutura de pesquisa

				if(Codigo.val() != "" && Codigo.val() != " " && Codigo.val() != undefined && parseInt(Codigo.val()) > 0){
					cache.filtros.estruturaCampos.codigo = Codigo.val();
				}

				if(Nome.val() != "" && Nome.val() != " " && Nome.val() != undefined){
					cache.filtros.estruturaCampos.nome = Nome.val();
				}

				if(CPF.val() != "" && CPF.val() != " " && CPF.val() != undefined){
					cache.filtros.estruturaCampos.cpf = CPF.val();
				}

			// Define os filtros no cache

				cache.paginaAtual = 1;

			// Grava no cache

				localStorage.setItem("Clientes", JSON.stringify(cache));

			// Exibe o Botão de Remover Filtro

				$("#div-btn-remover-filtros-clientes").show();

			// Carrega as Clientes com Filtro

				CarregaClientes();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Clientes

		$(document).on("click", "#btn-remover-filtros-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Clientes"));

			// Limpa o cache

				cache.filtros.estruturaCampos = "";

			// Grava no cache

				localStorage.setItem("Clientes", JSON.stringify(cache));

			// Limpa o Formulario

				limpaFormulario("#form_filtros_clientes");

			// Oculta o Botão de Remover Filtro

				$("#div-btn-remover-filtros-clientes").hide();

			// Carrega as Clientes com Filtro

				CarregaClientes();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-cad-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Define as Mascaras

				$("input[name='modal_cad_clientes_input_RG']").mask('99.999.999-9');
				$("input[name='modal_cad_clientes_input_CPF']").mask('999.999.999-99');
				$("input[name='modal_cad_clientes_input_Telefone']").mask(SPMaskBehavior, spOptions);
				$("input[name='modal_cad_clientes_input_CEP']").mask('99999-999');

			// Limpa o Formulario

				limpaFormulario("#form_cad_Clientes");

			// Remove as classes de erros

				$("#modal_cad_clientes_div_Nome").removeClass("has-error");
				$("#modal_cad_clientes_div_CPF").removeClass("has-error");
				$("#modal_cad_clientes_div_Telefone").removeClass("has-error");
				$("#modal_cad_clientes_div_CEP").removeClass("has-error");
				$("#modal_cad_clientes_div_UF").removeClass("has-error");
				$("#modal_cad_clientes_div_Cidade").removeClass("has-error");
				$("#modal_cad_clientes_div_Bairro").removeClass("has-error");
				$("#modal_cad_clientes_div_Endereco").removeClass("has-error");
				$("#modal_cad_clientes_div_Numero").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_cad_Clientes");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_Nome

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_cad_clientes_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_cad_clientes_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Nome").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_DataNascimento

		$(document).on('change', "input[name='modal_cad_clientes_input_DataNascimento']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var DataNascimento = $("input[name='modal_cad_clientes_input_DataNascimento']");

					// Realiza a Validação

						if(moment(DataNascimento.val()).format("L") != "Invalid date"){
							$("#modal_cad_clientes_div_DataNascimento").removeClass("has-error");
						} else {
							toastr.error('A Data de Nascimento precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_DataNascimento").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_CPF

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_CPF']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var CPF = $("input[name='modal_cad_clientes_input_CPF']");

					// Realiza a Validação

						if(CPF.val() != undefined && CPF.val() != null && CPF.val() != "" && CPF.val() != " "){
							$("#modal_cad_clientes_div_CPF").removeClass("has-error");
						} else {
							toastr.error('O CPF precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_CPF").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_Telefone

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_Telefone']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Telefone = $("input[name='modal_cad_clientes_input_Telefone']");

					// Realiza a Validação

						if(Telefone.val() != undefined && Telefone.val() != null && Telefone.val() != "" && Telefone.val() != " "){
							$("#modal_cad_clientes_div_Telefone").removeClass("has-error");
						} else {
							toastr.error('O Telefone precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Telefone").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_CEP

		$(document).on('change', "input[name='modal_cad_clientes_input_CEP']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var CEP = $("input[name='modal_cad_clientes_input_CEP']");

					// Realiza a Validação

						if(CEP.val() != undefined && CEP.val() != null && CEP.val() != "" && CEP.val() != " "){

							// Declara as Variaveis

								var Bairro = $("input[name='modal_cad_clientes_input_Bairro']");
								var Endereco = $("input[name='modal_cad_clientes_input_Endereco']");
								var Cidade = $("input[name='modal_cad_clientes_input_Cidade']");
								var UF = $("input[name='modal_cad_clientes_input_UF']");
								var ChecaCEP = CEP.val();

							// Trata o CEP recebido

								ChecaCEP = ChecaCEP.replace(/\-/g,"");
								ChecaCEP = ChecaCEP.replace(/\./g,"");

							// Verifica se o cep possui 8 digitos

								if(ChecaCEP.length == 8){

									$.ajax({
										type: "get",
										dataType: "html",
										url: "https://viacep.com.br/ws/"+ChecaCEP+"/json/",
										beforeSend: function(data){
											swal({
												title: 'Consultando CEP',
												text: 'Aguarde',
												onOpen: () => {
												  swal.showLoading()
												},
												allowOutsideClick: false
											})
										},
										error: function(erro) {
											var mensagem = null; 
											if (erro == "timeout") {				        						
												mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
											}
											else {				        						
												mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
											}
											swal.close();
											swal({
												title: "Ops!",
												text: mensagem,
												type: "error",
												allowOutsideClick: false
											})
										},		
										success: function(retorno) {

											// Trata o retorno transformando em array

												var CEPArray = jQuery.parseJSON(retorno);

											// Define o valor dos campos

												Bairro.val(CEPArray.bairro);
												Endereco.val(CEPArray.logradouro);
												Cidade.val(CEPArray.localidade);
												UF.val(CEPArray.uf);

											// Fecha o SweetAlert

												swal.close();

										}
									});

								} else {

									// Exibe Mensagem de Erro

										toastr.error('O CEP especificado esta incorreto, verifique e tente novamente', 'Ops!');

								}
							$("#modal_cad_clientes_div_CEP").removeClass("has-error");
						} else {
							toastr.error('O CEP precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_CEP").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_Cidade

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_Cidade']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Cidade = $("input[name='modal_cad_clientes_input_Cidade']");

					// Realiza a Validação

						if(Cidade.val() != undefined && Cidade.val() != null && Cidade.val() != "" && Cidade.val() != " "){
							$("#modal_cad_clientes_div_Cidade").removeClass("has-error");
						} else {
							toastr.error('A Cidade precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_Cidade").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_UF

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_UF']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var UF = $("input[name='modal_cad_clientes_input_UF']");

					// Realiza a Validação

						if(UF.val() != undefined && UF.val() != null && UF.val() != "" && UF.val() != " "){
							$("#modal_cad_clientes_div_UF").removeClass("has-error");
						} else {
							toastr.error('A UF precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_UF").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_Bairro

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_Bairro']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Bairro = $("input[name='modal_cad_clientes_input_Bairro']");

					// Realiza a Validação

						if(Bairro.val() != undefined && Bairro.val() != null && Bairro.val() != "" && Bairro.val() != " "){
							$("#modal_cad_clientes_div_Bairro").removeClass("has-error");
						} else {
							toastr.error('A Bairro precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_Bairro").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_Endereco

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_Endereco']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Endereco = $("input[name='modal_cad_clientes_input_Endereco']");

					// Realiza a Validação

						if(Endereco.val() != undefined && Endereco.val() != null && Endereco.val() != "" && Endereco.val() != " "){
							$("#modal_cad_clientes_div_Endereco").removeClass("has-error");
						} else {
							toastr.error('O Endereço precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Endereco").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_clientes_input_Numero

		$(document).on('change keyup', "input[name='modal_cad_clientes_input_Numero']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Numero = $("input[name='modal_cad_clientes_input_Numero']");

					// Realiza a Validação

						if(Numero.val() != undefined && Numero.val() != null && Numero.val() != "" && Numero.val() != " "){
							$("#modal_cad_clientes_div_Numero").removeClass("has-error");
						} else {
							toastr.error('O Número precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Numero").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o cadastro do(a) Cliente

		$(document).on('click', "#btn-modal-cad_clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Nome = $("input[name='modal_cad_clientes_input_Nome']");
				var RG = $("input[name='modal_cad_clientes_input_RG']");
				var CPF = $("input[name='modal_cad_clientes_input_CPF']");
				var Email = $("input[name='modal_cad_clientes_input_Email']");
				var Telefone = $("input[name='modal_cad_clientes_input_Telefone']");
				var CEP = $("input[name='modal_cad_clientes_input_CEP']");
				var Cidade = $("input[name='modal_cad_clientes_input_Cidade']");
				var UF = $("input[name='modal_cad_clientes_input_UF']");
				var Bairro = $("input[name='modal_cad_clientes_input_Bairro']");
				var Endereco = $("input[name='modal_cad_clientes_input_Endereco']");
				var Numero = $("input[name='modal_cad_clientes_input_Numero']");
				var DataNascimento = $("input[name='modal_cad_clientes_input_DataNascimento']");
				var BotaoModalCadClientes = $("#btn-modal-cad_clientes");
				var TipoClientes = 1;
				var Funcao = 1;

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = CPF.val() != undefined && CPF.val() != null && CPF.val() != "" && CPF.val() != " ";
					validacao[2] = Telefone.val() != undefined && Telefone.val() != null && Telefone.val() != "" && Telefone.val() != " ";
					validacao[3] = CEP.val() != undefined && CEP.val() != null && CEP.val() != "" && CEP.val() != " " && ((CEP.val()).replace(/\-/g,"").replace(/\./g,"")).length == 8;
					validacao[4] = Cidade.val() != undefined && Cidade.val() != null && Cidade.val() != "" && Cidade.val() != " ";
					validacao[5] = UF.val() != undefined && UF.val() != null && UF.val() != "" && UF.val() != " ";
					validacao[6] = Bairro.val() != undefined && Bairro.val() != null && Bairro.val() != "" && Bairro.val() != " ";
					validacao[7] = Endereco.val() != undefined && Endereco.val() != null && Endereco.val() != "" && Endereco.val() != " ";
					validacao[8] = Numero.val() != undefined && Numero.val() != null && Numero.val() != "" && Numero.val() != " ";
					validacao[9] = moment(DataNascimento.val()).format("L") != "Invalid date";

				var verificaValidacao = validacao[0] && validacao[1] && validacao[2] && validacao[3] && validacao[4] && validacao[5] && validacao[6] && validacao[7] && validacao[8] && validacao[9];

			// Desativa o botão

				BotaoModalCadClientes.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'nome' : Nome.val(),
							'rg' : RG.val(),
							'cpf' : CPF.val(),
							'email' : Email.val(),
							'telefone' : Telefone.val(),
							'cep' : CEP.val(),
							'cidade' : Cidade.val(),
							'uf' : UF.val(),
							'bairro' : Bairro.val(),
							'endereco' : Endereco.val(),
							'numero' : Numero.val(),
							'data_nascimento' : moment(DataNascimento.val()).format("YYYY-MM-DD")
						},
						url: 'api/clientes/inserir',
						beforeSend: function(data){
							swal({
								title: 'Cadastrando Novo(a) Cliente',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								if(retorno.hasOwnProperty("Status")){
									swal.close();
									swal({
										title: retorno.mensagem_titulo,
										text: retorno.mensagem_texto,
										type: retorno.mensagem_tipo,
										allowOutsideClick: false
									})

									return
								}

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_cad_Clientes");

									// Chama a função para Carregar as Clientes
									
										CarregaClientes();

									// Ativa o botão modal-clientes

										BotaoModalCadClientes.prop("disabled", false);
								
								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Nome").addClass("has-error");
						}
						
						if(!validacao[1]){
							toastr.error('O CPF precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_CPF").addClass("has-error");
						}

						if(!validacao[2]){
							toastr.error('O Telefone precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Telefone").addClass("has-error");
						}

						if(!validacao[3]){
							toastr.error('O CEP precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_CEP").addClass("has-error");
						}

						if(!validacao[4]){
							toastr.error('A Cidade precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_Cidade").addClass("has-error");
						}

						if(!validacao[5]){
							toastr.error('A UF precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_UF").addClass("has-error");
						}

						if(!validacao[6]){
							toastr.error('A Bairro precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_Bairro").addClass("has-error");
						}

						if(!validacao[7]){
							toastr.error('O Endereço precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Endereco").addClass("has-error");
						}

						if(!validacao[8]){
							toastr.error('O Número precisa ser preenchido', 'Ops!');
							$("#modal_cad_clientes_div_Numero").addClass("has-error");
						}

						if(!validacao[9]){
							toastr.error('A Data de Nascimento precisa ser preenchida', 'Ops!');
							$("#modal_cad_clientes_div_DataNascimento").addClass("has-error");
						}
									
					// Ativa o Botão
					
						BotaoModalCadClientes.prop("disabled", false);

				}
		});
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-alt-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Pega os Dados da Entrada Avulsa

				var DadosClientes = JSON.parse(this.activeElement.getAttribute("data-clientes"));
			
			// Define as Mascaras

				$("input[name='modal_alt_clientes_input_RG']").mask('99.999.999-9');
				$("input[name='modal_alt_clientes_input_CPF']").mask('999.999.999-99');
				$("input[name='modal_alt_clientes_input_Telefone']").mask(SPMaskBehavior, spOptions);
				$("input[name='modal_alt_clientes_input_CEP']").mask('99999-999');

			// Limpa o Formulario

				limpaFormulario("#form_alt_Clientes");

			// Seta os parametros

				$("input[name='modal_alt_clientes_input_Codigo']").val(DadosClientes.codigo);
				$("input[name='modal_alt_clientes_input_NomeAnterior']").val(DadosClientes.nome);
				$("input[name='modal_alt_clientes_input_CPFAnterior']").val(DadosClientes.cpf);
				$("input[name='modal_alt_clientes_input_Nome']").val(DadosClientes.nome);
				$("input[name='modal_alt_clientes_input_RG']").val(DadosClientes.rg);
				$("input[name='modal_alt_clientes_input_CPF']").val(DadosClientes.cpf);
				$("input[name='modal_alt_clientes_input_Email']").val(DadosClientes.email);
				$("input[name='modal_alt_clientes_input_Telefone']").val(DadosClientes.telefone);
				$("input[name='modal_alt_clientes_input_CEP']").val(DadosClientes.cep);
				$("input[name='modal_alt_clientes_input_Cidade']").val(DadosClientes.cidade);
				$("input[name='modal_alt_clientes_input_UF']").val(DadosClientes.uf);
				$("input[name='modal_alt_clientes_input_Bairro']").val(DadosClientes.bairro);
				$("input[name='modal_alt_clientes_input_Endereco']").val(DadosClientes.endereco);
				$("input[name='modal_alt_clientes_input_Numero']").val(DadosClientes.numero);
				$("input[name='modal_alt_clientes_input_DataNascimento']").val(DadosClientes.data_nascimento);

			// Remove as classes de erros

				$("#modal_alt_clientes_div_Nome").removeClass("has-error");
				$("#modal_alt_clientes_div_CPF").removeClass("has-error");
				$("#modal_alt_clientes_div_Telefone").removeClass("has-error");
				$("#modal_alt_clientes_div_CEP").removeClass("has-error");
				$("#modal_alt_clientes_div_UF").removeClass("has-error");
				$("#modal_alt_clientes_div_Cidade").removeClass("has-error");
				$("#modal_alt_clientes_div_Bairro").removeClass("has-error");
				$("#modal_alt_clientes_div_Endereco").removeClass("has-error");
				$("#modal_alt_clientes_div_Numero").removeClass("has-error");
				$("#modal_alt_clientes_div_DataNascimento").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_alt_Clientes");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_Nome

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_alt_clientes_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_alt_clientes_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Nome").addClass("has-error");
						}

				}

		})


//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_DataNascimento

		$(document).on('change', "input[name='modal_alt_clientes_input_DataNascimento']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var DataNascimento = $("input[name='modal_alt_clientes_input_DataNascimento']");

					// Realiza a Validação

						if(moment(DataNascimento.val()).format("L") != "Invalid date"){
							$("#modal_alt_clientes_div_DataNascimento").removeClass("has-error");
						} else {
							toastr.error('A Data de Nascimento precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_DataNascimento").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_CPF

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_CPF']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var CPF = $("input[name='modal_alt_clientes_input_CPF']");

					// Realiza a Validação

						if(CPF.val() != undefined && CPF.val() != null && CPF.val() != "" && CPF.val() != " "){
							$("#modal_alt_clientes_div_CPF").removeClass("has-error");
						} else {
							toastr.error('O CPF precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_CPF").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_Telefone

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_Telefone']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Telefone = $("input[name='modal_alt_clientes_input_Telefone']");

					// Realiza a Validação

						if(Telefone.val() != undefined && Telefone.val() != null && Telefone.val() != "" && Telefone.val() != " "){
							$("#modal_alt_clientes_div_Telefone").removeClass("has-error");
						} else {
							toastr.error('O Telefone precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Telefone").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_CEP

		$(document).on('change', "input[name='modal_alt_clientes_input_CEP']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var CEP = $("input[name='modal_alt_clientes_input_CEP']");

					// Realiza a Validação

						if(CEP.val() != undefined && CEP.val() != null && CEP.val() != "" && CEP.val() != " "){

							// Declara as Variaveis

								var Bairro = $("input[name='modal_alt_clientes_input_Bairro']");
								var Endereco = $("input[name='modal_alt_clientes_input_Endereco']");
								var Cidade = $("input[name='modal_alt_clientes_input_Cidade']");
								var UF = $("input[name='modal_alt_clientes_input_UF']");
								var ChecaCEP = CEP.val();

							// Trata o CEP recebido

								ChecaCEP = ChecaCEP.replace(/\-/g,"");
								ChecaCEP = ChecaCEP.replace(/\./g,"");

							// Verifica se o cep possui 8 digitos

								if(ChecaCEP.length == 8){

									$.ajax({
										type: "get",
										dataType: "html",
										url: "https://viacep.com.br/ws/"+ChecaCEP+"/json/",
										beforeSend: function(data){
											swal({
												title: 'Consultando CEP',
												text: 'Aguarde',
												onOpen: () => {
												  swal.showLoading()
												},
												allowOutsideClick: false
											})
										},
										error: function(erro) {
											var mensagem = null; 
											if (erro == "timeout") {				        						
												mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
											}
											else {				        						
												mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
											}
											swal.close();
											swal({
												title: "Ops!",
												text: mensagem,
												type: "error",
												allowOutsideClick: false
											})
										},		
										success: function(retorno) {

											// Trata o retorno transformando em array

												var CEPArray = jQuery.parseJSON(retorno);

											// Define o valor dos campos

												Bairro.val(CEPArray.bairro);
												Endereco.val(CEPArray.logradouro);
												Cidade.val(CEPArray.localidade);
												UF.val(CEPArray.uf);

											// Fecha o SweetAlert

												swal.close();

										}
									});

								} else {

									// Exibe Mensagem de Erro

										toastr.error('O CEP especifialto esta incorreto, verifique e tente novamente', 'Ops!');

								}
							$("#modal_alt_clientes_div_CEP").removeClass("has-error");
						} else {
							toastr.error('O CEP precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_CEP").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_Cidade

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_Cidade']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Cidade = $("input[name='modal_alt_clientes_input_Cidade']");

					// Realiza a Validação

						if(Cidade.val() != undefined && Cidade.val() != null && Cidade.val() != "" && Cidade.val() != " "){
							$("#modal_alt_clientes_div_Cidade").removeClass("has-error");
						} else {
							toastr.error('A Cidade precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_Cidade").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_UF

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_UF']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var UF = $("input[name='modal_alt_clientes_input_UF']");

					// Realiza a Validação

						if(UF.val() != undefined && UF.val() != null && UF.val() != "" && UF.val() != " "){
							$("#modal_alt_clientes_div_UF").removeClass("has-error");
						} else {
							toastr.error('A UF precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_UF").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_Bairro

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_Bairro']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Bairro = $("input[name='modal_alt_clientes_input_Bairro']");

					// Realiza a Validação

						if(Bairro.val() != undefined && Bairro.val() != null && Bairro.val() != "" && Bairro.val() != " "){
							$("#modal_alt_clientes_div_Bairro").removeClass("has-error");
						} else {
							toastr.error('A Bairro precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_Bairro").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_Endereco

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_Endereco']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Endereco = $("input[name='modal_alt_clientes_input_Endereco']");

					// Realiza a Validação

						if(Endereco.val() != undefined && Endereco.val() != null && Endereco.val() != "" && Endereco.val() != " "){
							$("#modal_alt_clientes_div_Endereco").removeClass("has-error");
						} else {
							toastr.error('O Endereço precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Endereco").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_clientes_input_Numero

		$(document).on('change keyup', "input[name='modal_alt_clientes_input_Numero']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Clientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Numero = $("input[name='modal_alt_clientes_input_Numero']");

					// Realiza a Validação

						if(Numero.val() != undefined && Numero.val() != null && Numero.val() != "" && Numero.val() != " "){
							$("#modal_alt_clientes_div_Numero").removeClass("has-error");
						} else {
							toastr.error('O Número precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Numero").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a alteração do(a) Cliente

		$(document).on('click', "#btn-modal-alt_clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Codigo = $("input[name='modal_alt_clientes_input_Codigo']");
				var NomeAnterior = $("input[name='modal_alt_clientes_input_NomeAnterior']");
				var CPFAnterior = $("input[name='modal_alt_clientes_input_CPFAnterior']");
				var Nome = $("input[name='modal_alt_clientes_input_Nome']");
				var RG = $("input[name='modal_alt_clientes_input_RG']");
				var CPF = $("input[name='modal_alt_clientes_input_CPF']");
				var Email = $("input[name='modal_alt_clientes_input_Email']");
				var Telefone = $("input[name='modal_alt_clientes_input_Telefone']");
				var CEP = $("input[name='modal_alt_clientes_input_CEP']");
				var Cidade = $("input[name='modal_alt_clientes_input_Cidade']");
				var UF = $("input[name='modal_alt_clientes_input_UF']");
				var Bairro = $("input[name='modal_alt_clientes_input_Bairro']");
				var Endereco = $("input[name='modal_alt_clientes_input_Endereco']");
				var Numero = $("input[name='modal_alt_clientes_input_Numero']");
				var DataNascimento = $("input[name='modal_alt_clientes_input_DataNascimento']");
				var BotaoModalAltClientes = $("#btn-modal-alt_clientes");
				var TipoClientes = 1;
				var Funcao = 2;

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = CPF.val() != undefined && CPF.val() != null && CPF.val() != "" && CPF.val() != " ";
					validacao[2] = Telefone.val() != undefined && Telefone.val() != null && Telefone.val() != "" && Telefone.val() != " ";
					validacao[3] = CEP.val() != undefined && CEP.val() != null && CEP.val() != "" && CEP.val() != " " && ((CEP.val()).replace(/\-/g,"").replace(/\./g,"")).length == 8;
					validacao[4] = Cidade.val() != undefined && Cidade.val() != null && Cidade.val() != "" && Cidade.val() != " ";
					validacao[5] = UF.val() != undefined && UF.val() != null && UF.val() != "" && UF.val() != " ";
					validacao[6] = Bairro.val() != undefined && Bairro.val() != null && Bairro.val() != "" && Bairro.val() != " ";
					validacao[7] = Endereco.val() != undefined && Endereco.val() != null && Endereco.val() != "" && Endereco.val() != " ";
					validacao[8] = Numero.val() != undefined && Numero.val() != null && Numero.val() != "" && Numero.val() != " ";
					validacao[9] = moment(DataNascimento.val()).format("L") != "Invalid date";

				var verificaValidacao = validacao[0] && validacao[1] && validacao[2] && validacao[3] && validacao[4] && validacao[5] && validacao[6] && validacao[7] && validacao[8] && validacao[9];

			// Desativa o botão

				BotaoModalAltClientes.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'codigo' : Codigo.val(),
							'nome_anterior' : NomeAnterior.val(),
							'cpf_anterior' : CPFAnterior.val(),
							'nome' : Nome.val(),
							'rg' : RG.val(),
							'cpf' : CPF.val(),
							'email' : Email.val(),
							'telefone' : Telefone.val(),
							'cep' : CEP.val(),
							'cidade' : Cidade.val(),
							'uf' : UF.val(),
							'bairro' : Bairro.val(),
							'endereco' : Endereco.val(),
							'numero' : Numero.val(),
							'data_nascimento' : moment(DataNascimento.val()).format("YYYY-MM-DD")
						},
						url: 'api/clientes/atualizar',
						beforeSend: function(data){
							swal({
								title: 'Alterando Cliente',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								if(retorno.hasOwnProperty("Status")){
									swal.close();
									swal({
										title: retorno.mensagem_titulo,
										text: retorno.mensagem_texto,
										type: retorno.mensagem_tipo,
										allowOutsideClick: false
									})

									return
								}

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_alt_Clientes");

									// Chama a função para Carregar as Clientes
									
										CarregaClientes();

									// Ativa o botão modal-clientes

										BotaoModalAltClientes.prop("disabled", false);
								
								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Nome").addClass("has-error");
						}
						
						if(!validacao[1]){
							toastr.error('O CPF precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_CPF").addClass("has-error");
						}

						if(!validacao[2]){
							toastr.error('O Telefone precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Telefone").addClass("has-error");
						}

						if(!validacao[3]){
							toastr.error('O CEP precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_CEP").addClass("has-error");
						}

						if(!validacao[4]){
							toastr.error('A Cidade precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_Cidade").addClass("has-error");
						}

						if(!validacao[5]){
							toastr.error('A UF precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_UF").addClass("has-error");
						}

						if(!validacao[6]){
							toastr.error('A Bairro precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_Bairro").addClass("has-error");
						}

						if(!validacao[7]){
							toastr.error('O Endereço precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Endereco").addClass("has-error");
						}

						if(!validacao[8]){
							toastr.error('O Número precisa ser preenchido', 'Ops!');
							$("#modal_alt_clientes_div_Numero").addClass("has-error");
						}

						if(!validacao[9]){
							toastr.error('A Data de Nascimento precisa ser preenchida', 'Ops!');
							$("#modal_alt_clientes_div_DataNascimento").addClass("has-error");
						}
									
					// Ativa o Botão
					
						BotaoModalAltClientes.prop("disabled", false);

				}
		});
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Entrada Avulsa

		$(document).on('click', "#btn-del-clientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var DadosClientes = JSON.parse(this.activeElement.getAttribute("data-clientes"));
				var Codigo = DadosClientes.codigo;
				var Funcao = 3;

			// Verifica se realmente irá excluir o registro e se for verdadeira executa o ajax

				swal({
					title: 'Deseja prosseguir com a exclusão deste Cliente?',
					text: "Lembramos que após excluir, não havera volta!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Deletar',
					cancelButtonText: 'Cancelar',
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: true,
					allowOutsideClick: false
				}).then(function(data){

					if (data.value != undefined) {

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'codigo' : Codigo
							},
							url: 'api/clientes/excluir',
							beforeSend: function(data){
								swal({
									title: 'Excluindo Cliente',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									if(retorno.hasOwnProperty("Status")){
										swal.close();
										swal({
											title: retorno.mensagem_titulo,
											text: retorno.mensagem_texto,
											type: retorno.mensagem_tipo,
											allowOutsideClick: false
										})

										return
									}

								// Promisse do sweet alert

									swal({
										title: retorno.mensagem_titulo,
										text: retorno.mensagem_texto,
										type: retorno.mensagem_tipo,
										allowOutsideClick: false
									}).then(()=>{

										// Chama a função para Carregar as Clientes
										
											CarregaClientes();
									
									})
							}
						});

					} 

				});
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});