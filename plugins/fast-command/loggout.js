//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                   	jQuery da Página : [PATH]/home.asp		    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o procedimento de Login

		$(document).on("click", "#btn-loggout", ()=>{

			// Declaração das Variaveis

				var Usuario = Sessao.Usuario;
				var Token = Sessao.Token;
				var Funcao = 2;

			// Realiza as validações

				var validacao = []
					validacao[0] = Usuario != "" && Usuario != " " && Usuario != undefined && Usuario != null;
					validacao[1] = Token != "" && Token != " " && Token != undefined && Token != null;

				var verificaValidacao = validacao[0] && validacao[1];

			// Confere a validação

				if(verificaValidacao == true){

					// Realiza a chamada do ajax

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Token 
							},
							data: {
								'Usuario' : Usuario,
								'Token' : Token,
								'Funcao' : Funcao
							},
							url: 'ajax/autenticacao_ajax.asp',
							beforeSend: function(data){
								swal({
									title: 'Realizando loggout',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},
							success: function(retorno) {

								// Converte o retorno de HTML para JSON
				
									retorno = jQuery.parseJSON(retorno);

								// Verifica qual tipo de status recebeu

									if(parseInt(retorno.Status) == 200){

										// Encerra o Sweetalert

											swal.close();

										// Seta os Valores no Session Storage

											swal({
												title: retorno.MensagemTitulo,
												text: retorno.MensagemTexto,
												type: retorno.MensagemTipo,
												allowOutsideClick: false
											}).then((data)=>{

												// Limpa o session e local storage

													sessionStorage.clear();
													localStorage.clear();

												// Redireciona para a pagina principal

													window.location.href = 'default.asp';

											})

										
									} else {

										// Sweetalert

											swal({
												title: retorno.MensagemTitulo,
												text: retorno.MensagemTexto,
												type: retorno.MensagemTipo,
												allowOutsideClick: false
											})

									}

							}

						});

				} else {

					if(!validacao[0]){
						toastr.error('O Usuario da sessão esta como vazio', 'Ops!');
					}

					if(!validacao[0]){
						toastr.error('O Token da sessão esta como vazio', 'Ops!');
					}

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});