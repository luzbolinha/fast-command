//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                  JavaScript da Página : [PATH]/cardapio.php		   	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

jQuery(document).ready(function($) {

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao finalizar o carregamento da pagina

		$(document).ready(()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Renderiza as Modals
				
				renderizaModal("#modal_cad_Ingredientes", "<span class='text-muted'><i class='fa fa-plus'></i> Adicionar Novo Ingrediente</span>");
				renderizaModal("#modal_alt_Ingredientes", "<span class='text-muted'><i class='fa fa-pencil'></i> Editar Ingrediente</span>");

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa os scripts ao fechar ou ir para outra pagina

		$(window).on('beforeunload', function(){
		   localStorage.clear();
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a troca de paginas da tabela de Ingrediente

		$(document).on('click', "#btn-pgn-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Chama a função para troca de paginas

				trocaPagina(this, "Ingredientes");

			// Carrega as Entradas Avulsas
		
				CarregaIngredientes();

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-tab-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
				
			// Carrega as Entradas Avulsas

				CarregaIngredientes();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-collapse-filtros-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache na variavel

				var cache = JSON.parse(localStorage.getItem("TipoVeiculo"));

			// Carrega o Cache em variavel Local

				var cacheFiltros = JSON.parse(localStorage.getItem("Ingredientes"));
					cacheFiltros = cacheFiltros.filtros.estruturaCampos;

			// Limpa o Formulario

				limpaFormulario("#form_filtros_ingredientes");

			// Verifica se ja existe cache de filtros

				if(cacheFiltros != undefined && cacheFiltros != "" && cacheFiltros != " "){

					// Verifica se existe filtro para cada Campo

						if(cacheFiltros.hasOwnProperty("codigo")){
							$("input[name='filtros_ingredientes_input_Codigo']").val(cacheFiltros.codigo);
						}

						if(cacheFiltros.hasOwnProperty("nome")){
							$("select[name='filtros_ingredientes_input_Nome']").val(cacheFiltros.nome);
						}

				} else {

					// Parametriza os Campos

						$("input[name='filtros_ingredientes_input_Codigo']").val();
						$("input[name='filtros_ingredientes_input_Nome']").val();

				}

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-filtrar-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Ingredientes"));
					cache.filtros.estruturaCampos = {};

			// Declara as Variaveis

				var Codigo = $("input[name='filtros_ingredientes_input_Codigo']");
				var Nome = $("input[name='filtros_ingredientes_input_Nome']");

			// Monta a estrutura de pesquisa

				if(Codigo.val() != "" && Codigo.val() != " " && Codigo.val() != undefined && parseInt(Codigo.val()) > 0){
					cache.filtros.estruturaCampos.Codigo = Codigo.val();
					estruturaPesquisa += "&codigo = " + Codigo.val();
				}

				if(Nome.val() != "" && Nome.val() != " " && Nome.val() != undefined && parseInt(Nome.val()) > 0){
					cache.filtros.estruturaCampos.Nome = Nome.val();
					estruturaPesquisa += "&codmensalista = " + Nome.val();
				}

			// Define os filtros no cache

				cache.paginaAtual = 1;

			// Grava no cache

				localStorage.setItem("Ingredientes", JSON.stringify(cache));

			// Exibe o Botão de Remover Filtro

				$("#div-btn-remover-filtros-ingredientes").show();

			// Carrega as Entradas Avulsas
		
				CarregaIngredientes();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Executa o carregamento das Entradas Avulsas

		$(document).on("click", "#btn-remover-filtros-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Carrega o Cache em variavel Local

				var cache = JSON.parse(localStorage.getItem("Ingredientes"));

			// Limpa o cache

				cache.filtros.estruturaCampos = "";

			// Grava no cache

				localStorage.setItem("Ingredientes", JSON.stringify(cache));

			// Limpa o Formulario

				limpaFormulario("#form_filtros_ingredientes");

			// Oculta o Botão de Remover Filtro

				$("#div-btn-remover-filtros-ingredientes").hide();

			// Carrega as Entradas Avulsas
		
				CarregaIngredientes();

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-cad-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Define as Mascaras

				$("input[name='modal_cad_ingredientes_input_ValorVenda']").mask('000.000.000.000.000,00', {reverse: true});
				$("input[name='modal_cad_ingredientes_input_ValorCompra']").mask('000.000.000.000.000,00', {reverse: true});

			// Limpa o Formulario

				limpaFormulario("#form_cad_Ingredientes");

			// Remove as classes de erros

				$("#modal_cad_ingredientes_div_Nome").removeClass("has-error");
				$("#modal_cad_ingredientes_div_Descricao").removeClass("has-error");
				$("#modal_cad_ingredientes_div_ValorVenda").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_cad_Ingredientes");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_ingredientes_input_Nome

		$(document).on('change', "input[name='modal_cad_ingredientes_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Ingredientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_cad_ingredientes_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_cad_ingredientes_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome do lanche precisa ser preenchido', 'Ops!');
							$("#modal_cad_ingredientes_div_Nome").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_cad_ingredientes_input_ValorVenda

		$(document).on('change', "input[name='modal_cad_ingredientes_input_ValorVenda']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_cad_Ingredientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ValorVenda = $("input[name='modal_cad_ingredientes_input_ValorVenda']").val();
							ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
							ValorVenda = parseFloat(ValorVenda);

					// Realiza a Validação

						if(ValorVenda > 0){
							$("#modal_cad_ingredientes_div_ValorVenda").removeClass("has-error");
						} else {
							toastr.error('Especifique o Valor para Venda do Ingrediente', 'Ops!');
							$("#modal_cad_ingredientes_div_ValorVenda").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza o cadastro da Nova Ingrediente

		$(document).on('click', "#btn-modal-cad_ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Nome = $("input[name='modal_cad_ingredientes_input_Nome']");
				var Descricao = $("input[name='modal_cad_ingredientes_input_Descricao']");
				var ValorVenda = $("input[name='modal_cad_ingredientes_input_ValorVenda']").val();
					ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
					ValorVenda = parseFloat(ValorVenda);
				var ValorCompra = $("input[name='modal_cad_ingredientes_input_ValorCompra']");
				var BotaoModalCadIngredientes = $("#btn-modal-cad_ingredientes");

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = ValorVenda > 0;

				var verificaValidacao = validacao[0] && validacao[1];

			// Desativa o botão

				BotaoModalCadIngredientes.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'nome' : Nome.val(),
							'descricao' : Descricao.val(),
							'valor_venda' : ValorVenda,
							'valor_compra' : parseFloat((ValorCompra.val()).replace(/\./g, "").replace(/\,/g, "."))
						},
						url: 'api/ingredientes/inserir',
						beforeSend: function(data){
							swal({
								title: 'Cadastrando Novo Ingrediente',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
							BotaoModalCadIngredientes.prop("disabled", false);
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								// if(retorno.hasOwnProperty("Status")){
								// 	swal.close();
								// 	swal({
								// 		title: retorno.mensagem_titulo,
								// 		text: retorno.mensagem_texto,
								// 		type: retorno.mensagem_tipo,
								// 		allowOutsideClick: false
								// 	})

								// 	BotaoModalCadIngredientes.prop("disabled", false);

								// 	return
								// }

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_cad_Ingredientes");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaIngredientes();

									// Ativa o botão modal-ingredientes

										BotaoModalCadIngredientes.prop("disabled", false);

								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_cad_ingredientes_div_Nome").addClass("has-error");
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
						}
						
						if(!validacao[1]){
							$("#modal_cad_ingredientes_div_ValorVenda").addClass("has-error");
							toastr.error('O Valor de Venda precisa ser preenchido', 'Ops!');
						}
									
					// Ativa o Botão
					
						BotaoModalCadIngredientes.prop("disabled", false);

				}
		});
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Gerencia a Modal de Nova Entrada

		$(document).on('click', "#btn-alt-ingredientes", ()=>{

			// Renderiza o Icheck

		        renderizaICheck('input[name="modal_alt_ingredientes_input_VagaPreferencial"]',1);
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }
			
			// Pega os Dados da Ingrediente

				var DadosIngredientes = JSON.parse(this.activeElement.getAttribute("data-ingredientes"));
					DadosIngredientes.valor_venda = (DadosIngredientes.valor_venda).replace(/\,/g, "").replace(/\./g, ",");
					DadosIngredientes.valor_compra = (DadosIngredientes.valor_compra).replace(/\,/g, "").replace(/\./g, ",");

			// Define as Mascaras

				$("input[name='modal_alt_ingredientes_input_ValorVenda']").mask('000.000.000.000.000,00', {reverse: true});
				$("input[name='modal_alt_ingredientes_input_ValorCompra']").mask('000.000.000.000.000,00', {reverse: true});

			// Limpa o Formulario

				limpaFormulario("#form_alt_Ingredientes");

			// Define o valor dos campos

				$("input[name='modal_alt_ingredientes_input_Codigo']").val(DadosIngredientes.codigo);
				$("input[name='modal_alt_ingredientes_input_Nome']").val(DadosIngredientes.nome);
				$("input[name='modal_alt_ingredientes_input_NomeAnterior']").val(DadosIngredientes.nome);
				$("input[name='modal_alt_ingredientes_input_Descricao']").val(DadosIngredientes.descricao);
				$("input[name='modal_alt_ingredientes_input_ValorVenda']").val(DadosIngredientes.valor_venda);
				$("input[name='modal_alt_ingredientes_input_ValorCompra']").val(DadosIngredientes.valor_compra);
			// Remove as classes de erros

				$("#modal_alt_ingredientes_div_Nome").removeClass("has-error");
				$("#modal_alt_ingredientes_div_Descricao").removeClass("has-error");
				$("#modal_alt_ingredientes_div_ValorVenda").removeClass("has-error");

			// Abre a Modal

				abriraModal("#modal_alt_Ingredientes");
		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_ingredientes_input_Nome

		$(document).on('change', "input[name='modal_alt_ingredientes_input_Nome']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Ingredientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var Nome = $("input[name='modal_alt_ingredientes_input_Nome']");

					// Realiza a Validação

						if(Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " "){
							$("#modal_alt_ingredientes_div_Nome").removeClass("has-error");
						} else {
							toastr.error('O Nome do lanche precisa ser preenchido', 'Ops!');
							$("#modal_alt_ingredientes_div_Nome").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Verifica o Campo modal_alt_ingredientes_input_ValorVenda

		$(document).on('change', "input[name='modal_alt_ingredientes_input_ValorVenda']", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Verifica o Status da Modal

				var statusModal = $("#modal_alt_Ingredientes").iziModal("getState");

				if(statusModal == "opened"){

					// Declaração das Variaveis

						var ValorVenda = $("input[name='modal_alt_ingredientes_input_ValorVenda']").val();
							ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
							ValorVenda = parseFloat(ValorVenda);

					// Realiza a Validação

						if(ValorVenda > 0){
							$("#modal_alt_ingredientes_div_ValorVenda").removeClass("has-error");
						} else {
							toastr.error('Especifique o Valor para Venda do Ingrediente', 'Ops!');
							$("#modal_alt_ingredientes_div_ValorVenda").addClass("has-error");
						}

				}

		})

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a alteração da Ingrediente

		$(document).on('click', "#btn-modal-alt_ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var Codigo = $("input[name='modal_alt_ingredientes_input_Codigo']");
				var Nome = $("input[name='modal_alt_ingredientes_input_Nome']");
				var NomeAnterior = $("input[name='modal_alt_ingredientes_input_NomeAnterior']");
				var Descricao = $("input[name='modal_alt_ingredientes_input_Descricao']");
				var ValorVenda = $("input[name='modal_alt_ingredientes_input_ValorVenda']").val();
					ValorVenda = ValorVenda.replace(/\./g, "").replace(/\,/g, ".");
					ValorVenda = parseFloat(ValorVenda);
				var ValorCompra = $("input[name='modal_alt_ingredientes_input_ValorCompra']");
				var BotaoModalAltIngredientes = $("#btn-modal-alt_ingredientes");

			// Realiza a validação nos campos

				var validacao = [];
					validacao[0] = Nome.val() != undefined && Nome.val() != null && Nome.val() != "" && Nome.val() != " ";
					validacao[1] = ValorVenda > 0;

				var verificaValidacao = validacao[0] && validacao[1];

			// Desativa o botão

				BotaoModalAltIngredientes.prop("disabled", true);

			// Verifica a validação e se for verdadeira executa o ajax

				if(verificaValidacao == true){

					$.ajax({
						type: "POST",
						dataType: "html",
						headers: {
							Authorization : Sessao.Token 
						},
						data: {
							'codigo' : Codigo.val(),
							'nome' : Nome.val(),
							'nome_anterior' : NomeAnterior.val(),
							'descricao' : Descricao.val(),
							'valor_venda' : ValorVenda,
							'valor_compra' : parseFloat((ValorCompra.val()).replace(/\./g, "").replace(/\,/g, "."))
						},
						url: 'api/ingredientes/atualizar',
						beforeSend: function(data){
							swal({
								title: 'Alterando Ingrediente',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
							BotaoModalAltIngredientes.prop("disabled", false);
						},		
						success: function(retorno) {

							// Trata o retorno transformando em array

								retorno = jQuery.parseJSON(retorno);

							// Verifica se retornou algum erro

								// if(retorno.hasOwnProperty("Status")){
								// 	swal.close();
								// 	swal({
								// 		title: retorno.mensagem_titulo,
								// 		text: retorno.mensagem_texto,
								// 		type: retorno.mensagem_tipo,
								// 		allowOutsideClick: false
								// 	})

								// 	BotaoModalAltIngredientes.prop("disabled", false);

								// 	return
								// }

							// Promisse do sweet alert

								swal({
									title: retorno.mensagem_titulo,
									text: retorno.mensagem_texto,
									type: retorno.mensagem_tipo,
									allowOutsideClick: false
								}).then(()=>{

									// Fecha a Modal

										fecharModal("#modal_alt_Ingredientes");

									// Chama a função para Carregar as Entradas Avulsas
									
										CarregaIngredientes();

									// Ativa o botão modal-ingredientes

										BotaoModalAltIngredientes.prop("disabled", false);
								
								})
						}
					});

				} else {

					// Verifica o Campo que esta incorreto e destaca o mesmo

						if(!validacao[0]){
							$("#modal_alt_ingredientes_div_Nome").addClass("has-error");
							toastr.error('O Nome precisa ser preenchido', 'Ops!');
						}
						
						if(!validacao[1]){
							$("#modal_alt_ingredientes_div_ValorVenda").addClass("has-error");
							toastr.error('O Valor de Venda precisa ser preenchido', 'Ops!');
						}
									
					// Ativa o Botão
					
						BotaoModalAltIngredientes.prop("disabled", false);

				}
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Ingrediente

		$(document).on('click', "#btn-del-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var DadosIngredientes = JSON.parse(this.activeElement.getAttribute("data-ingredientes"));
				var Codigo = DadosIngredientes.codigo;

			// Verifica se realmente irá excluir o registro e se for verdadeira executa o ajax

				swal({
					title: 'Deseja prosseguir com a exclusão deste Ingrediente?',
					text: "Lembramos que após excluir, não havera volta!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Deletar',
					cancelButtonText: 'Cancelar',
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: true,
					allowOutsideClick: false
				}).then(function(data){

					if (data.value != undefined) {

						$.ajax({
							type: "POST",
							dataType: "html",
							headers: {
								Authorization : Sessao.Token 
							},
							data: {
								'codigo' : Codigo
							},
							url: 'api/ingredientes/excluir',
							beforeSend: function(data){
								swal({
									title: 'Excluindo Ingrediente',
									text: 'Aguarde',
									onOpen: () => {
									  swal.showLoading()
									},
									allowOutsideClick: false
								})
							},
							error: function(erro) {
								var mensagem = null; 
								if (erro == "timeout") {				        						
									mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
								}
								else {				        						
									mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
								}
								swal.close();
								swal({
									title: "Ops!",
									text: mensagem,
									type: "error",
									allowOutsideClick: false
								})
							},		
							success: function(retorno) {

								// Trata o retorno transformando em array

									retorno = jQuery.parseJSON(retorno);

								// Verifica se retornou algum erro

									// if(retorno.hasOwnProperty("Status")){
									// 	swal.close();
									// 	swal({
									// 		title: retorno.mensagem_titulo,
									// 		text: retorno.mensagem_texto,
									// 		type: retorno.mensagem_tipo,
									// 		allowOutsideClick: false
									// 	})

									// 	return
									// }

								// Promisse do sweet alert

									swal({
										title: retorno.mensagem_titulo,
										text: retorno.mensagem_texto,
										type: retorno.mensagem_tipo,
										allowOutsideClick: false
									}).then(()=>{

										// Chama a função para Carregar as Entradas Avulsas
										
											CarregaIngredientes();
									
									})
							}
						});

					} 

				});
		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Realiza a exclusão da Ingrediente

		$(document).on('click', "#btn-ativar-ingredientes, #btn-desativar-ingredientes", ()=>{
			
			// Verificação de Permissão de Modulo

				// if(AcessoModulo(Sessao.Permissoes) == false){
				// 	return false;
				// }

			// Declara as variaveis
				
				var DadosIngredientes = JSON.parse(this.activeElement.getAttribute("data-ingredientes"));
				var Codigo = DadosIngredientes.codigo;

				switch((this.activeElement.id).split("-")[1]){
					case "ativar":
						var Status = 1;
						break;
					case "desativar":
						var Status = 2;
						break;
				}

			// executa o ajax

				$.ajax({
					type: "POST",
					dataType: "html",
					headers: {
						Authorization : Sessao.Token 
					},
					data: {
						'codigo' : Codigo,
						'cod_status' : Status
					},
					url: 'api/ingredientes/atualizar',
					beforeSend: function(data){
						swal({
							title: 'Atualizando Status do Ingrediente',
							text: 'Aguarde',
							onOpen: () => {
							  swal.showLoading()
							},
							allowOutsideClick: false
						})
					},
					error: function(erro) {
						var mensagem = null; 
						if (erro == "timeout") {				        						
							mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
						}
						else {				        						
							mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
						}
						swal.close();
						swal({
							title: "Ops!",
							text: mensagem,
							type: "error",
							allowOutsideClick: false
						})
					},		
					success: function(retorno) {

						// Trata o retorno transformando em array

							retorno = jQuery.parseJSON(retorno);

						// Verifica se retornou algum erro

							// if(retorno.hasOwnProperty("Status")){
							// 	swal.close();
							// 	swal({
							// 		title: retorno.mensagem_titulo,
							// 		text: retorno.mensagem_texto,
							// 		type: retorno.mensagem_tipo,
							// 		allowOutsideClick: false
							// 	})

							// 	return
							// }

						// Promisse do sweet alert

							swal({
								title: retorno.mensagem_titulo,
								text: retorno.mensagem_texto,
								type: retorno.mensagem_tipo,
								allowOutsideClick: false
							}).then(()=>{

								// Chama a função para Carregar as Entradas Avulsas
								
									CarregaIngredientes();
							
							})
					}
				});

		});

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

});