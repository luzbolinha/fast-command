// Função para Mascara de Telefone

	var SPMaskBehavior = function (val) {
	  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
	  onKeyPress: function(val, e, field, options) {
	      field.mask(SPMaskBehavior.apply({}, arguments), options);
	    }
	};

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
	
//+--------------------------------------------------------------------------------+
//|                                                                                |
//|                jQuery Com Gerenciamento das Funções Padrões	    	           |
//|                                                                                |
//+--------------------------------------------------------------------------------+

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Gerenciar Arquivos via Input

		function readFile(input, idview, sizemax = 5.00) {

			if (input.files && input.files[0]) {

				// Verifica a Extensão do arquivo

					var extension = input.files[0].name.split('.').pop().toLowerCase();

				// Defini o Tamanho da Imagem em MB

					var TamanhoImagem = parseFloat(((input.files[0].size / 1024) / 1024).toFixed(2));

				// Condição para Verificar se o tipo de arquivo é aceitavel ou não

					if (upload.Types.indexOf(extension) > -1) {

						// Verifica o tamanho da imagem

							if(TamanhoImagem > sizemax){

								// Define a Array de retorno com as informações da imagem

									var imgArray = {
										input: input.files[0],
										error: "O Arquivo atual possui " + TamanhoImagem + " MB e o máximo permitido é de " + sizemax + " MB, verifique e tente novamente"
									}

								// Retorna a Array

									return imgArray;

							}

						// Inicia a Classe FileReader

							var reader = new FileReader();

						// Evento para o Inicio da leitura

							reader.onloadstart = (e)=>{

								// Carrega o Sweetalert	

									swal({
										title: 'Carregando Imagem',
										text: 'Aguarde',
										onOpen: () => {
										  swal.showLoading()
										},
										showConfirmButton: false,
										allowOutsideClick: false
									})

							}

						// Renderiza a imagem no idview

							reader.onload = (e)=>{

								// Fecha o Sweetalert

									swal.close();

								// Renderiza a imagem na div

						    		$(idview).attr('src', e.target.result);

						    }

						// Realiza a leitura do arquivo

							reader.readAsDataURL(input.files[0]);

						// Define a Array de retorno com as informações da imagem

							var imgArray = {
								input: input.files[0]
							}

						// Retorna a Array

							return imgArray;

					} else {

						// Define a Array de retorno com as informações da imagem

							var imgArray = {
								input: input.files[0],
								error: "O Arquivo contém formato inválido, verifique e tente novamente"
							}

						// Retorna a Array

							return imgArray;

					}

			} else {

				// Define a Array de retorno com as informações da imagem

					var imgArray = {
						input: false
					}

				// Retorna a Array

					return imgArray;

			}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para renderizar a modal
	
		function renderizaModal(modal_id, title, options){

			if(options == undefined){
				options = {}
			}

			$(modal_id).iziModal({
				title: title,
				subtitle: '',
				headerColor: options.hasOwnProperty("headerColor") ? options.headerColor : '#EEEEEE',
				background: options.hasOwnProperty("background") ? options.background : '#FFFFFF',
				theme: 'light',  // light
				appendTo: options.hasOwnProperty("appendTo") ? options.appendTo : '.body', // or false
				icon: null,
				iconText: null,
				iconColor: '',
				rtl: false,
				width: options.hasOwnProperty("width") ? options.width : 600,
				top: null,
				bottom: null,
				borderBottom: true,
				padding: 0,
				radius: 0,
				zindex: options.hasOwnProperty("zindex") ? options.zindex : 1050,
				iframe: false,
				iframeHeight: 400,
				iframeURL: null,
				focusInput: true,
				group: '',
				loop: false,
				navigateCaption: true,
				navigateArrows: true, // Boolean, 'closeToModal', 'closeScreenEdge'
				autoOpen: 0, // Boolean, Number
				bodyOverflow: false,
				fullscreen: false,
				openFullscreen: false,
				closeOnEscape: true,
				closeButton: true,
				overlay: true,
				overlayClose: false,
				overlayColor: 'rgba(0, 0, 0, 0.4)',
				timeout: false,
				timeoutProgressbar: false,
				pauseOnHover: true,
				timeoutProgressbarColor: 'rgba(255,255,255,0.5)',
				transitionIn: 'comingIn',
				transitionOut: 'comingOut',
				transitionInOverlay: 'fadeIn',
				transitionOutOverlay: 'fadeOut'
			});
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para pegar a URL

		function pegaUrl(){

			// Retorna a Url atual

				return window.location.href

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerenciar o menu

		function gerenciaMenu(){

			// Declara as Variaveis

				var url = pegaUrl();
				var urlSplit = url.split("/");
				var tamanhoUrl = urlSplit.length;
				var secao = 0;
				var subsecao = 0;
				var extensao = ".asp";
				var permissao = null;
				var permissoes = Sessao.Permissoes;
				var modulo = Object.keys(permissoes);
				var itemMenu = null;

			// Renderiza os itens do Menu

				for(var ponteiroPermissoes = 0;ponteiroPermissoes < modulo.length; ponteiroPermissoes++){

					// Define o modulo que sera verificado

						permissao = modulo[ponteiroPermissoes]
						itemMenu = permissao.toLowerCase();

					// Verifica se o modulo está ativado ou não

						if(permissao != "Desenvolvedor"){
							if(JSON.parse(permissoes[permissao])){
								$(menu[itemMenu].id).show();
							} else {
								$(menu[itemMenu].id).hide();
							}
						}

				}


			// Extrai a seção da pagina

				if(tamanhoUrl == 6){
					secao = urlSplit[5]
				} else if(tamanhoUrl == 5) {
					secao = urlSplit[4]
				} else {
					secao = urlSplit[3]
				}

			// Retira a extensão da seção

				secao = secao.replace(extensao,"");
				secao = secao.replace(/\?+[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+$/g,"");
				secao = secao.replace(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]/g,"");
				secao = secao.toLowerCase();

			// Verifica se possui subsecao

				if(secao.indexOf("_") > -1){

					// Define a subsecao

						subsecao = secao.split("_")[1];
						secao = secao.split("_")[0];
					
					// Define o item do menu ativo conforme secao

						menu[secao].active = true;
						$(menu[secao].id).addClass("active");
						menu[secao].submenu[subsecao].active = true;
						$(menu[secao].submenu[subsecao].id).addClass("active");

				} else {

					// Define o item do menu ativo conforme secao

						menu[secao].active = true;
						$(menu[secao].id).addClass("active");
					
				}

		}


//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerenciar o modulo no qual esta acessando

		function gerenciaModulo(){

			// Declara as Variaveis

				var url = pegaUrl();
				var urlSplit = url.split("/");
				var tamanhoUrl = urlSplit.length;
				var secao = 0;
				var subsecao = 0;
				var extensao = ".asp";
				var modulo = null;

			// Extrai a seção da pagina

				if(tamanhoUrl == 6){
					secao = urlSplit[5]
				} else if(tamanhoUrl == 5) {
					secao = urlSplit[4]
				} else {
					secao = urlSplit[3]
				}

			// Retira a extensão da seção

				secao = secao.replace(extensao,"")
				secao = secao.replace(/\?+[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+$/g,"")
				secao = secao.replace(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]/g,"")
				secao = secao.toLowerCase();

			// Condiçao para verificar qual modulo esta tentando acessar

				if(secao == "entradaavulsa"){
					modulo = "EntradaAvulsa"
				} else if(secao == "mensalistas"){
					modulo = "Mensalistas"
				} else if(secao == "configuracoes"){

					// Verifica se sera exibido ou não o modulo de Tipo de Usuário

						if(AcessoDesenvolvedor(Sessao.Permissoes)){
							$("#btn-tab-tipoUsuario").show();
						} else {
							$("#btn-tab-tipoUsuario").hide();
						}

				} else if(secao == "relatorios"){
					modulo = "Relatorios"
				} else if(secao == "estacionamento"){
					modulo = "Estacionamento"
				} else if(secao == "fluxocaixa"){
					modulo = "FluxoCaixa"
				}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para renderizar o iCheck

		function renderizaICheck(id, type){

			// Destroi o Icheck

		        $(id).icheck('destroy');

			// Declara as Variaveis

				var checkboxClass = 0;
				var radioClass = 0;

			// Switch para verificar o tipo de iCheck

				switch(parseInt(type)){
					case 1:
						checkboxClass = 'icheckbox_flat-green'
						radioClass = 'iradio_flat-green'
						break;
					case 2:
						checkboxClass = 'icheckbox_minimal-red'
						radioClass = 'iradio_minimal-red'
						break;
					case 3:
						checkboxClass = 'icheckbox_minimal-blue'
						radioClass = 'iradio_minimal-blue'
						break;
					case 4:
						checkboxClass = 'icheckbox_square-yellow'
						radioClass = 'iradio_square-yellow'
						break;
				}

			// Renderiza o iCheck

		        $(id).icheck({
		          checkboxClass: checkboxClass,
		          radioClass: radioClass
		        });

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para abrir a modal
	
		function abriraModal(modal_id){
			$(modal_id).iziModal('open');
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para fechar a modal
	
		function fecharModal(modal_id){
			$(modal_id).iziModal('close');
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para limpar formulario
	
		function limpaFormulario(formulario_id){
			var formulario = $(formulario_id);
			for(ponteiroFormulario = 0; ponteiroFormulario < formulario.length; ponteiroFormulario++){
				for(ponteiroCampos = 0; ponteiroCampos < formulario[ponteiroFormulario].length; ponteiroCampos++){
					formulario[ponteiroFormulario][ponteiroCampos].value = null
				}
			}
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerar estrutura da paginação
	
		function geraEstruturaPaginacao(paginacao_id, cacheName){

			// Declaração das Variaveis

				var cache = JSON.parse(localStorage.getItem(cacheName));
				var pagina_atual = parseInt(cache.paginaAtual);
				var registros_pagina = parseInt(cache.registrosPagina);
				var registros_por_pagina = parseInt(cache.registrosPorPagina);
				var total_registros = parseInt(cache.totalRegistros);
				var pagina_registros = ((pagina_atual * registros_por_pagina) - 5)

				var paginacao = $(paginacao_id);
					paginacaoId = (paginacao_id).replace(/\#/g, "");

				var texto_paginacao = $(paginacao_id+"-texto");

				var totalPaginas = ((total_registros / registros_por_pagina).toLocaleString()).split(",");
					
					if(parseInt(totalPaginas[1]) > 0){
						totalPaginas = parseInt(totalPaginas[0]) + 1
					} else {
						totalPaginas = parseInt(totalPaginas[0])
					}
			
			// Atualiza o total de paginas da array de cache

				cache.totalPaginas = totalPaginas;
			
			// Define os botões Fixos

				var botoesFixos = [];
					botoesFixos[0] = '<li><button type="button" class="btn btn-default" id="btn-'+paginacaoId+'" data-paginacao="1"><i class="fa fa-angle-double-left"></i></button></li>'
					botoesFixos[1] = '<li><button type="button" class="btn btn-default" id="btn-'+paginacaoId+'" data-paginacao="'+(parseInt(pagina_atual)-1)+'"><i class="fa fa-angle-left"></i></button></li>'
					botoesFixos[2] = '<li><button type="button" class="btn btn-default" id="btn-'+paginacaoId+'" data-paginacao="'+(parseInt(pagina_atual)+1)+'"><i class="fa fa-angle-right"></i></button></li>'
					botoesFixos[3] = '<li><button type="button" class="btn btn-default" id="btn-'+paginacaoId+'" data-paginacao="'+totalPaginas+'"><i class="fa fa-angle-double-right"></i></button></li>'

			// Define a variavel de estrutura da paginação

				var estruturaPaginacao = "";
			
			// Condição para exibição dos botões de Pagina Anterior e Primeira Pagina

				if(totalPaginas > 1 && pagina_atual > 1){
					estruturaPaginacao += botoesFixos[0]
					estruturaPaginacao += botoesFixos[1]
				}

			// Loop para gerar os botões das paginas

				for(ponteiroPaginas = 1; ponteiroPaginas <= totalPaginas; ponteiroPaginas++){
					if(ponteiroPaginas == pagina_atual){
						estruturaPaginacao += '<li><button type="button" class="btn btn-default" id="btn-'+paginacaoId+'" data-paginacao="'+ponteiroPaginas+'" style="background-color: #DDDDDD;">'+ponteiroPaginas+'</button></li>'
					} else {
						estruturaPaginacao += '<li><button type="button" class="btn btn-default" id="btn-'+paginacaoId+'" data-paginacao="'+ponteiroPaginas+'">'+ponteiroPaginas+'</button></li>'
					}
				}

			// Condição para exibição dos botões de Proxima Pagina e Ultima Pagina

				if(totalPaginas > 1 && pagina_atual < totalPaginas){
					estruturaPaginacao += botoesFixos[2]
					estruturaPaginacao += botoesFixos[3]
				}

			// Retorna a paginacao no elemento atribuido pela id da função

				paginacao.html(estruturaPaginacao);

			// Atualiza o texto da paginação

				var texto = "Exibindo $de - $para de um total de $total registro(s)"
					texto = texto.replace("$de", registros_pagina > 0 ? parseInt(pagina_registros+1) : 0)
					texto = texto.replace("$para", parseInt(registros_pagina+pagina_registros))
					texto = texto.replace("$total", parseInt(total_registros))
				texto_paginacao.html(texto);

			// Grava no localStorage o cache atualizado

				localStorage.setItem(cacheName, JSON.stringify(cache));
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Funcão para trocar a pagina

		function trocaPagina(objeto, cacheName){

			// Declara as Variaveis

				var paginaAtual = parseInt(objeto.activeElement.getAttribute("data-paginacao"));

			// Carrega o Cache na variavel

				var cache = JSON.parse(localStorage.getItem(cacheName));
					cache.paginaAtual = paginaAtual;

			// Grava as informações no LocalStorage

				localStorage.setItem(cacheName, JSON.stringify(cache));

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Extrair o Cliente

		function ExtraiCliente(codigo, cacheName){

			// Trata a Variavel

				codigo = parseInt(codigo);

			// Carrega o cache na variavel

				var cache = JSON.parse(localStorage.getItem(cacheName));
				var itens_cache = parseInt(cache.length);

			// Loop para Verificar o tipo

				for(Ponteiro = 0; Ponteiro < itens_cache; Ponteiro++){
					if(cache[Ponteiro].codigo == codigo){
						return cache[Ponteiro].nome;
					}
				}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Extrair o Tipo de Veiculo

		function ExtraiTipoVeiculo(codigo, cacheName){

			// Trata a Variavel

				codigo = parseInt(codigo);

			// Carrega o cache na variavel

				var cache = JSON.parse(localStorage.getItem(cacheName));
				var itens_cache = parseInt(cache.length);

			// Loop para Verificar o tipo

				for(Ponteiro = 0; Ponteiro < itens_cache; Ponteiro++){
					if(cache[Ponteiro].Codigo == codigo){
						return cache[Ponteiro].Descricao;
					}
				}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Extrair o Mensalista

		function ExtraiMensalista(codigo, cacheName){

			// Trata a Variavel

				codigo = parseInt(codigo);

			// Carrega o cache na variavel

				var cache = JSON.parse(localStorage.getItem(cacheName));
				var itens_cache = parseInt(cache.length);
				var retorno = null;

			// Loop para Verificar o tipo

				for(Ponteiro = 0; Ponteiro < itens_cache; Ponteiro++){
					if(cache[Ponteiro].Codigo == codigo){
						if(parseInt(cache[Ponteiro].TipoMensalista) == 2){
							retorno = cache[Ponteiro].Nome + " (" + cache[Ponteiro].CNPJ + ")";
						} else {
							retorno = cache[Ponteiro].Nome + " (" + cache[Ponteiro].CPF + ")";
						}
						return retorno;
					}
				}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Extrair o Tipo de Usuário

		function ExtraiUsuario(codigo, cacheName){

			// Trata a Variavel

				codigo = parseInt(codigo);

			// Carrega o cache na variavel

				var cache = JSON.parse(localStorage.getItem(cacheName));
				var itens_cache = parseInt(cache.length);
				var retorno = null;

			// Loop para Verificar o tipo

				for(Ponteiro = 0; Ponteiro < itens_cache; Ponteiro++){
					if(cache[Ponteiro].Codigo == codigo){
						return cache[Ponteiro].Descricao;
					}
				}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Extrair a Forma de Pagamento

		function ExtraiFormaPagamento(codigo, cacheName){

			// Trata a Variavel

				codigo = parseInt(codigo);

			// Carrega o cache na variavel

				var cache = JSON.parse(localStorage.getItem(cacheName));
				var itens_cache = parseInt(cache.length);
				var retorno = null;

			// Loop para Verificar o tipo

				for(Ponteiro = 0; Ponteiro < itens_cache; Ponteiro++){
					if(cache[Ponteiro].Codigo == codigo){
						return cache[Ponteiro].Descricao;
					}
				}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Converter Minutos decimais para Tempo

		function ConverteMinutoDecimal(minutos){

			if(minutos > 0){
				var minutoXhora = minutos / 60;
				var hora = ((minutoXhora).toString()).split(".")[0];
					hora = parseInt(hora);
					hora = hora > 9 ? hora : "0" + hora;
				var minuto = "0." + ((minutoXhora).toString()).split(".")[1];
					minuto = parseFloat(minuto);
					minuto = Math.round(minuto * 60);
					minuto = minuto > 9 ? parseInt(minuto) : "0" + parseInt(minuto);

				return hora + ":" + minuto				
			} else {
				return "00:00"
			}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerar a Notificação

		// function GeraNotificacao(titulo, opcoes){

		// 	var Notificacao = new Notification(titulo, opcoes);

		// }

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Calcular o Valor Total

		function CalculaValorTotal(ValorHora, ValorFracao, TempoTotal, cacheName, aplicarValores){

			// Carrega o Cache

				var cache = JSON.parse(localStorage.getItem(cacheName));

			// Define as Variaveis

				var HabilitaFracao = cache.habilitarFracao;
				var DuracaoFracao = cache.duracaoFracao;
				var HabilitaTolerancia = cache.habilitarTolerancia;
				var DuracaoTolerancia = cache.duracaoTolerancia;

				var estruturaDeCalculo = {
					ValorHora : 0,
					ValorFracao : 0,
					TotalHoras : 0,
					TotalMinutos : 0,
					AplicarHoraCheia : 0,
					AplicarFracao : 0,
					AplicarTolerancia : 0,
					ValorTotal : 0
				}

				estruturaDeCalculo.ValorHora = parseFloat(ValorHora.replace(".", "").replace(",", "."));
				estruturaDeCalculo.ValorFracao = parseFloat(ValorFracao.replace(".", "").replace(",", "."));
				estruturaDeCalculo.TotalHoras = parseInt(TempoTotal.split(":")[0]);
				estruturaDeCalculo.TotalMinutos = parseInt(TempoTotal.split(":")[1]);
				estruturaDeCalculo.AplicarHoraCheia = aplicarValores[0];
				estruturaDeCalculo.AplicarFracao = aplicarValores[1];
				estruturaDeCalculo.AplicarTolerancia = aplicarValores[2];

			// Calcula o valor total a ser pago

				if(HabilitaFracao == 1){
					estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorHora * estruturaDeCalculo.TotalHoras;
					if(estruturaDeCalculo.AplicarHoraCheia && estruturaDeCalculo.TotalMinutos > 0){
						if(HabilitaTolerancia == 1){
							if(estruturaDeCalculo.AplicarTolerancia && estruturaDeCalculo.TotalMinutos > parseInt(DuracaoTolerancia.split(":")[1]) || estruturaDeCalculo.AplicarTolerancia == false){
								estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorTotal + estruturaDeCalculo.ValorHora;  // Tolerância
							}
						} else {
							estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorTotal + estruturaDeCalculo.ValorHora;  // Hora Cheia
						}
					} else if(estruturaDeCalculo.AplicarFracao && estruturaDeCalculo.TotalMinutos > 0){
						if(HabilitaTolerancia == 1){
							if(estruturaDeCalculo.AplicarTolerancia && estruturaDeCalculo.TotalMinutos > parseInt(DuracaoTolerancia.split(":")[1]) || estruturaDeCalculo.AplicarTolerancia == false){
								estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorTotal + estruturaDeCalculo.ValorFracao;  // Tolerância
							}
						} else {
							estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorTotal + estruturaDeCalculo.ValorFracao;  // Fração
						}
					}
				} else {
					estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorHora * estruturaDeCalculo.TotalHoras;
					if(HabilitaTolerancia == 1){
						if(estruturaDeCalculo.AplicarTolerancia && estruturaDeCalculo.TotalMinutos > parseInt(DuracaoTolerancia.split(":")[1]) || estruturaDeCalculo.AplicarTolerancia == false){
							estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorTotal + estruturaDeCalculo.ValorHora;  // Tolerância
						}
					} else {
						if(estruturaDeCalculo.TotalMinutos > 0){
							estruturaDeCalculo.ValorTotal = estruturaDeCalculo.ValorTotal + estruturaDeCalculo.ValorHora; // Hora Cheia
						}
					}
				}

				estruturaDeCalculo.ValorTotal = ((parseFloat(estruturaDeCalculo.ValorTotal).toFixed(2)).toString()).replace(".", ",");

				return estruturaDeCalculo.ValorTotal;

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para gerenciar os logs

		function log(tipo, mensagem){
			switch(tipo){
				case "error":
					console.error(mensagem);
					break;
				case "warn":
					console.warn(mensagem);
					break;
				default:
					console.log(mensagem);
					break;
			}
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para consultar o cep no viaCep

		function consultaCEP(cep){

			// Trata o CEP recebido

				cep = cep.replace(/\-/g,"");
				cep = cep.replace(/\./g,"");

			// Verifica se o cep possui 8 digitos

				if(cep.length == 8){

					$.ajax({
						type: "get",
						dataType: "html",
						url: "https://viacep.com.br/ws/"+cep+"/json/",
						beforeSend: function(data){
							swal({
								title: 'Consultando CEP',
								text: 'Aguarde',
								onOpen: () => {
								  swal.showLoading()
								},
								allowOutsideClick: false
							})
						},
						error: function(erro) {
							var mensagem = null; 
							if (erro == "timeout") {				        						
								mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
							}
							else {				        						
								mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
							}
							swal.close();
							swal({
								title: "Ops!",
								text: mensagem,
								type: "error",
								allowOutsideClick: false
							})
						},		
						success: function(retorno) {

							// Fecha o SweetAlert

								swal.close();

							// Trata o retorno transformando em array

								return jQuery.parseJSON(retorno);
						}
					});

				} else {
					return false
				}

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Gerar Token

		function GeraToken(tamanho, tiposcaracteres = 1){

			// Valida as variaveis de entrada

				if(typeof tamanho != 'number' || !Number.isInteger(tamanho) || tamanho < 1){
					return console.error("function GeraToken(tamanho, tiposcaracteres): a variavel [tamanho] deve conter apenas numeros inteiros e acima de 0");
				}
				if(typeof tiposcaracteres != 'number' || !Number.isInteger(tiposcaracteres) || tiposcaracteres < 1 || tiposcaracteres > 7){
					return console.error("function GeraToken(tamanho, tiposcaracteres): a variavel [tiposcaracteres] deve conter apenas numeros inteiros e estar entre 1 á 7");
				}

			// Declara as Variaveis

				var Token = "";
				var TipoCaracter = null;
				var Caracter = null;

			// Realiza a repetição para gerar o token

				for(var ponteiro = 0;ponteiro < tamanho;ponteiro++){

					TipoCaracter = Math.floor((Math.random() * tiposcaracteres) + 1);

					switch(TipoCaracter){
						case 1:
							Caracter = String.fromCharCode(Math.floor(Math.random() * (tabelaASCII.LetrasMaiusculas.fim - tabelaASCII.LetrasMaiusculas.inicio + 1)) + tabelaASCII.LetrasMaiusculas.inicio);
							break;
						case 2:
							Caracter = String.fromCharCode(Math.floor(Math.random() * (tabelaASCII.LetrasMinusculas.fim - tabelaASCII.LetrasMinusculas.inicio + 1)) + tabelaASCII.LetrasMinusculas.inicio);
							break;
						case 3:
							Caracter = String.fromCharCode(Math.floor(Math.random() * (tabelaASCII.Numeros.fim - tabelaASCII.Numeros.inicio + 1)) + tabelaASCII.Numeros.inicio);
							break;
						case 4:
							Caracter = String.fromCharCode(Math.floor(Math.random() * (tabelaASCII.CaracteresEspeciais1.fim - tabelaASCII.CaracteresEspeciais1.inicio + 1)) + tabelaASCII.CaracteresEspeciais1.inicio);
							break;
						case 5:
							Caracter = String.fromCharCode(Math.floor(Math.random() * (tabelaASCII.CaracteresEspeciais2.fim - tabelaASCII.CaracteresEspeciais2.inicio + 1)) + tabelaASCII.CaracteresEspeciais2.inicio);
							break;
						case 6:
							Caracter = String.fromCharCode(Math.floor(Math.random() * (tabelaASCII.CaracteresEspeciais3.fim - tabelaASCII.CaracteresEspeciais3.inicio + 1)) + tabelaASCII.CaracteresEspeciais3.inicio);
							break;
						case 7:
							Caracter = String.fromCharCode(Math.floor(Math.random() * (tabelaASCII.CaracteresEspeciais4.fim - tabelaASCII.CaracteresEspeciais4.inicio + 1)) + tabelaASCII.CaracteresEspeciais4.inicio);
							break;
					}

					Token += Caracter;

				}

			// Retorna o Token

				return Token;

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para identificar o modulo no qual esta acessando

		function AcessoModulo(Permissoes){

			// Declara as Variaveis

				var url = pegaUrl();
				var urlSplit = url.split("/");
				var tamanhoUrl = urlSplit.length;
				var secao = 0;
				var subsecao = 0;
				var extensao = ".asp";
				var modulo = null;

			// Extrai a seção da pagina

				if(tamanhoUrl == 6){
					secao = urlSplit[5]
				} else if(tamanhoUrl == 5) {
					secao = urlSplit[4]
				} else {
					secao = urlSplit[3]
				}

			// Retira a extensão da seção

				secao = secao.replace(extensao,"")
				secao = secao.replace(/\?+[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]+$/g,"")
				secao = secao.replace(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?.]/g,"")
				secao = secao.toLowerCase();

			// Condiçao para verificar qual modulo esta tentando acessar

				if(secao == "entradaavulsa"){
					modulo = "EntradaAvulsa"
				} else if(secao == "mensalistas"){
					modulo = "Mensalistas"
				} else if(secao == "configuracoes"){
					modulo = "Configuracoes"
				} else if(secao == "relatorios"){
					modulo = "Relatorios"
				} else if(secao == "estacionamento"){
					modulo = "Estacionamento"
				} else if(secao == "fluxocaixa"){
					modulo = "FluxoCaixa"
				}

			// Verifica se o modulo que esta tentando acessar é permitido pelas permissões

				if(Permissoes.hasOwnProperty(modulo)){
					if(JSON.parse(Permissoes[modulo])){
						return true
					} else {
						return false
					}
				} else {
					return false
				}
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para verifica se o usuário possui acessos de desenvolvedor

		function AcessoDesenvolvedor(Permissoes){

			if(Permissoes.hasOwnProperty("Desenvolvedor")){
				if(JSON.parse(Permissoes.Desenvolvedor)){
					return true
				} else {
					return false
				}
			} else {
				return false
			}
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Download de URI

		function downloadURI(uri, name){
		    var link = document.createElement("a");
		    link.download = name;
		    link.href = uri;
		    link.click();
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Reiniciar o Bootstrap.toogle

		function reiniciaBootstrapToogle(id){

			// Destroi o componente

				$(id).bootstrapToggle('destroy');

			// Inicializa o componrente

				$(id).bootstrapToggle();
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Carregar um JSON

		function carregaJSON(url, usaCache = false, cache = ""){

			// Define a Promise

				return new Promise((resolve, reject)=>{

					// Verifica se é uma url valida

						var regUrl = /(([http]|[https])+([:])+([\/\/])+([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?)|(([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?)/gm;
						
						if(!regUrl.test(url)){

							// Retorna o log no console

								log("error", "URL inválida");

							// encerra função
							
								return reject(false);

						} else {

							// Realiza o ajax para consulta

								$.ajax({
									type: "get",
									dataType: "JSON",
									crossDomain: true,
									url: url,
									beforeSend: function(data){
										swal({
											title: 'Carregando arquivo JSON',
											text: 'Aguarde',
											onOpen: () => {
											  swal.showLoading()
											},
											allowOutsideClick: false
										})
									},
									error: function(erro) {
										var mensagem = null; 
										if (erro == "timeout") {				        						
											mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
										}
										else {				        						
											mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
										}
										swal.close();
										swal({
											title: "Ops!",
											text: mensagem,
											type: "error",
											allowOutsideClick: false
										}).then((data)=>{
											reject();
										})
									},		
									success: function(retorno) {

										// Fecha o SweetAlert

											swal.close();

										// Verifica se irá gravar em cache

											if(usaCache && cache != "" && cache != null && cache != " "){
												localStorage.setItem(cache, JSON.stringify(retorno));
											}

										// Trata o retorno transformando em array

											return resolve(retorno);
									}
								});

						}

				});

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Tratar o Texto com regex

		function EstruturaMensagemComVariavel(texto, variavel){

			// Declara as Variaveis

				var dePara = null;
				var ArrayVariaveis = [];

			// Verifica qual Array de Variaveis ira utilizar

				switch(variavel){
					case "MensagemCupomAvulsoEntrada":
						ArrayVariaveis = Object.keys(cupomSMS.MensagemCupomAvulsoEntrada.variaveis);
						dePara = cupomSMS.MensagemCupomAvulsoEntrada.dePara;
						break;
					case "MensagemCupomAvulsoSaida":
						ArrayVariaveis = Object.keys(cupomSMS.MensagemCupomAvulsoSaida.variaveis);
						dePara = cupomSMS.MensagemCupomAvulsoSaida.dePara;
						break;
					case "MensagemCupomMensalidade":
						ArrayVariaveis = Object.keys(cupomSMS.MensagemCupomMensalidade.variaveis);
						dePara = cupomSMS.MensagemCupomMensalidade.dePara;
						break;
					default:
						return false
						break;
				}

			// Verifica as Variaveis e realiza a substituição dos valores

				if(variavel == "MensagemCupomAvulsoEntrada" || variavel == "MensagemCupomAvulsoSaida"){

					// Carrega o Cache do Cupom

						var cache = JSON.parse(localStorage.getItem("CupomFiscalAvulso"));

				} else if(variavel == "MensagemCupomMensalidade"){

					// Carrega o Cache do Cupom

						var cache = JSON.parse(localStorage.getItem("CupomFiscalMensalidade"));

				}

			// Função para Realizar o forEach

				function forEachTexto(element, index, array){
					if(texto.indexOf(element) > -1){
						texto = texto.replace(element, cache[(dePara[element]).split(".")[0]][(dePara[element]).split(".")[1]]);
					}
				}

			// Executa o forEach

				ArrayVariaveis.forEach(forEachTexto);

			// Estrutura o retorno

				var retorno = {
					mensagem : texto,
					totalCaracteres : texto.length
				}

			// Retorna a Mensagem

				return retorno

		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Tratar o Texto com regex

		function TrataTexto(texto, regex_para_texto = false){
			var texto_tratado = texto
			if(regex_para_texto){
				texto_tratado = texto_tratado.replace(/\n/gm, "/n");
				texto_tratado = texto_tratado.replace(/\r/gm, "/r");
				return texto_tratado;
			} else {
				texto_tratado = texto_tratado.replace(/\/n/gm, "\n");
				texto_tratado = texto_tratado.replace(/\/r/gm, "\r");
				return texto_tratado;
			}
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função que gerencia o Base64

		var Base64 = {
			_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
			encode: function(e) {
				var t="";
				var n,r,i,s,o,u,a;
				var f=0;
				e=Base64._utf8_encode(e);
				while(f<e.length){
					n=e.charCodeAt(f++);
					r=e.charCodeAt(f++);
					i=e.charCodeAt(f++);
					s=n>>2;
					o=(n&3)<<4|r>>4;
					u=(r&15)<<2|i>>6;
					a=i&63;
					if(isNaN(r)){
						u=a=64
					} else if(isNaN(i)){
						a=64
					}
					t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)
				}
				return t
			},
			decode: function(e) {
				var t="";
				var n,r,i;
				var s,o,u,a;
				var f=0;
				e=e.replace(/[^A-Za-z0-9+/=]/g,"");
				while(f<e.length){
					s=this._keyStr.indexOf(e.charAt(f++));
					o=this._keyStr.indexOf(e.charAt(f++));
					u=this._keyStr.indexOf(e.charAt(f++));
					a=this._keyStr.indexOf(e.charAt(f++));
					n=s<<2|o>>4;
					r=(o&15)<<4|u>>2;
					i=(u&3)<<6|a;
					t=t+String.fromCharCode(n);
					if(u!=64){
						t=t+String.fromCharCode(r)
					}
					if(a!=64){
						t=t+String.fromCharCode(i)
					}
				}
				t=Base64._utf8_decode(t);
				return t
			},
			_utf8_encode: function(e) {
				e=e.replace(/rn/g,"n");
				var t="";
				for(var n=0; n<e.length; n++){
					var r=e.charCodeAt(n);
					if(r<128){
						t+=String.fromCharCode(r)
					} else if(r>127&&r<2048){
						t+=String.fromCharCode(r>>6|192);
						t+=String.fromCharCode(r&63|128)
					} else {
						t+=String.fromCharCode(r>>12|224);
						t+=String.fromCharCode(r>>6&63|128);
						t+=String.fromCharCode(r&63|128)
					}
				}
				return t
			},
			_utf8_decode: function(e) {
				var t="";
				var n=0;
				var r=c1=c2=0;
				while(n<e.length){
					r=e.charCodeAt(n);
					if(r<128){
						t+=String.fromCharCode(r);
						n++
					} else if(r>191&&r<224){
						c2=e.charCodeAt(n+1);
						t+=String.fromCharCode((r&31)<<6|c2&63);
						n+=2
					} else {
						c2=e.charCodeAt(n+1);
						c3=e.charCodeAt(n+2);
						t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);
						n+=3
					}
				}
				return t
			}
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Verifica o navegador é mobile/tablet ou não

		function checaAcessoMobile() {
		  var check = false;
		  var regex1 = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i;
		  var regex2 = /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i;
		  (function(a){if(regex1.test(a)||regex2.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
		  return check;
		}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	// Função para Enviar SMS

		function EnviaSMS(API, Dados, configSMS, configSMSCupom){

			// Define a Estrutura dos dados que serão enviados

				var EstruturaDados = {};

			// Define os Dados que a API precisa para fazer o Envio

				var DadosAPI = API.methodsAPI.Envio.dados;

			// Cria a Função de forEach para estruturar os dados

				function forEachDados(element, index, array){
					if(Dados.hasOwnProperty((element).replace("*", ""))){
						if(element.indexOf("*") > -1){
							if(Dados[(element).replace("*", "")] == "" || Dados[(element).replace("*", "")] == " " || Dados[(element).replace("*", "")] == undefined || Dados[(element).replace("*", "")] == null){
								return false
							} else {
								EstruturaDados[(element).replace("*", "")] = Dados[(element).replace("*", "")];
							}
						} else {
							EstruturaDados[element] = Dados[element];
						}
					}
				}

				DadosAPI.forEach(forEachDados);

			// Verifica se a Estrutura de Dados não esta vázia

				if(EstruturaDados != false){

					// Verifica se a API necessita de token

						if(API.methodsAPI.Envio.needToken){

							// Realiza o Ajax de Envio

								$.ajax({
									type: API.methodsAPI.Envio.Request.type,
									dataType: API.methodsAPI.Envio.Request.dataType,
									contentType: API.methodsAPI.Envio.Request.contentType,
									data: EstruturaDados,
									headers: {
										Authorization : "Basic " + configSMS.Token
									},
									url: API.methodsAPI.Envio.url,
									beforeSend: function(data){
										swal({
											title: 'Realizando Envio da Mensagem',
											text: 'Aguarde',
											onOpen: () => {
											  swal.showLoading()
											},
											allowOutsideClick: false
										})
									},
									error: function(erro) {
										var mensagem = null; 
										if (erro == "timeout") {				        						
											mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
										}
										else {				        						
											mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
										}
										swal.close();
										swal({
											title: "Ops!",
											text: mensagem,
											type: "error",
											allowOutsideClick: false
										})
									},		
									success: function(retorno) {

										// Checa se o retorna é uma array e caso não seja Transforma o Retorno em Array

											if(typeof retorno != "object"){
												retorno = JSON.parse(retorno);
											}

										// Define as Variaveis

											var Funcao = 1;

										// Realiza o Ajax para Salvar no historico o sms Enviado

											$.ajax({
												type: "POST",
												dataType: "html",
												headers: {
													Authorization : Sessao.Token 
												},
												data: {
													'ConfigSMS' : configSMS.Codigo,
													'Mensagem' : EstruturaDados.text == undefined ? EstruturaDados.content : EstruturaDados.text,
													'Numero' : EstruturaDados.number,
													'LogJSON' : JSON.stringify(retorno),
													'Funcao' : Funcao
												},
												url: 'ajax/historicoSMS_ajax.asp',
												beforeSend: function(data){
													swal({
														title: 'Registrando no Historico de SMS',
														text: 'Aguarde',
														onOpen: () => {
														  swal.showLoading()
														},
														allowOutsideClick: false
													})
												},
												error: function(erro) {
													var mensagem = null; 
													if (erro == "timeout") {				        						
														mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
													}
													else {				        						
														mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
													}
													swal.close();
													swal({
														title: "Ops!",
														text: mensagem,
														type: "error",
														allowOutsideClick: false
													})
												},		
												success: function(retorno) {

													// Fechar o SweetAlert

														swal.close();

													// Trata o retorno transformando em array

														retorno = jQuery.parseJSON(retorno);

													// Verifica se retornou algum erro

														if(retorno.hasOwnProperty("Status")){
															swal.close();
															swal({
																title: retorno.MensagemTitulo,
																text: retorno.MensagemTexto,
																type: retorno.MensagemTipo,
																allowOutsideClick: false
															})

														}

													// Promise do sweet alert

														swal({
															title: retorno.MensagemTitulo,
															text: retorno.MensagemTexto,
															type: retorno.MensagemTipo,
															allowOutsideClick: false
														})

												}
											})

									}
								})

						} else {

							// Realiza o Ajax de Envio

								$.ajax({
									type: API.methodsAPI.Envio.Request.type,
									dataType: API.methodsAPI.Envio.Request.dataType,
									data: EstruturaDados,
									url: API.methodsAPI.Envio.url,
									beforeSend: function(data){
										swal({
											title: 'Realizando Envio da Mensagem',
											text: 'Aguarde',
											onOpen: () => {
											  swal.showLoading()
											},
											allowOutsideClick: false
										})
									},
									error: function(erro) {
										var mensagem = null; 
										if (erro == "timeout") {				        						
											mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
										}
										else {				        						
											mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
										}
										swal.close();
										swal({
											title: "Ops!",
											text: mensagem,
											type: "error",
											allowOutsideClick: false
										})
									},		
									success: function(retorno) {

										// Checa se o retorna é uma array e caso não seja Transforma o Retorno em Array

											if(typeof retorno != "object"){
												retorno = JSON.parse(retorno);
											}

										// Define as Variaveis

											var Funcao = 1;

										// Realiza o Ajax para Salvar no historico o sms Enviado

											$.ajax({
												type: "POST",
												dataType: "html",
												headers: {
													Authorization : Sessao.Token 
												},
												data: {
													'ConfigSMS' : configSMS.Codigo,
													'Mensagem' : EstruturaDados.text == undefined ? EstruturaDados.content : EstruturaDados.text,
													'Numero' : EstruturaDados.number,
													'LogJSON' : JSON.stringify(retorno),
													'Funcao' : Funcao
												},
												url: 'ajax/historicoSMS_ajax.asp',
												beforeSend: function(data){
													swal({
														title: 'Registrando no Historico de SMS',
														text: 'Aguarde',
														onOpen: () => {
														  swal.showLoading()
														},
														allowOutsideClick: false
													})
												},
												error: function(erro) {
													var mensagem = null; 
													if (erro == "timeout") {				        						
														mensagem = 'O Servidor demorou na resposta, tente novamente mais tarde!';
													}
													else {				        						
														mensagem = 'Algo estranho aconteceu, tente novamente mais tarde!';
													}
													swal.close();
													swal({
														title: "Ops!",
														text: mensagem,
														type: "error",
														allowOutsideClick: false
													})
												},		
												success: function(retorno) {

													// Fechar o SweetAlert

														swal.close();

													// Trata o retorno transformando em array

														retorno = jQuery.parseJSON(retorno);

													// Verifica se retornou algum erro

														if(retorno.hasOwnProperty("Status")){
															swal.close();
															swal({
																title: retorno.MensagemTitulo,
																text: retorno.MensagemTexto,
																type: retorno.MensagemTipo,
																allowOutsideClick: false
															})

														}

													// Promise do sweet alert

														swal({
															title: retorno.MensagemTitulo,
															text: retorno.MensagemTexto,
															type: retorno.MensagemTipo,
															allowOutsideClick: false
														})

												}
											})

									}
								})

						}
					
									
				}


		}
		
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------