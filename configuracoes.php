<?php include("includes/constantes.php") ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
  	<?php include("head.php") ?>
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <?php include("header_body.php") ?>

      <?php include("modal/modal_cad_vagasEstacionamento.php") ?>
      <?php include("modal/modal_alt_vagasEstacionamento.php") ?>

      <?php include("modal/modal_cad_tipoVeiculo.php") ?>
      <?php include("modal/modal_alt_tipoVeiculo.php") ?>

      <?php include("modal/modal_cad_plano.php") ?>
      <?php include("modal/modal_alt_plano.php") ?>

      <?php include("modal/modal_cad_tipoUsuario.php") ?>
      <?php include("modal/modal_alt_tipoUsuario.php") ?>

      <?php include("modal/modal_cad_usuario.php") ?>
      <?php include("modal/modal_alt_usuario.php") ?>

      <?php include("modal/modal_cad_configSMS.php") ?>
      <?php include("modal/modal_alt_configSMS.php") ?>

      <?php include("nav_body.php") ?>
      
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Configurações
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-cogs"></i> Configurações</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" id="btn-tab-geral" data-toggle="tab">Geral</a></li>
                      <li><a href="#tab_2" id="btn-tab-estacionamento" data-toggle="tab">Estacionamento</a></li>
                      <li><a href="#tab_3" id="btn-tab-tipoVeiculo" data-toggle="tab">Tipos de Veículos</a></li>
                      <li><a href="#tab_4" id="btn-tab-planos" data-toggle="tab">Planos</a></li>
                      <li><a href="#tab_5" id="btn-tab-tipoUsuario" data-toggle="tab">Tipos de Usuários</a></li>
                      <li><a href="#tab_6" id="btn-tab-usuarios" data-toggle="tab">Usuários</a></li>
                      <li><a href="#tab_7" id="btn-tab-configcupom" data-toggle="tab">Cupom</a></li>
                      <li><a href="#tab_8" id="btn-tab-configSMS" style="display: none;" data-toggle="tab">SMS</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                        <input type="hidden" name="CodConfiguracoes" value="">
                        <div class="row">
                          <!-- Inicio da Configuração de SMS -->
                          <div class="col-md-12">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Configuração de SMS
                                  </h1>
                                </div>
                                <div class="col-md-12" style="padding-bottom: 30px;">
                                  <p>
                                    Habilite a funcionalidade de SMS possibilitando a gestão dos gateways para envios de comprovantes e notificações.
                                  </p>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                                  <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <input type="checkbox" class="form-control" name="HabilitarSMS" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                  </div>
                                </div>
                              </div>
                            </blockquote>
                          </div>
                          <!-- Fim da Configuração de SMS -->
                          <!-- Inicio do Botão de Salvar -->
                          <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-success btn-lg" id="btn-tab-geral-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                          </div>
                          <!-- Fim do Botão de Salvar -->
                          <!-- Inicio da Linha -->
                          <div class="col-md-12">
                            <hr>
                          </div>
                          <!-- Fim da Linha -->
                          <!-- Inicio da Configuração de Fração -->
                          <div class="col-md-12">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Configuração de Fração
                                  </h1>
                                </div>
                                <div class="col-md-12" style="padding-bottom: 30px;">
                                  <p>
                                    Com esta ferramenta é possivel habilitar/desabilitar o calculo da fração de hora nas entradas avulsas.
                                  </p>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                                  <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <input type="checkbox" class="form-control" name="HabilitarFracao" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                  </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                                  <div class="form-group" id="div_DuracaoFracao">
                                    <label class="control-label">Duração</label>
                                    <input type="text" class="form-control text-center" placeholder="--:--" name="DuracaoFracao">
                                  </div>
                                </div>
                              </div>
                            </blockquote>
                          </div>
                          <!-- Fim da Configuração de Fração -->
                          <!-- Inicio do Botão de Salvar -->
                          <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-success btn-lg" id="btn-tab-geral-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                          </div>
                          <!-- Fim do Botão de Salvar -->
                          <!-- Inicio da Linha -->
                          <div class="col-md-12">
                            <hr>
                          </div>
                          <!-- Fim da Linha -->
                          <!-- Inicio da Configuração de Tolerância -->
                          <div class="col-md-12">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Configuração de Tolerância
                                  </h1>
                                </div>
                                <div class="col-md-12" style="padding-bottom: 30px;">
                                  <p>
                                    Com esta ferramenta é possivel habilitar/desabilitar o calculo da tolerância nas entradas avulsas.
                                  </p>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                                  <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <input type="checkbox" class="form-control" name="HabilitarTolerancia" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                  </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                                  <div class="form-group" id="div_DuracaoTolerancia">
                                    <label class="control-label">Duração</label>
                                    <input type="text" class="form-control text-center" placeholder="--:--" name="DuracaoTolerancia">
                                  </div>
                                </div>
                              </div>
                            </blockquote>
                          </div>
                          <!-- Fim da Configuração de Tolerância -->
                          <!-- Inicio do Botão de Salvar -->
                          <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-success btn-lg" id="btn-tab-geral-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                          </div>
                          <!-- Fim do Botão de Salvar -->
                          <!-- Inicio da Linha -->
                          <div class="col-md-12">
                            <hr>
                          </div>
                          <!-- Fim da Linha -->
                          <!-- Inicio das Informações da Empresa -->
                          <div class="col-md-12">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Dados da Empresa
                                  </h1>
                                </div>
                                <div class="col-md-12">
                                  <p>
                                    Preencha as informações da sua empresa
                                  </p>
                                </div>
                            </blockquote>
                          </div>
                          <div class="col-md-12">
                            <div class="row">
                              <div class="col-xs-12 col-sm-7 col-md-5">
                                <div class="form-group" id="div_Razao">
                                  <label class="control-label" for="Razao">Razão</label>
                                  <input type="text" class="form-control" name="Razao">
                                </div>
                              </div>
                              <div class="col-xs-6 col-sm-5 col-md-3">
                                <div class="form-group" id="div_CNPJ">
                                  <label class="control-label" for="CNPJ">CNPJ</label>
                                  <input type="text" class="form-control" name="CNPJ">
                                </div>
                              </div>
                              <div class="col-xs-6 col-sm-4 col-md-2">
                                <div class="form-group" id="div_TelefoneResidencial">
                                  <label class="control-label" for="TelefoneResidencial">Telefone Residencial</label>
                                  <input type="text" class="form-control" name="TelefoneResidencial">
                                </div>
                              </div>
                              <div class="col-xs-6 col-sm-4 col-md-2">
                                <div class="form-group" id="div_TelefoneComercial">
                                  <label class="control-label" for="TelefoneComercial">Telefone Comercial</label>
                                  <input type="text" class="form-control" name="TelefoneComercial">
                                </div>
                              </div>
                              <div class="col-xs-6 col-sm-4 col-md-2">
                                <div class="form-group" id="div_TelefoneCorporativo">
                                  <label class="control-label" for="TelefoneCorporativo">Telefone Corporativo</label>
                                  <input type="text" class="form-control" name="TelefoneCorporativo">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-3">
                                <div class="form-group" id="div_Email">
                                  <label class="control-label" for="Email">E-Mail</label>
                                  <input type="email" class="form-control" name="Email">
                                </div>
                              </div>
                              <div class="col-xs-4 col-sm-4 col-md-2">
                                <div class="form-group" id="div_CEP">
                                  <label class="control-label" for="CEP">CEP</label>
                                  <input type="text" class="form-control" name="CEP">
                                </div>
                              </div>
                              <div class="col-xs-8 col-sm-10 col-md-4">
                                <div class="form-group" id="div_Cidade">
                                  <label class="control-label" for="Cidade">Cidade</label>
                                  <input type="text" class="form-control" name="Cidade">
                                </div>
                              </div>
                              <div class="col-xs-3 col-sm-2 col-md-1">
                                <div class="form-group" id="div_UF">
                                  <label class="control-label" for="UF">UF</label>
                                  <input type="text" class="form-control" name="UF">
                                </div>
                              </div>
                              <div class="col-xs-9 col-sm-5 col-md-4">
                                <div class="form-group" id="div_Bairro">
                                  <label class="control-label" for="Bairro">Bairro</label>
                                  <input type="text" class="form-control" name="Bairro">
                                </div>
                              </div>
                              <div class="col-xs-9 col-sm-5 col-md-6">
                                <div class="form-group" id="div_Endereco">
                                  <label class="control-label" for="Endereco">Endereço</label>
                                  <input type="text" class="form-control" name="Endereco">
                                </div>
                              </div>
                              <div class="col-xs-3 col-sm-2 col-md-1">
                                <div class="form-group" id="div_Numero">
                                  <label class="control-label" for="Numero">Nº</label>
                                  <input type="text" class="form-control" name="Numero">
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- Fim das Informações da Empresa -->
                          <!-- Inicio do Botão de Salvar -->
                          <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-success btn-lg" id="btn-tab-geral-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                          </div>
                          <!-- Fim do Botão de Salvar -->
                          <!-- Inicio da Linha -->
                          <div class="col-md-12">
                            <hr>
                          </div>
                          <!-- Fim da Linha -->
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <blockquote style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                Vagas Cadastradas
                              </h1>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 15px;">
                              <p>
                                Cadastre as vagas do seu estacionamento de forma rapida e simples
                              </p>
                            </div>
                            <div class="col-md-12">
                                <button type="button" id="btn-cad-vagasEstacionamento" class="btn btn-info"><i class="fa fa-plus"></i> Cadastrar Vagas</button>
                            </div>
                          </div>
                        </blockquote>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabEstacionamento" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Icone</th>
                                  <th class="text-center">Tipo de Veiculo</th>
                                  <th class="text-center">Total de Vagas</th>
                                  <th class="text-center">Vagas Disponiveis</th>
                                  <th class="text-center">Vagas Indisponiveis</th>
                                  <th class="text-center">Valor da Hora</th>
                                  <th class="text-center">Valor da Fração</th>
                                  <th class="text-center">Vaga Preferencial</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosEstacionamento">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Icone</th>
                                  <th class="text-center">Tipo de Veiculo</th>
                                  <th class="text-center">Total de Vagas</th>
                                  <th class="text-center">Vagas Disponiveis</th>
                                  <th class="text-center">Vagas Indisponiveis</th>
                                  <th class="text-center">Valor da Hora</th>
                                  <th class="text-center">Valor da Fração</th>
                                  <th class="text-center">Vaga Preferencial</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-estacionamento-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-estacionamento">
                              
                            </ul>
                          </div>
                        </div>
                       <!--  <div class="row">
                          <div class="col-md-12">
                            <hr>
                          </div>
                        </div>
                        <blockquote style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                Vagas Relacionadas
                              </h1>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 15px;">
                              <p>
                                As vagas relacionadas servem para unir as vagas com outras
                              </p>
                            </div>
                          </div>
                        </blockquote> -->
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_3">
                        <blockquote style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                Tipo de Veículo
                              </h1>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 15px;">
                              <p>
                                Cadastre os tipos de veiculos do seu estacionamento de forma rapida e simples
                              </p>
                            </div>
                            <div class="col-md-12">
                                <button type="button" id="btn-cad-tipoVeiculo" class="btn btn-info"><i class="fa fa-plus"></i> Cadastrar Tipo de Veiculo</button>
                            </div>
                          </div>
                        </blockquote>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabTipoVeiculo" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosTipoVeiculo">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-tipoVeiculo-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-tipoVeiculo">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_4">
                        <blockquote style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                Planos
                              </h1>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 15px;">
                              <p>
                                Cadastre os planos para mensalistas de forma rapida e simples
                              </p>
                            </div>
                            <div class="col-md-12">
                                <button type="button" id="btn-cad-plano" class="btn btn-info"><i class="fa fa-plus"></i> Cadastrar Plano</button>
                            </div>
                          </div>
                        </blockquote>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabPlanos" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Valor</th>
                                  <th class="text-center">Vagas</th>
                                  <th class="text-center">Horas</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosPlanos">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Valor</th>
                                  <th class="text-center">Vagas</th>
                                  <th class="text-center">Horas</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-planos-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-planos">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_5">
                        <blockquote style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                Tipos de Usuários
                              </h1>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 15px;">
                              <p>
                                Cadastre os tipos de usuário para melhor gerenciar os níveis de acesso e permissionamento de forma rapida e simples
                              </p>
                            </div>
                            <div class="col-md-12">
                                <button type="button" id="btn-cad-tipoUsuario" class="btn btn-info"><i class="fa fa-plus"></i> Cadastrar Tipo de Usuário</button>
                            </div>
                          </div>
                        </blockquote>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabTiposUsuarios" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosTiposUsuarios">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-tipoUsuario-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-tipoUsuario">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_6">
                        <blockquote style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                Usuários
                              </h1>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 15px;">
                              <p>
                                Cadastre os usuários para que tenham acesso ao sistema de forma escalavel ao tipo de usuário de forma rapida e simples
                              </p>
                            </div>
                            <div class="col-md-12">
                                <button type="button" id="btn-cad-usuario" class="btn btn-info"><i class="fa fa-plus"></i> Cadastrar Usuário</button>
                            </div>
                          </div>
                        </blockquote>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabUsuarios" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Nome</th>
                                  <th class="text-center">E-Mail</th>
                                  <th class="text-center">Tipo de Usuário</th>
                                  <th class="text-center">Status</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosUsuarios">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Nome</th>
                                  <th class="text-center">E-Mail</th>
                                  <th class="text-center">Tipo de Usuário</th>
                                  <th class="text-center">Status</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-usuarios-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-usuarios">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_7">
                        <input type="hidden" name="CodConfigCupomAvulsoEntrada" value="">
                        <input type="hidden" name="CodConfigCupomAvulsoSaida" value="">
                        <input type="hidden" name="CodConfigCupomMensalidade" value="">
                        <input type="hidden" name="CodConfigSMSCupom" value="">
                        <div class="row">
                          <!-- Inicio da Configuração de Impressão do Cupom Avulso de Entrada -->
                          <div class="col-md-12">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Configuração de Impressão do Cupom Avulso de Entrada
                                  </h1>
                                </div>
                                <div class="col-md-12">
                                  <p>
                                    Gerencie as informações que serão impressas no cupom avulso de entrada
                                  </p>
                                </div>
                              </div>
                            </blockquote>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="row">
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
                                    <label class="control-label col-md-12">(1) Topo</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoEntrada_HabilitarTopo" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
                                    <label class="control-label col-md-12">(2) Informações da Empresa</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoEntrada_HabilitarInfoEmpresa" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomAvulsoEntrada_HabilitarInfoVeiculo">
                                    <label class="control-label col-md-12">(3) Informações do Veículo</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoEntrada_HabilitarInfoVeiculo" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomAvulsoEntrada_HabilitarInfoServico">
                                    <label class="control-label col-md-12">(4) Informações do Serviço</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoEntrada_HabilitarInfoServico" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <img width="100%" src="images/cupomAvulsoEntrada.png">
                              </div>
                              <div class="col-md-12 text-center" style="padding-top: 20px">
                                <button type="button" class="btn btn-success btn-lg" id="btn-tab-configcupom-avulsoentrada-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                              </div>
                            </div>
                          </div>
                          <!-- Fim da Configuração de Impressão do Cupom Avulso de Entrada -->
                          <!-- Inicio da Linha -->
                          <div class="col-md-12">
                            <hr>
                          </div>
                          <!-- Fim da Linha -->
                          <!-- Inicio da Configuração de Impressão do Cupom Avulso de Saida -->
                          <div class="col-md-12">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Configuração de Impressão do Cupom Avulso de Saida
                                  </h1>
                                </div>
                                <div class="col-md-12">
                                  <p>
                                    Gerencie as informações que serão impressas no cupom avulso de saida
                                  </p>
                                </div>
                              </div>
                            </blockquote>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="row">
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
                                    <label class="control-label col-md-12">(1) Topo</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoSaida_HabilitarTopo" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
                                    <label class="control-label col-md-12">(2) Informações da Empresa</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoSaida_HabilitarInfoEmpresa" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomAvulsoSaida_HabilitarInfoVeiculo">
                                    <label class="control-label col-md-12">(3) Informações do Veículo</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoSaida_HabilitarInfoVeiculo" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomAvulsoSaida_HabilitarInfoServico">
                                    <label class="control-label col-md-12">(4) Informações do Serviço</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoSaida_HabilitarInfoServico" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomAvulsoSaida_HabilitarInfoCaixa">
                                    <label class="control-label col-md-12">(5) Informações do Caixa</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomAvulsoSaida_HabilitarInfoCaixa" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <img width="100%" src="images/cupomAvulsoSaida.png">
                              </div>
                              <div class="col-md-12 text-center" style="padding-top: 20px">
                                <button type="button" class="btn btn-success btn-lg" id="btn-tab-configcupom-avulsosaida-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                              </div>
                            </div>
                          </div>
                          <!-- Fim da Configuração de Impressão do Cupom Avulso de Saida -->
                          <!-- Inicio da Linha -->
                          <div class="col-md-12">
                            <hr>
                          </div>
                          <!-- Fim da Linha -->
                          <!-- Inicio da Configuração de Impressão do Cupom de Mensalidade -->
                          <div class="col-md-12">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Configuração de Impressão do Cupom de Mensalidade
                                  </h1>
                                </div>
                                <div class="col-md-12">
                                  <p>
                                    Gerencie as informações que serão impressas no cupom de mensalidade
                                  </p>
                                </div>
                              </div>
                            </blockquote>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="row">
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
                                    <label class="control-label col-md-12">(1) Topo</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomMensalidade_HabilitarTopo" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
                                    <label class="control-label col-md-12">(2) Informações da Empresa</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomMensalidade_HabilitarInfoEmpresa" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomMensalidade_HabilitarInfoMensalista">
                                    <label class="control-label col-md-12">(3) Informações do(a) Mensalista</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomMensalidade_HabilitarInfoMensalista" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomMensalidade_HabilitarInfoMensalidade">
                                    <label class="control-label col-md-12">(4) Informações da Mensalidade</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomMensalidade_HabilitarInfoMensalidade" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" id="div_CupomMensalidade_HabilitarInfoCaixa">
                                    <label class="control-label col-md-12">(5) Informações do Caixa</label>
                                    <div class="form-group">
                                      <input type="checkbox" class="form-control" name="CupomMensalidade_HabilitarInfoCaixa" data-toggle="toggle" data-onstyle="success" data-on="Habilitado" data-off="Desabilitado">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <img width="100%" src="images/cupomMensalidade.png">
                              </div>
                              <div class="col-md-12 text-center" style="padding-top: 20px">
                                <button type="button" class="btn btn-success btn-lg" id="btn-tab-configcupom-mensalidade-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                              </div>
                            </div>
                          </div>
                          <!-- Fim da Configuração de Impressão do Cupom de Mensalidade -->
                          <!-- Inicio da Linha -->
                          <div class="col-md-12" id="linha-configSMSCupom">
                            <hr>
                          </div>
                          <!-- Fim da Linha -->
                          <!-- Inicio da Configuração de Mensagens do Cupom para Envio via SMS -->
                          <div class="col-md-12" id="configSMSCupom">
                            <blockquote style="margin-top: 10px;">
                              <div class="row">
                                <div class="col-md-12">
                                  <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                    Configuração de Mensagems para Envio de Cupom via SMS
                                  </h1>
                                </div>
                                <div class="col-md-12">
                                  <p>
                                    Gerencie as mensagens que serão enviadas via sms referente a cada cupom do sistema.
                                  </p>
                                </div>
                              </div>
                            </blockquote>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="row">

                                  <div class="col-xs-12 col-md-6 text-center">
                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <span style="font-weight: bold;">Mensagem que será enviada quando o cliente realizar a entrada no estacionamento</span>
                                      </div>
                                      <div class="panel-body">
                                        <div class="form-group" style="margin: 0px;" id="div_ConfigSMSCupom_MensagemCupomAvulsoEntrada">
                                          <textarea class="form-control" name="ConfigSMSCupom_MensagemCupomAvulsoEntrada" data-maxlength="3000" cols="auto" rows="6" style="resize:none"></textarea>
                                        </div>
                                        <div class="row">
                                          <div class="col-xs-12 text-right">
                                            <span>Total de Caracteres (<label id="label-ConfigSMSCupom_MensagemCupomAvulsoEntrada-totalCaracteres">0</label>)</span>
                                          </div>
                                        </div>
                                        <div class="row" id="div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada" style="display: none;">
                                          <div class="col-xs-12 text-center">
                                            <span style="font-weight: bold;">Variáveis que estão sendo utilizadas na mensagem</span>
                                          </div>
                                          <div class="col-xs-12" id="variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoEntrada"></div>
                                        </div>
                                      </div>
                                      <div class="panel-footer">
                                        <button type="button" class="btn btn-primary" id="btn-MensagemCupomAvulsoEntrada-informacao"><i class="fa fa-info-circle"></i> Informações</button>
                                        <button type="button" class="btn btn-info" id="btn-MensagemCupomAvulsoEntrada-redefine"><i class="fa fa-recycle"></i> Redefinir</button>
                                        <button type="button" class="btn btn-danger" id="btn-MensagemCupomAvulsoEntrada-limpa"><i class="fa fa-trash-o"></i> Limpar</button>
                                        <button type="button" class="btn btn-success" id="btn-MensagemCupomAvulsoEntrada-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-xs-12 col-md-6 text-center">
                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <span style="font-weight: bold;">Mensagem que será enviada quando o cliente realizar a saída do estacionamento</span>
                                      </div>
                                      <div class="panel-body">
                                        <div class="form-group" style="margin: 0px;" id="div_ConfigSMSCupom_MensagemCupomAvulsoSaida">
                                          <textarea class="form-control" name="ConfigSMSCupom_MensagemCupomAvulsoSaida" data-maxlength="3000" cols="auto" rows="6" style="resize:none"></textarea>
                                        </div>
                                        <div class="row">
                                          <div class="col-xs-12 text-right">
                                            <span>Total de Caracteres (<label id="label-ConfigSMSCupom_MensagemCupomAvulsoSaida-totalCaracteres">0</label>)</span>
                                          </div>
                                        </div>
                                        <div class="row" id="div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida" style="display: none;">
                                          <div class="col-xs-12 text-center">
                                            <span style="font-weight: bold;">Variáveis que estão sendo utilizadas na mensagem</span>
                                          </div>
                                          <div class="col-xs-12" id="variaveisUtilizadas-ConfigSMSCupom_MensagemCupomAvulsoSaida"></div>
                                        </div>
                                      </div>
                                      <div class="panel-footer">
                                        <button type="button" class="btn btn-primary" id="btn-MensagemCupomAvulsoSaida-informacao"><i class="fa fa-info-circle"></i> Informações</button>
                                        <button type="button" class="btn btn-info" id="btn-MensagemCupomAvulsoSaida-redefine"><i class="fa fa-recycle"></i> Redefinir</button>
                                        <button type="button" class="btn btn-danger" id="btn-MensagemCupomAvulsoSaida-limpa"><i class="fa fa-trash-o"></i> Limpar</button>
                                        <button type="button" class="btn btn-success" id="btn-MensagemCupomAvulsoSaida-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-xs-12 col-md-6 text-center">
                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <span style="font-weight: bold;">Mensagem que será enviada quando o(a) mensalista realizar o pagamento da Mensalidade</span>
                                      </div>
                                      <div class="panel-body">
                                        <div class="form-group" style="margin: 0px;" id="div_ConfigSMSCupom_MensagemCupomMensalidade">
                                          <textarea class="form-control" name="ConfigSMSCupom_MensagemCupomMensalidade" data-maxlength="3000" cols="auto" rows="6" style="resize:none"></textarea>
                                        </div>
                                        <div class="row">
                                          <div class="col-xs-12 text-right">
                                            <span>Total de Caracteres (<label id="label-ConfigSMSCupom_MensagemCupomMensalidade-totalCaracteres">0</label>)</span>
                                          </div>
                                        </div>
                                        <div class="row" id="div_variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade" style="display: none;">
                                          <div class="col-xs-12 text-center">
                                            <span style="font-weight: bold;">Variáveis que estão sendo utilizadas na mensagem</span>
                                          </div>
                                          <div class="col-xs-12" id="variaveisUtilizadas-ConfigSMSCupom_MensagemCupomMensalidade"></div>
                                        </div>
                                      </div>
                                      <div class="panel-footer">
                                        <button type="button" class="btn btn-primary" id="btn-MensagemCupomMensalidade-informacao"><i class="fa fa-info-circle"></i> Informações</button>
                                        <button type="button" class="btn btn-info" id="btn-MensagemCupomMensalidade-redefine"><i class="fa fa-recycle"></i> Redefinir</button>
                                        <button type="button" class="btn btn-danger" id="btn-MensagemCupomMensalidade-limpa"><i class="fa fa-trash-o"></i> Limpar</button>
                                        <button type="button" class="btn btn-success" id="btn-MensagemCupomMensalidade-salvar"><i class="fa fa-floppy-o"></i> Salvar</button>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- Fim da Configuração de Mensagens do Cupom para Envio via SMS -->
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_8">
                        <blockquote style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <h1 style="margin-top: 0px;padding-bottom: 15px;">
                                Configurações de SMS
                              </h1>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 15px;">
                              <p>
                                Cadastre as Configurações de SMS que serão utilizadas pelo sistema para envio de Notificações e Cupons.
                              </p>
                            </div>
                            <div class="col-md-12">
                                <button type="button" id="btn-cad-configSMS" class="btn btn-info"><i class="fa fa-plus"></i> Cadastrar Configuração de SMS</button>
                            </div>
                          </div>
                        </blockquote>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabConfigSMS" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Link da API</th>
                                  <th class="text-center">Status</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosConfigSMS">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Descrição</th>
                                  <th class="text-center">Link da API</th>
                                  <th class="text-center">Status</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-configSMS-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-configSMS">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                  </div><!-- nav-tabs-custom -->
                </div>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("footer_body.php") ?>
    </div><!-- ./wrapper -->
    <?php include("footer.php") ?>
    <!--Vagaberta - PainelControle-->
    <script src="plugins/vagaberta/configuracoes.js"></script>
    <script src="plugins/vagaberta/configuracoesEstacionamento.js"></script>
    <script src="plugins/vagaberta/configuracoesTipoVeiculo.js"></script>
    <script src="plugins/vagaberta/configuracoesPlano.js"></script>
    <script src="plugins/vagaberta/configuracoesTipoUsuario.js"></script>
    <script src="plugins/vagaberta/configuracoesUsuario.js"></script>
    <script src="plugins/vagaberta/configuracoesCupom.js"></script>
    <script src="plugins/vagaberta/configuracoesSMS.js"></script>
  </body>
</html>