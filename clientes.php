<?php include("includes/constantes.php") ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php include("head.php") ?>
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <?php include("header_body.php") ?>

      <?php include("modal/modal_cad_cliente.php") ?>
      <?php include("modal/modal_alt_cliente.php") ?>

      <?php include("nav_body.php") ?>
      
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Clientes
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i> Clientes</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#tab_clientes" id="btn-tab-clientes" data-toggle="tab"><i class="fa fa-users"></i> Clientes <span class="label label-default" id="badge-TotalRegistrosClientes">0</span></a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_clientes">
                      <div class="row" style="padding-bottom: 10px;">
                        <div class="col-xs-12 col-sm-12 col-md-3 text-center">
                          <button type="button" id="btn-cad-clientes" class="btn btn-info"><i class="fa fa-plus"></i> Novo Cliente</button>
                        </div>
                        <div class="col-xs-12 visible-xs visible-sm">
                          <hr>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                          <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                              <a data-toggle="collapse" data-parent="#accordion" id="btn-collapse-filtros-clientes" href="#collapseFiltros-clientes">
                                <div class="box-header with-border">
                                  <h4 class="box-title">
                                      <i class="fa fa-filter"></i> Filtros
                                  </h4>
                                </div>
                              </a>
                              <div id="collapseFiltros-clientes" class="panel-collapse collapse">
                                <div class="box-body">
                                  <div class="row">
                                    <form id="form_filtros_clientes">
                                      <div class="col-xs-4 col-sm-3 col-md-2" id="filtros_clientes_divCodigo">
                                        <div class="form-group" id="filtros_clientes_div_Codigo">
                                          <label class="control-label" for="filtros_clientes_input_Codigo">Codigo</label>
                                          <input type="text" class="form-control" name="filtros_clientes_input_Codigo">
                                        </div>
                                      </div>
                                      <div class="col-xs-8 col-sm-6 col-md-4" id="filtros_clientes_divNome">
                                        <div class="form-group" id="filtros_clientes_div_Nome">
                                          <label class="control-label" for="filtros_clientes_input_Nome">Nome</label>
                                          <input type="text" class="form-control" name="filtros_clientes_input_Nome">
                                        </div>
                                      </div>
                                      <div class="col-xs-12 col-sm-3 col-md-3" id="filtros_clientes_divCPF">
                                        <div class="form-group" id="filtros_clientes_div_CPF">
                                          <label class="control-label" for="filtros_clientes_input_CPF">CPF</label>
                                          <input type="text" class="form-control" name="filtros_clientes_input_CPF">
                                        </div>
                                      </div>
                                      <div class="col-xs-6 col-sm-4 col-md-2 text-center" id="div-btn-filtrar-clientes">
                                        <button type="button" class="btn btn-primary filtrar" id="btn-filtrar-clientes"><i class="fa fa-filter"></i> Filtrar</button>
                                      </div>
                                      <div class="col-xs-6 col-sm-4 col-md-4 text-center" id="div-btn-remover-filtros-clientes" style="display: none;">
                                        <button type="button" class="btn btn-danger remover-filtro" id="btn-remover-filtros-clientes"><i class="fa fa-filter"></i> Remover Filtros</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row" style="margin: 0px;">
                        <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                          <table id="tabClientes" class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th class="text-center">Codigo</th>
                                <th class="text-center">Nome</th>
                                <th class="text-center">RG</th>
                                <th class="text-center">CPF</th>
                                <th class="text-center">Telefone</th>
                                <th class="text-center">E-Mail</th>
                                <th class="text-center">Opções</th>
                              </tr>
                            </thead>
                            <tbody id="registrosClientes">
                            </tbody>
                            <tfoot>
                              <tr>
                                <th class="text-center">Codigo</th>
                                <th class="text-center">Nome</th>
                                <th class="text-center">RG</th>
                                <th class="text-center">CPF</th>
                                <th class="text-center">Telefone</th>
                                <th class="text-center">E-Mail</th>
                                <th class="text-center">Opções</th>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="col-md-5" style="padding-top: 10px;">
                          <span id="pgn-clientes-texto"></span>
                        </div>
                        <div class="col-md-7" style="padding-top: 10px;">  
                          <ul class="pagination pagination-sm no-margin pull-right" id="pgn-clientes">
                            
                          </ul>
                        </div>
                      </div>
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("footer_body.php") ?>
    </div><!-- ./wrapper -->
    <?php include("footer.php") ?>
    <!--Fast Command - PainelControle-->
    <script src="plugins/fast-command/clientes.js"></script>
  </body>
</html>