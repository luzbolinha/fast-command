<?php include("modal/modal_perfil.php") ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="image text-center">
        <img src="images/default-user.png" id="menu-ImagemPerfil" class="img-circle" alt="User Image" />
      </div>
      <div class="status text-center">
        <span id="menu-Status" class="label label-success">Online</span>
      </div>
      <div class="info text-center">
        <p id="menu-Nome">Fast Command</p>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MENU DE NAVEGAÇÃO</li>
      <li id="menu-estacionamento">
        <a href="comandas.php">
          <i class="fa fa-dashboard"></i> <span>Comandas</span>
        </a>
      </li>
      <li id="menu-cardapio">
        <a href="cardapio.php">
          <i class="fa fa-list-alt"></i> <span>Cardápio</span>
        </a>
      </li>
      <li id="menu-clientes">
        <a href="clientes.php">
          <i class="fa fa-users"></i> <span>Clientes</span>
        </a>
      </li>
      <!-- <li id="menu-configuracoes">
        <a href="configuracoes.php">
          <i class="fa fa-cogs"></i> <span>Configurações</span>
        </a>
      </li> -->
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>