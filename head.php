<?php include("includes/constantes.php") ?>
<meta charset="UTF-8">
<title><?php echo $nome_sistema ?></title>

<!--Favicon-->
<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
<link rel="manifest" href="images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- Bootstrap 3.3.2 -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Toggle -->
<link href="plugins/bootstrap-toggle/css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- iCheck for checkboxes and radio inputs -->
<link href="plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
<!-- font-awesome -->
<link rel="stylesheet" href="plugins/font-awesome-4.7.0/css/font-awesome.css">
<!-- Ionicons 2.0.0 -->
<link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
<!-- Theme style -->
<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link href="dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<!-- iziModal -->
<link href="plugins/iziModal/css/iziModal.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap time Picker -->
<link href="plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/>
<!-- TOASTR -->
<link rel="stylesheet" href="plugins/toastr/css/toastr.css">
<link rel="stylesheet" href="plugins/toastr/css/toastr.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/SweetAlert2/sweetalert2.min.css">
<!-- Fast Command Style -->
<link rel="stylesheet" href="plugins/fast-command/style.css">

<!--Arquivos JS Principais-->
<!-- jQuery 2.1.3 -->
<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap Toggle -->
<script	src="plugins/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
<!-- iCheck -->
<!-- <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script> -->
<!-- iCheck 2.x -->
<script src="plugins/iCheck-2.x/icheck.min.js" type="text/javascript"></script>
<!-- jQuery UI 1.11.2 -->
<script src="plugins/jQueryUI/jquery-ui-1.11.2.min.js" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>  
<!-- Morris.js charts -->
<script src="plugins/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js" type="text/javascript"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='plugins/fastclick/fastclick.min.js'></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js" type="text/javascript"></script>
<!--iziModal-->
<script src="plugins/iziModal/js/iziModal.js"></script>
<!-- Mask jQuery Scripts -->
<script src="plugins/jQuery_Mask/jquery.mask.js"></script>
<!-- Moment -->
<script src="plugins/Moment/moment.min.js"></script>
<script src="plugins/Moment/moment-timezone.min.js"></script>
<!-- Toastr Scripts -->
<script src="plugins/toastr/js/toastr.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/SweetAlert2/sweetalert2.min.js"></script>
<!--PrintThis-->
<script src="plugins/printThis/printThis.js"></script>
<!--Google Chart-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!--Arquivos JS - Fast Command-->
<!--Fast Command - Constantes-->
<script src="plugins/fast-command/constantes.js"></script>
<!--Fast Command - Funcoes-->
<script src="plugins/fast-command/funcoes.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->