<?php include("includes/constantes.php") ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
  	<?php include("head.php") ?>
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <?php include("header_body.php") ?>

      <?php include("modal/modal_cad_comandas.php") ?>
      <?php include("modal/modal_alt_comandas.php") ?>
      <?php include("modal/modal_pag_comandas.php") ?>

      <?php include("nav_body.php") ?>
      
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Comandas
            <small>Resumo</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Comandas</a></li>
            <li class="active">Resumo</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active">
                        <a href="#tab_1" id="btn-tab-comandas" data-toggle="tab"><i class="fa fa-car"></i> Comandas <span class="label label-default" id="badge-TotalRegistrosComandas">0</span></a>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                        <div class="row" style="padding-bottom: 10px;">
                          <div class="col-xs-12 col-sm-12 col-md-3 text-center">
                            <button type="button" id="btn-cad-comandas" class="btn btn-info"><i class="fa fa-plus"></i> Comanda</button>
                          </div>
                          <div class="col-xs-12 visible-xs visible-sm">
                            <hr>
                          </div>
                          <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="box-group" id="accordion">
                              <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                              <div class="panel box box-primary">
                                <a data-toggle="collapse" data-parent="#accordion" id="btn-collapse-filtros-comandas" href="#collapseFiltrosComandas">
                                  <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <i class="fa fa-filter"></i> Filtros
                                    </h4>
                                  </div>
                                </a>
                                <div id="collapseFiltrosComandas" class="panel-collapse collapse">
                                  <div class="box-body">
                                    <div class="row">
                                      <form id="form_filtros_comandas">
                                        <div class="col-xs-4 col-sm-3 col-md-3" id="filtros_comandas_divCodigo">
                                          <div class="form-group" id="filtros_comandas_div_Codigo">
                                            <label class="control-label" for="filtros_comandas_input_Codigo">Codigo</label>
                                            <input type="text" class="form-control" name="filtros_comandas_input_Codigo">
                                          </div>
                                        </div>
                                        <div class="col-xs-8 col-sm-4 col-md-9" id="filtros_comandas_divClientes">
                                          <div class="form-group" id="filtros_comandas_div_Clientes">
                                            <label class="control-label" for="filtros_comandas_input_Clientes">Clientes</label>
                                            <select class="form-control" data-live-search="true" name="filtros_comandas_input_Clientes">
                                            </select>
                                          </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-6 text-center" id="div-btn-filtrar-comandas">
                                          <button type="button" class="btn btn-primary" id="btn-filtrar-comandas"><i class="fa fa-filter"></i> Filtrar</button>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-6 text-center" id="div-btn-remover-filtros-comandas" style="display: none;">
                                          <button type="button" class="btn btn-danger" id="btn-remover-filtros-comandas"><i class="fa fa-filter"></i> Remover Filtros</button>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div><!-- /.box-body -->
                        </div>
                        <div class="row" style="margin: 0px;">
                          <div class="col-md-12 table-responsive" style="padding: 0px;margin: 0px;">
                            <table id="tabComandas" class="table table-hover table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Cliente</th>
                                  <th class="text-center">Total</th>
                                  <th class="text-center">Pago</th>
                                  <th class="text-center">Troco</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </thead>
                              <tbody id="registrosComandas">
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th class="text-center">Codigo</th>
                                  <th class="text-center">Cliente</th>
                                  <th class="text-center">Total</th>
                                  <th class="text-center">Pago</th>
                                  <th class="text-center">Troco</th>
                                  <th class="text-center">Opções</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="col-md-5" style="padding-top: 10px;">
                            <span id="pgn-comandas-texto"></span>
                          </div>
                          <div class="col-md-7" style="padding-top: 10px;">  
                            <ul class="pagination pagination-sm no-margin pull-right" id="pgn-comandas">
                              
                            </ul>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                  </div><!-- nav-tabs-custom -->
                </div>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include("footer_body.php") ?>
    </div><!-- ./wrapper -->
    <?php include("footer.php") ?>
    <!--Fast Command - PainelControle-->
    <script src="plugins/fast-command/comandas.js"></script>
  </body>
</html>