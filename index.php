<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">

    <?php include("head.php") ?>
    
    <!--Fast Command - PainelControle-->
    <script src="plugins/fast-command/checaSessaoLogin.js"></script>
  <style>
    /*.login-page{
      background-image: url('images/background.jpg');
      background-repeat: no-repeat;
      background-size: 100% 100%;
      background-color: #FFFFFF;
    }*/
  </style>
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <img src="<?php echo $logo_sistema ?>" width="100px">
      </div><!-- /.login-logo -->
      <div class="login-box-body" style="box-shadow: 1px 1px 5px #666;">
        <p class="login-box-msg"><img src="<?php echo $logo_sistema_menu_black ?>" width="140px"></p>
        <form name="formLogin">
          <div class="form-group has-feedback" id="div_input_login_Email">
            <input type="text" class="form-control" name="input_login_Email" placeholder="Email"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback" id="div_input_login_Senha">
            <input type="password" class="form-control" name="input_login_Senha" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
              <button type="button" id="btn-logar" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Entrar</button>
            </div><!-- /.col -->
            <div class="col-xs-3"></div>
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <?php include("footer.php") ?>
    <!--Fast Command - PainelControle-->
    <script src="plugins/fast-command/login.js"></script>
  </body>
</html>