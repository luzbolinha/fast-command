<!--Fast Command - ChecaSessao-->
<script src="plugins/fast-command/checaSessao.js"></script>

<header class="main-header">
  <!-- Logo -->
  <a href="estacionamento.asp" class="logo text-center"><img src="<?php echo $logo_sistema_menu ?>" width="150px" class="text-center"></a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle visible-xs-block" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img id="nav-ImagemPerfil" src="images/default-user.png" class="user-image" alt="User Image"/>
            <span id="nav-Nome" class="hidden-xs">Fast Command</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img id="nav-ul-ImagemPerfil" src="images/default-user.png" class="img-circle" alt="User Image" />
              <p>
                <span id="nav-ul-Nome">Fast Command</span>
                <small id="nav-ul-Email">contato@fastcommand.com.br</small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <button type="button" id="btn-perfil" class="btn btn-primary btn-flat"><i class="fa fa-user-circle"></i> Perfil</button>
              </div>
              <div class="pull-right">
                <button type="button" id="btn-loggout" class="btn btn-danger btn-flat"><i class="fa fa-sign-out"></i> Sair</button>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>