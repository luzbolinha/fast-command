<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

	/**
	 * Consulta todos(as) os(as) Clientes
	 */
	public function index()
	{

		// Carrega a Model

			$this->load->model('Clientes_model', '', TRUE);

		// Verifica filtros

			if($this->input->post('filtros') != null){
				$this->Clientes_model->filtros = $this->input->post('filtros');
			}

		// Verifica se ira ocorrer paginação

			if($this->input->post('registros_paginas') != null){
				$this->Clientes_model->registros_paginas = $this->input->post('registros_paginas');
			}

			if($this->input->post('pagina_registro') != null){
				$this->Clientes_model->pagina_registro = $this->input->post('pagina_registro');
			}

		// Retorna os dados em JSON

			echo json_encode($this->Clientes_model->consulta_registros());

	}

	/**
	 * Consulta o(a) perfil do(a) Cliente
	 */
	public function perfil()
	{

		// Carrega a Model

			$this->load->model('Clientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Clientes_model->codigo = $this->input->post('codigo');

		// Retorna os dados em JSON

			echo json_encode($this->Clientes_model->consultar_registro());
	}
	
	/**
	 * Insere o(a) Cliente
	 */
	public function inserir()
	{
		// Carrega a Model

			$this->load->model('Clientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Clientes_model->nome = $this->input->post('nome');
			$this->Clientes_model->cpf = $this->input->post('cpf');
			$this->Clientes_model->rg = $this->input->post('rg');
			$this->Clientes_model->telefone = $this->input->post('telefone');
			$this->Clientes_model->email = $this->input->post('email');
			$this->Clientes_model->data_nascimento = $this->input->post('data_nascimento');
			$this->Clientes_model->cep = $this->input->post('cep');
			$this->Clientes_model->cidade = $this->input->post('cidade');
			$this->Clientes_model->bairro = $this->input->post('bairro');
			$this->Clientes_model->numero = $this->input->post('numero');
			$this->Clientes_model->endereco = $this->input->post('endereco');
			$this->Clientes_model->uf = $this->input->post('uf');

		// realiza a inserção dos dados e retorna o JSON

			echo json_encode($this->Clientes_model->inserir_registro());

	}
	
	/**
	 * Atualiza os dados do(a) Cliente
	 */
	public function atualizar()
	{

		// Carrega a Model

			$this->load->model('Clientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Clientes_model->codigo = $this->input->post('codigo');
			$this->Clientes_model->nome = $this->input->post('nome');
			$this->Clientes_model->nome_anterior = $this->input->post('nome_anterior');
			$this->Clientes_model->cpf = $this->input->post('cpf');
			$this->Clientes_model->cpf_anterior = $this->input->post('cpf_anterior');
			$this->Clientes_model->rg = $this->input->post('rg');
			$this->Clientes_model->telefone = $this->input->post('telefone');
			$this->Clientes_model->email = $this->input->post('email');
			$this->Clientes_model->data_nascimento = $this->input->post('data_nascimento');
			$this->Clientes_model->cep = $this->input->post('cep');
			$this->Clientes_model->cidade = $this->input->post('cidade');
			$this->Clientes_model->bairro = $this->input->post('bairro');
			$this->Clientes_model->numero = $this->input->post('numero');
			$this->Clientes_model->endereco = $this->input->post('endereco');

		// realiza a atualização dos dados e retorna o JSON

			echo json_encode($this->Clientes_model->atualizar_registro());
	}
	
	/**
	 * Exclui o(a) Cliente
	 */
	public function excluir()
	{

		// Carrega a Model

			$this->load->model('Clientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Clientes_model->codigo = $this->input->post('codigo');

		// realiza a exclusão dos dados e retorna o JSON

			echo json_encode($this->Clientes_model->excluir_registro());

	}
}
