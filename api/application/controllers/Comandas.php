<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comandas extends CI_Controller {

	/**
	 * Consulta todos os Comandas
	 */
	public function index()
	{

		// Carrega a Model

			$this->load->model('Comandas_model', '', TRUE);

		// Verifica filtros

			if($this->input->post('filtros') != null){
				$this->Comandas_model->filtros = $this->input->post('filtros');
			}

		// Verifica se ira ocorrer paginação

			if($this->input->post('registros_paginas') != null){
				$this->Comandas_model->registros_paginas = $this->input->post('registros_paginas');
			}

			if($this->input->post('pagina_registro') != null){
				$this->Comandas_model->pagina_registro = $this->input->post('pagina_registro');
			}

		// Retorna os dados em JSON

			echo json_encode($this->Comandas_model->consulta_registros());

	}

	/**
	 * Consulta o detalhes do Lanche
	 */
	public function detalhes()
	{

		// Carrega a Model

			$this->load->model('Comandas_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Comandas_model->codigo = $this->input->post('codigo');

		// Retorna os dados em JSON

			echo json_encode($this->Comandas_model->consultar_registro());
	}
	
	/**
	 * Insere o Lanche
	 */
	public function inserir()
	{
		// Carrega a Model

			$this->load->model('Comandas_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Comandas_model->cod_clientes = $this->input->post('cod_clientes');

		// realiza a inserção dos dados e retorna o JSON

			echo json_encode($this->Comandas_model->inserir_registro());

	}
	
	/**
	 * Atualiza os dados do Lanche
	 */
	public function atualizar()
	{

		// Carrega a Model

			$this->load->model('Comandas_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Comandas_model->cod_clientes = $this->input->post('cod_clientes');
			$this->Comandas_model->cod_status = $this->input->post('cod_status');

		// realiza a atualização dos dados e retorna o JSON

			echo json_encode($this->Comandas_model->atualizar_registro());
	}
	
	/**
	 * Exclui o Lanche
	 */
	public function excluir()
	{

		// Carrega a Model

			$this->load->model('Comandas_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Comandas_model->codigo = $this->input->post('codigo');

		// realiza a exclusão dos dados e retorna o JSON

			echo json_encode($this->Comandas_model->excluir_registro());

	}
}

