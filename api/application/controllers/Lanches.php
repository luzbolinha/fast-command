<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lanches extends CI_Controller {

	/**
	 * Consulta todos os Lanches
	 */
	public function index()
	{

		// Carrega a Model

			$this->load->model('Lanches_model', '', TRUE);

		// Verifica filtros

			if($this->input->post('filtros') != null){
				$this->Lanches_model->filtros = $this->input->post('filtros');
			}

		// Verifica se ira ocorrer paginação

			if($this->input->post('registros_paginas') != null){
				$this->Lanches_model->registros_paginas = $this->input->post('registros_paginas');
			}

			if($this->input->post('pagina_registro') != null){
				$this->Lanches_model->pagina_registro = $this->input->post('pagina_registro');
			}

		// Retorna os dados em JSON

			echo json_encode($this->Lanches_model->consulta_registros());

	}

	/**
	 * Consulta o detalhes do Lanche
	 */
	public function detalhes()
	{

		// Carrega a Model

			$this->load->model('Lanches_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Lanches_model->codigo = $this->input->post('codigo');

		// Retorna os dados em JSON

			echo json_encode($this->Lanches_model->consultar_registro());
	}
	
	/**
	 * Insere o Lanche
	 */
	public function inserir()
	{
		// Carrega a Model

			$this->load->model('Lanches_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Lanches_model->nome = $this->input->post('nome');
			$this->Lanches_model->descricao = $this->input->post('descricao');
			$this->Lanches_model->valor_venda = $this->input->post('valor_venda');
			$this->Lanches_model->valor_producao = $this->input->post('valor_producao');

		// realiza a inserção dos dados e retorna o JSON

			echo json_encode($this->Lanches_model->inserir_registro());

	}
	
	/**
	 * Atualiza os dados do Lanche
	 */
	public function atualizar()
	{

		// Carrega a Model

			$this->load->model('Lanches_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Lanches_model->codigo = $this->input->post('codigo');
			$this->Lanches_model->nome = $this->input->post('nome');
			$this->Lanches_model->nome_anterior = $this->input->post('nome_anterior');
			$this->Lanches_model->descricao = $this->input->post('descricao');
			$this->Lanches_model->valor_venda = $this->input->post('valor_venda');
			$this->Lanches_model->valor_producao = $this->input->post('valor_producao');
			$this->Lanches_model->cod_status = $this->input->post('cod_status');

		// realiza a atualização dos dados e retorna o JSON

			echo json_encode($this->Lanches_model->atualizar_registro());
	}
	
	/**
	 * Exclui o Lanche
	 */
	public function excluir()
	{

		// Carrega a Model

			$this->load->model('Lanches_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Lanches_model->codigo = $this->input->post('codigo');

		// realiza a exclusão dos dados e retorna o JSON

			echo json_encode($this->Lanches_model->excluir_registro());

	}
}

