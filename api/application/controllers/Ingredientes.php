<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingredientes extends CI_Controller {

	/**
	 * Consulta todos os Ingredientes
	 */
	public function index()
	{

		// Carrega a Model

			$this->load->model('Ingredientes_model', '', TRUE);

		// Verifica filtros

			if($this->input->post('filtros') != null){
				$this->Ingredientes_model->filtros = $this->input->post('filtros');
			}

		// Verifica se ira ocorrer paginação

			if($this->input->post('registros_paginas') != null){
				$this->Ingredientes_model->registros_paginas = $this->input->post('registros_paginas');
			}

			if($this->input->post('pagina_registro') != null){
				$this->Ingredientes_model->pagina_registro = $this->input->post('pagina_registro');
			}

		// Retorna os dados em JSON

			echo json_encode($this->Ingredientes_model->consulta_registros());

	}

	/**
	 * Consulta o detalhes do Lanche
	 */
	public function detalhes()
	{

		// Carrega a Model

			$this->load->model('Ingredientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Ingredientes_model->codigo = $this->input->post('codigo');

		// Retorna os dados em JSON

			echo json_encode($this->Ingredientes_model->consultar_registro());
	}
	
	/**
	 * Insere o Lanche
	 */
	public function inserir()
	{
		// Carrega a Model

			$this->load->model('Ingredientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Ingredientes_model->nome = $this->input->post('nome');
			$this->Ingredientes_model->descricao = $this->input->post('descricao');
			$this->Ingredientes_model->valor_venda = $this->input->post('valor_venda');
			$this->Ingredientes_model->valor_producao = $this->input->post('valor_producao');

		// realiza a inserção dos dados e retorna o JSON

			echo json_encode($this->Ingredientes_model->inserir_registro());

	}
	
	/**
	 * Atualiza os dados do Lanche
	 */
	public function atualizar()
	{

		// Carrega a Model

			$this->load->model('Ingredientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Ingredientes_model->codigo = $this->input->post('codigo');
			$this->Ingredientes_model->nome = $this->input->post('nome');
			$this->Ingredientes_model->nome_anterior = $this->input->post('nome_anterior');
			$this->Ingredientes_model->descricao = $this->input->post('descricao');
			$this->Ingredientes_model->valor_venda = $this->input->post('valor_venda');
			$this->Ingredientes_model->valor_producao = $this->input->post('valor_producao');
			$this->Ingredientes_model->cod_status = $this->input->post('cod_status');

		// realiza a atualização dos dados e retorna o JSON

			echo json_encode($this->Ingredientes_model->atualizar_registro());
	}
	
	/**
	 * Exclui o Lanche
	 */
	public function excluir()
	{

		// Carrega a Model

			$this->load->model('Ingredientes_model', '', TRUE);

		// Seta os valores recebidos pela requisição

			$this->Ingredientes_model->codigo = $this->input->post('codigo');

		// realiza a exclusão dos dados e retorna o JSON

			echo json_encode($this->Ingredientes_model->excluir_registro());

	}
}

