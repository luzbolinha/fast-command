<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_model extends CI_Model {

	public $codigo;
	public $nome;
	public $nome_anterior;
	public $cpf;
	public $cpf_anterior;
	public $rg;
	public $telefone;
	public $email;
	public $data_nascimento;
	public $cep;
	public $cidade;
	public $bairro;
	public $numero;
	public $endereco;
	public $uf;
	public $data_cadastro;
	public $horario_cadastro;
	public $data_atualizacao;
	public $horario_atualizacao;
	public $cod_situacao;
	public $registros_paginas;
	public $pagina_registro;
	public $filtros;

	/**
	 * Função para inicialização da model
	 */
	
	public function __construct() {
        parent::__construct();
    }

	/**
	 * Função para pegar o ultimo registro da tabela
	 */

	public function ultimo_registro(){

		// Define a Query

			$query = $this->db->where('cod_situacao', 1)
				->order_by('codigo', 'desc')
				->get('clientes', 1);

		// Retorna os resultados

			return $query->result();

	}

	/**
	 * Função para pegar o ultimo registro da tabela
	 */

	public function consulta_registros(){

		// Verifica se a Consulta tera paginação ou não

			if($this->pagina_registro != null && $this->registros_paginas != null){

				// Define a Query

					$query = $this->db->where('cod_situacao', 1);

				// Verifica se a consulta tera filtros

					if($this->filtros != null){
						$query->where($this->filtros);
					}

				// Realiza a Consulta

					$query->order_by('codigo', 'asc')
						->limit($this->registros_paginas)
						->offset($this->pagina_registro);

			} else {

				// Define a Query

					$query = $this->db->where('cod_situacao', 1);

				// Verifica se a consulta tera filtros

					if($this->filtros != null){
						$query->where($this->filtros);
					}

				// Realiza a Consulta

					$query->order_by('codigo', 'asc'); 

			}

		// Retorna os resultados
				
			return [
				"clientes" => $query->get('clientes')->result(),
				"total_clientes" => $query->count_all_results('clientes') 
			];
	}

	/**
	 * Função para Inserir registro na Tabela
	 */
	
	public function inserir_registro(){
		
		// Seta o timezone

			date_default_timezone_set("America/Sao_Paulo");

		// Declara as Variaveis

			$this->data_cadastro = date("Y-m-d");
			$this->horario_cadastro = date("H:i:s", time());
			$this->cod_situacao = 1;
			$validacao = [];
			$data_nascimento_array = explode("-", $this->data_nascimento);

		// Realiza as validações

			$validacao[0] = strlen($this->nome) > 0 & $this->nome != null & $this->nome != "" & $this->nome != " ";
			$validacao[1] = strlen($this->cpf) > 0 & $this->cpf != null & $this->cpf != "" & $this->cpf != " ";
			$validacao[2] = strlen($this->telefone) > 0 & $this->telefone != null & $this->telefone != "" & $this->telefone != " ";
			$validacao[3] = strlen($this->email) > 0 & $this->email != null & $this->email != "" & $this->email != " ";
			$validacao[4] = strlen($this->cep) > 0 & $this->cep != null & $this->cep != "" & $this->cep != " ";
			$validacao[5] = strlen($this->cidade) > 0 & $this->cidade != null & $this->cidade != "" & $this->cidade != " ";
			$validacao[6] = strlen($this->bairro) > 0 & $this->bairro != null & $this->bairro != "" & $this->bairro != " ";
			$validacao[7] = strlen($this->numero) > 0 & $this->numero != null & $this->numero != "" & $this->numero != " ";
			$validacao[8] = strlen($this->endereco) > 0 & $this->endereco != null & $this->endereco != "" & $this->endereco != " ";
			$validacao[9] = strlen($this->uf) > 0 & $this->uf != null & $this->uf != "" & $this->uf != " ";
			$validacao[10] = checkdate(date("m", $data_nascimento_array[1]), date("d",$data_nascimento_array[2]), date("Y", $data_nascimento_array[0]));

		// Checa se as validações estão corretas para inserir o registro e caso não retorna o erro em tela

			$checa_validacao = $validacao[0] & $validacao[1] & $validacao[2] & $validacao[3] & $validacao[4] & $validacao[5] & $validacao[6] & $validacao[7] & $validacao[8] & $validacao[9] & $validacao[10]; 

			if($checa_validacao){

				// Verifica se já existe um registro com o Nome ou CPF informado

					$total_registros = $this->db->where("nome", $this->nome)
												->or_where("cpf", $this->cpf)
												->count_all_results('clientes');

					if($total_registros > 0){

						// Retorna a mensagem de erro na tela

							show_error("O CPF ou Nome informado já pertence á outro(a) cliente, verifique e tente novamente.", 500, "Um erro foi encontrado");

					} else {

						// Estrutura o insert

							$this->db->set("nome", $this->nome);
							$this->db->set("cpf", $this->cpf);
							$this->db->set("rg", $this->rg);
							$this->db->set("telefone", $this->telefone);
							$this->db->set("email", $this->email);
							$this->db->set("data_nascimento", $this->data_nascimento);
							$this->db->set("cep", $this->cep);
							$this->db->set("cidade", $this->cidade);
							$this->db->set("bairro", $this->bairro);
							$this->db->set("numero", $this->numero);
							$this->db->set("endereco", $this->endereco);
							$this->db->set("uf", $this->uf);
							$this->db->set("data_cadastro", $this->data_cadastro);
							$this->db->set("horario_cadastro", $this->horario_cadastro);
							$this->db->set("cod_situacao", $this->cod_situacao);

						// Realiza o insert no banco de dados

							$this->db->insert('clientes');

						// Retorna a mensagem de sucesso

							return [
								"mensagem_titulo" => "Sucesso",
								"mensagem_texto" => "Cliente cadastrado(a) com sucesso",
								"mensagem_tipo" => "success"
							];
						
					}

			} else {

				// Declara a array mensagem para captar todos os erro(s) encontrado(s)

					$mensagem = [];
					if(!$validacao[0]){
						array_push($mensagem, "Verifique o campo Nome.\r");
					}
					if(!$validacao[1]){
						array_push($mensagem, "Verifique o campo CPF.\r");
					}
					if(!$validacao[2]){
						array_push($mensagem, "Verifique o campo Telefone.\r");
					}
					if(!$validacao[3]){
						array_push($mensagem, "Verifique o campo Email.\r");
					}
					if(!$validacao[4]){
						array_push($mensagem, "Verifique o campo CEP.\r");
					}
					if(!$validacao[5]){
						array_push($mensagem, "Verifique o campo Cidade.\r");
					}
					if(!$validacao[6]){
						array_push($mensagem, "Verifique o campo Bairro.\r");
					}
					if(!$validacao[7]){
						array_push($mensagem, "Verifique o campo Numero.\r");
					}
					if(!$validacao[8]){
						array_push($mensagem, "Verifique o campo Endereço.\r");
					}
					if(!$validacao[9]){
						array_push($mensagem, "Verifique o campo UF.\r");
					}
					if(!$validacao[10]){
						array_push($mensagem, "Verifique o campo Data de Nascimento.\r");
					}
				
				// Verifica a quantidade de erros para tratar o titulo

					if(count($mensagem) > 1){
						$titulo_mensagem = "Alguns erros foram encontrados";
					}else{
						$titulo_mensagem = "Um erro foi encontrado";
					}

				// Retorna a mensagem de erro na tela

					show_error($mensagem, 500, $titulo_mensagem);

			}

	}

	/**
	 * Função para Atualizar registro na Tabela
	 */
	
	public function atualizar_registro(){
		
		// Seta o timezone

			date_default_timezone_set("America/Sao_Paulo");

		// Verifica quais campos serão alterados

			// Verifica o Campo Nome

			if($this->nome != ""){
				if(strlen($this->nome) > 0 & $this->nome != null & $this->nome != "" & $this->nome != " "){
					$this->db->set("nome", $this->nome);
				}
			}

			// Verifica o Campo CPF

			if($this->cpf != ""){
				if(strlen($this->cpf) > 0 & $this->cpf != null & $this->cpf != "" & $this->cpf != " "){
					$this->db->set("cpf", $this->cpf);
				}
			}

			// Verifica o Campo Telefone

			if($this->telefone != ""){
				if(strlen($this->telefone) > 0 & $this->telefone != null & $this->telefone != "" & $this->telefone != " "){
					$this->db->set("telefone", $this->telefone);
				}
			}

			// Verifica o Campo Email

			if($this->email != ""){
				if(strlen($this->email) > 0 & $this->email != null & $this->email != "" & $this->email != " "){
					$this->db->set("email", $this->email);
				}
			}

			// Verifica o Campo CEP

			if($this->cep != ""){
				if(strlen($this->cep) > 0 & $this->cep != null & $this->cep != "" & $this->cep != " "){
					$this->db->set("cep", $this->cep);
				}
			}

			// Verifica o Campo Cidade

			if($this->cidade != ""){
				if(strlen($this->cidade) > 0 & $this->cidade != null & $this->cidade != "" & $this->cidade != " "){
					$this->db->set("cidade", $this->cidade);
				}
			}

			// Verifica o Campo Bairro

			if($this->bairro != ""){
				if(strlen($this->bairro) > 0 & $this->bairro != null & $this->bairro != "" & $this->bairro != " "){
					$this->db->set("bairro", $this->bairro);
				}
			}

			// Verifica o Campo Numero

			if($this->numero != ""){
				if(strlen($this->numero) > 0 & $this->numero != null & $this->numero != "" & $this->numero != " "){
					$this->db->set("numero", $this->numero);
				}
			}

			// Verifica o Campo Endereço

			if($this->endereco != ""){
				if(strlen($this->endereco) > 0 & $this->endereco != null & $this->endereco != "" & $this->endereco != " "){
					$this->db->set("endereco", $this->endereco);
				}
			}

			// Verifica o Campo UF

			if($this->endereco != ""){
				if(strlen($this->endereco) > 0 & $this->endereco != null & $this->endereco != "" & $this->endereco != " "){
					$this->db->set("endereco", $this->endereco);
				}
			}

			// Verifica o Campo Data de Nascimento

			if($this->data_nascimento != ""){
				$data_nascimento_array = explode("-", $this->data_nascimento);
				if(checkdate(date("m", $data_nascimento_array[1]), date("d",$data_nascimento_array[2]), date("Y", $data_nascimento_array[0]))){
					$this->db->set("data_nascimento", $this->data_nascimento);
				}
			}

		// Acrescenta as variaveis de timestamp do update

			$this->db->set("data_atualizacao", date("Y-m-d"));
			$this->db->set("horario_atualizacao", date("H:i:s", time()));
			$this->db->where("codigo", $this->codigo);

		// Verifica se o codigo do cliente foi enviado

			if($this->codigo != null){

				// Realiza as validações de Nome e CPF para evitar duplicidade

					if($this->nome != $this->nome_anterior && $this->cpf != $this->cpf_anterior){

						// Verifica se tem duplicidades

							$total_registros = $this->db->where("nome", $this->nome)
													->or_where("cpf", $this->cpf)
													->count_all_results('clientes');

							if($total_registros > 0){

								// Retorna a mensagem de erro na tela

									show_error("O CPF ou Nome informado já pertence á outro(a) cliente, verifique e tente novamente.", 500, "Um erro foi encontrado");

							} else {

								// Realiza o update no banco de dados

									$this->db->update('clientes');

								// Retorna a mensagem de sucesso

									return [
										"mensagem_titulo" => "Sucesso",
										"mensagem_texto" => "Cliente alterado(a) com sucesso",
										"mensagem_tipo" => "success"
									];

							}

					} else {

						// Realiza o update no banco de dados

							$this->db->update('clientes');

						// Retorna a mensagem de sucesso

							return [
								"mensagem_titulo" => "Sucesso",
								"mensagem_texto" => "Cliente alterado(a) com sucesso",
								"mensagem_tipo" => "success"
							];

					}

			} else {

				// Retorna a mensagem de erro na tela

					show_error("Informe o codigo do cliente para realizar a alteração", 500, "Um erro foi encontrado");

			}

	}

	/**
	 * Função para Excluir registro na Tabela
	 */
	
	public function excluir_registro(){
		
		// Seta o timezone

			date_default_timezone_set("America/Sao_Paulo");

		// Acrescenta as variaveis de timestamp do update

			$this->db->where("codigo", $this->codigo);
			$this->db->set("data_atualizacao", date("Y-m-d"));
			$this->db->set("horario_atualizacao", date("H:i:s", time()));
			$this->db->set("cod_situacao", 2);

		// Verifica se o codigo do cliente foi enviado

			if($this->codigo != null){

				// Realiza o update no banco de dados

					$this->db->update('clientes');

				// Retorna a mensagem de sucesso

					return [
						"mensagem_titulo" => "Sucesso",
						"mensagem_texto" => "Cliente excluído(a) com sucesso",
						"mensagem_tipo" => "success"
					];

			} else {

				// Retorna a mensagem de erro na tela

					show_error("Informe o codigo do cliente para realizar a exclusão", 500, "Um erro foi encontrado");

			}

	}

	/**
	 * Função para Consultar registro específico na Tabela
	 */
	
	public function consultar_registro(){

		// Verifica se o codigo do cliente foi enviado

			if($this->codigo != null){

				// Define a Query

					$query = $this->db->where('cod_situacao', 1)
						->where("codigo", $this->codigo)
						->get('clientes');

				// Retorna os resultados
						
					return $query->result();

			} else {

				// Retorna a mensagem de erro na tela

					show_error("Informe o codigo do cliente para realizar a consulta", 500, "Um erro foi encontrado");

			}

	}

}

?>