<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lanches_model extends CI_Model {

	public $codigo;
	public $nome;
	public $nome_anterior;
	public $descricao;
	public $valor_producao;
	public $valor_venda;
	public $data_cadastro;
	public $horario_cadastro;
	public $data_atualizacao;
	public $horario_atualizacao;
	public $cod_status;
	public $cod_situacao;
	public $registros_paginas;
	public $pagina_registro;
	public $filtros;

	/**
	 * Função para inicialização da model
	 */
	
	public function __construct() {
        parent::__construct();
    }

	/**
	 * Função para pegar o ultimo registro da tabela
	 */

	public function ultimo_registro(){

		// Define a Query

			$query = $this->db->where('cod_situacao', 1)
				->order_by('codigo', 'desc')
				->get('lanches', 1);

		// Retorna os resultados

			return $query->result();

	}

	/**
	 * Função para pegar o ultimo registro da tabela
	 */

	public function consulta_registros(){

		// Verifica se a Consulta tera paginação ou não

			if($this->pagina_registro != null && $this->registros_paginas != null){

				// Define a Query

					$query = $this->db->where('cod_situacao', 1);

				// Verifica se a consulta tera filtros

					if($this->filtros != null){
						$query->where($this->filtros);
					}

				// Realiza a Consulta

					$query->order_by('codigo', 'asc')
						->limit($this->registros_paginas)
						->offset($this->pagina_registro);

			} else {

				// Define a Query

					$query = $this->db->where('cod_situacao', 1);

				// Verifica se a consulta tera filtros

					if($this->filtros != null){
						$query->where($this->filtros);
					}

				// Realiza a Consulta

					$query->order_by('codigo', 'asc'); 

			}

		// Retorna os resultados
				
			return [
				"lanches" => $query->get('lanches')->result(),
				"total_lanches" => $query->count_all_results('lanches') 
			];
	}

	/**
	 * Função para Inserir registro na Tabela
	 */
	
	public function inserir_registro(){
		
		// Seta o timezone

			date_default_timezone_set("America/Sao_Paulo");

		// Declara as Variaveis

			$this->data_cadastro = date("Y-m-d");
			$this->horario_cadastro = date("H:i:s", time());
			$this->cod_status = 1;
			$this->cod_situacao = 1;
			$validacao = [];

		// Realiza as validações

			$validacao[0] = strlen($this->nome) > 0 & $this->nome != null & $this->nome != "" & $this->nome != " ";
			$validacao[1] = $this->valor_venda > 0 & $this->valor_venda != null;

		// Checa se as validações estão corretas para inserir o registro e caso não retorna o erro em tela

			$checa_validacao = $validacao[0] & $validacao[1]; 

			if($checa_validacao){

				// Verifica se já existe um registro com o Nome ou CPF informado

					$total_registros = $this->db->where("nome", $this->nome)
												->count_all_results('lanches');

					if($total_registros > 0){

						// Retorna a mensagem de erro na tela

							show_error("Já existe um lanche com o nome informado, verifique e tente novamente", 500, "Um erro foi encontrado");

					} else {

						// Estrutura o insert

							$this->db->set("nome", $this->nome);
							$this->db->set("descricao", $this->descricao);
							$this->db->set("valor_producao", $this->valor_producao);
							$this->db->set("valor_venda", $this->valor_venda);
							$this->db->set("cod_status", $this->cod_status);
							$this->db->set("cod_situacao", $this->cod_situacao);
							$this->db->set("data_cadastro", $this->data_cadastro);
							$this->db->set("horario_cadastro", $this->horario_cadastro);

						// Realiza o insert no banco de dados

							$this->db->insert('lanches');

						// Retorna a mensagem de sucesso

							return [
								"mensagem_titulo" => "Sucesso",
								"mensagem_texto" => "Lanche cadastrado com sucesso",
								"mensagem_tipo" => "success"
							];
						
					}

			} else {

				// Declara a array mensagem para captar todos os erro(s) encontrado(s)

					$mensagem = [];
					if(!$validacao[0]){
						array_push($mensagem, "Verifique o campo Nome.\r");
					}
					if(!$validacao[1]){
						array_push($mensagem, "Verifique o campo Valor de Venda.\r");
					}
				
				// Verifica a quantidade de erros para tratar o titulo

					if(count($mensagem) > 1){
						$titulo_mensagem = "Alguns erros foram encontrados";
					}else{
						$titulo_mensagem = "Um erro foi encontrado";
					}

				// Retorna a mensagem de erro na tela

					show_error($mensagem, 500, $titulo_mensagem);

			}

	}

	/**
	 * Função para Atualizar registro na Tabela
	 */
	
	public function atualizar_registro(){
		
		// Seta o timezone

			date_default_timezone_set("America/Sao_Paulo");

		// Verifica quais campos serão alterados

			// Verifica o Campo Nome

			if($this->nome != ""){
				if(strlen($this->nome) > 0 & $this->nome != null & $this->nome != "" & $this->nome != " "){
					$this->db->set("nome", $this->nome);
				}
			}

			// Verifica o Campo Valor de Venda

			if($this->valor_venda != ""){
				if(strlen($this->valor_venda) > 0 & $this->valor_venda != null & $this->valor_venda != "" & $this->valor_venda != " "){
					$this->db->set("valor_venda", $this->valor_venda);
				}
			}

			// Verifica o Campo Status

			if($this->cod_status != ""){
				if($this->cod_status > 0 && $this->cod_status < 3){
					$this->db->set("cod_status", $this->cod_status);
				}
			}

		// Acrescenta as variaveis de timestamp do update

			$this->db->set("data_atualizacao", date("Y-m-d"));
			$this->db->set("horario_atualizacao", date("H:i:s", time()));
			$this->db->where("codigo", $this->codigo);

		// Verifica se o codigo do lanche foi enviado

			if($this->codigo != null){

				// Realiza as validações de Nome e CPF para evitar duplicidade

					if($this->nome != $this->nome_anterior){

						// Verifica se tem duplicidades

							$total_registros = $this->db->where("nome", $this->nome)
													->count_all_results('lanches');

							if($total_registros > 0){

								// Retorna a mensagem de erro na tela

									show_error("Já existe um lanche com o nome informado, verifique e tente novamente", 500, "Um erro foi encontrado");

							} else {

								// Realiza o update no banco de dados

									$this->db->update('lanches');

								// Retorna a mensagem de sucesso

									return [
										"mensagem_titulo" => "Sucesso",
										"mensagem_texto" => "Lanche alterado com sucesso",
										"mensagem_tipo" => "success"
									];

							}

					} else {

						// Realiza o update no banco de dados

							$this->db->update('lanches');

						// Retorna a mensagem de sucesso

							return [
								"mensagem_titulo" => "Sucesso",
								"mensagem_texto" => "Lanche alterado com sucesso",
								"mensagem_tipo" => "success"
							];

					}

			} else {

				// Retorna a mensagem de erro na tela

					show_error("Informe o codigo do lanche para realizar a alteração", 500, "Um erro foi encontrado");

			}

	}

	/**
	 * Função para Excluir registro na Tabela
	 */
	
	public function excluir_registro(){
		
		// Seta o timezone

			date_default_timezone_set("America/Sao_Paulo");

		// Acrescenta as variaveis de timestamp do update

			$this->db->where("codigo", $this->codigo);
			$this->db->set("data_atualizacao", date("Y-m-d"));
			$this->db->set("horario_atualizacao", date("H:i:s", time()));
			$this->db->set("cod_situacao", 2);

		// Verifica se o codigo do lanche foi enviado

			if($this->codigo != null){

				// Realiza o update no banco de dados

					$this->db->update('lanches');

				// Retorna a mensagem de sucesso

					return [
						"mensagem_titulo" => "Sucesso",
						"mensagem_texto" => "Lanche excluído com sucesso",
						"mensagem_tipo" => "success"
					];

			} else {

				// Retorna a mensagem de erro na tela

					show_error("Informe o codigo do lanche para realizar a exclusão", 500, "Um erro foi encontrado");

			}

	}

	/**
	 * Função para Consultar registro específico na Tabela
	 */
	
	public function consultar_registro(){

		// Verifica se o codigo do lanche foi enviado

			if($this->codigo != null){

				// Define a Query

					$query = $this->db->where('cod_situacao', 1)
						->where("codigo", $this->codigo)
						->get('lanches');

				// Retorna os resultados
						
					return $query->result();

			} else {

				// Retorna a mensagem de erro na tela

					show_error("Informe o codigo do lanche para realizar a consulta", 500, "Um erro foi encontrado");

			}

	}

}

?>