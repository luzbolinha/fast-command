<footer class="main-footer">
	<div class="pull-right hidden-xs">
	  <b>Versão</b> <?php echo $versao_sistema ?>
	</div>
	<strong><img src="<?php echo $logo_sistema_menu_black ?>" width="80px">&copy; 2019 <a href="#">Luis Gustavo Hunzecher de Castro</a>.</strong> Todos os direitos reservados.
</footer>