-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.38-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela dbfastcommand.bebidas
CREATE TABLE IF NOT EXISTS `bebidas` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` text,
  `valor_compra` float(10,2) DEFAULT NULL,
  `valor_venda` float(10,2) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_status` int(11) NOT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.bebidas: ~0 rows (aproximadamente)
DELETE FROM `bebidas`;
/*!40000 ALTER TABLE `bebidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `bebidas` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(50) NOT NULL,
  `rg` varchar(50) DEFAULT NULL,
  `telefone` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `data_nascimento` date NOT NULL,
  `cep` varchar(50) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `numero` varchar(50) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `uf` varchar(10) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.clientes: ~9 rows (aproximadamente)
DELETE FROM `clientes`;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(26, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', '41.968.822-5', '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', 'SP', '2019-10-21', '05:39:18', '2019-10-21', '06:22:35', 2);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(27, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', '', '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', 'SP', '2019-10-21', '06:05:25', '2019-10-21', '06:23:05', 2);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(28, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', '', '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', 'SP', '2019-10-21', '06:06:40', '2019-10-21', '06:23:08', 2);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(29, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', '', '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', 'SP', '2019-10-21', '06:07:29', '2019-10-21', '06:23:12', 2);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(30, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', NULL, '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', '', '0000-00-00', '00:00:00', '2019-10-21', '06:20:49', 1);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(31, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', NULL, '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', '', '0000-00-00', '00:00:00', '2019-10-21', '06:20:49', 1);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(32, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', NULL, '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', '', '0000-00-00', '00:00:00', '2019-10-21', '06:20:49', 1);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(33, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', NULL, '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', '', '0000-00-00', '00:00:00', '2019-10-21', '06:20:49', 1);
INSERT INTO `clientes` (`codigo`, `nome`, `cpf`, `rg`, `telefone`, `email`, `data_nascimento`, `cep`, `cidade`, `bairro`, `numero`, `endereco`, `uf`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_situacao`) VALUES
	(34, 'Luis Gustavo Hunzecher de Cast', '426.173.458-90', '', '(14) 99111-0819', 'luisgustavo@creatise.com.br', '1995-02-03', '17030-510', 'Bauru', 'Parque Paulistano', '639', 'Rua Capitão Alcides', 'SP', '2019-10-21', '06:14:19', '2019-10-21', '06:20:49', 1);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.comandas
CREATE TABLE IF NOT EXISTS `comandas` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `cod_clientes` int(11) NOT NULL,
  `total` float(10,2) DEFAULT NULL,
  `troco` float(10,2) DEFAULT NULL,
  `pago` float(10,2) DEFAULT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_status` int(11) NOT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.comandas: ~0 rows (aproximadamente)
DELETE FROM `comandas`;
/*!40000 ALTER TABLE `comandas` DISABLE KEYS */;
INSERT INTO `comandas` (`codigo`, `cod_clientes`, `total`, `troco`, `pago`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(1, 31, NULL, NULL, NULL, '2019-10-21', '10:21:43', NULL, NULL, 1, 1);
INSERT INTO `comandas` (`codigo`, `cod_clientes`, `total`, `troco`, `pago`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(2, 33, NULL, NULL, NULL, '2019-10-21', '10:38:04', NULL, NULL, 1, 1);
/*!40000 ALTER TABLE `comandas` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.comandas_bebidas
CREATE TABLE IF NOT EXISTS `comandas_bebidas` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `cod_comandas` int(11) NOT NULL,
  `cod_bebidas` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.comandas_bebidas: ~0 rows (aproximadamente)
DELETE FROM `comandas_bebidas`;
/*!40000 ALTER TABLE `comandas_bebidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `comandas_bebidas` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.comandas_lanches
CREATE TABLE IF NOT EXISTS `comandas_lanches` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `cod_comandas` int(11) NOT NULL,
  `cod_lanches` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.comandas_lanches: ~0 rows (aproximadamente)
DELETE FROM `comandas_lanches`;
/*!40000 ALTER TABLE `comandas_lanches` DISABLE KEYS */;
/*!40000 ALTER TABLE `comandas_lanches` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.comandas_lanches_x_ingredientes
CREATE TABLE IF NOT EXISTS `comandas_lanches_x_ingredientes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `cod_comandas_lanches` int(11) NOT NULL,
  `cod_ingredientes` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.comandas_lanches_x_ingredientes: ~0 rows (aproximadamente)
DELETE FROM `comandas_lanches_x_ingredientes`;
/*!40000 ALTER TABLE `comandas_lanches_x_ingredientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `comandas_lanches_x_ingredientes` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.forma_pagamento
CREATE TABLE IF NOT EXISTS `forma_pagamento` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` text,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.forma_pagamento: ~0 rows (aproximadamente)
DELETE FROM `forma_pagamento`;
/*!40000 ALTER TABLE `forma_pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `forma_pagamento` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.ingredientes
CREATE TABLE IF NOT EXISTS `ingredientes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `descricao` text,
  `valor_venda` float(10,2) NOT NULL,
  `valor_compra` int(11) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `habilitar_adicional` int(11) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_status` int(11) NOT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.ingredientes: ~7 rows (aproximadamente)
DELETE FROM `ingredientes`;
/*!40000 ALTER TABLE `ingredientes` DISABLE KEYS */;
INSERT INTO `ingredientes` (`codigo`, `nome`, `descricao`, `valor_venda`, `valor_compra`, `quantidade`, `habilitar_adicional`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(1, 'Rúcula', '', 0.40, NULL, NULL, 1, '2019-10-21', '08:55:45', NULL, NULL, 1, 1);
INSERT INTO `ingredientes` (`codigo`, `nome`, `descricao`, `valor_venda`, `valor_compra`, `quantidade`, `habilitar_adicional`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(2, 'Alface', '', 0.30, NULL, NULL, 1, '2019-10-21', '09:00:24', NULL, NULL, 1, 1);
INSERT INTO `ingredientes` (`codigo`, `nome`, `descricao`, `valor_venda`, `valor_compra`, `quantidade`, `habilitar_adicional`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(3, 'Cheddar', '', 1.50, NULL, NULL, 1, '2019-10-21', '09:00:38', NULL, NULL, 1, 1);
INSERT INTO `ingredientes` (`codigo`, `nome`, `descricao`, `valor_venda`, `valor_compra`, `quantidade`, `habilitar_adicional`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(4, 'Bacon', '', 2.00, NULL, NULL, 1, '2019-10-21', '09:00:44', NULL, NULL, 1, 1);
INSERT INTO `ingredientes` (`codigo`, `nome`, `descricao`, `valor_venda`, `valor_compra`, `quantidade`, `habilitar_adicional`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(5, 'Hambúrguer', '', 3.00, NULL, NULL, 1, '2019-10-21', '09:00:54', NULL, NULL, 1, 1);
INSERT INTO `ingredientes` (`codigo`, `nome`, `descricao`, `valor_venda`, `valor_compra`, `quantidade`, `habilitar_adicional`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(6, 'Ovo', '', 0.80, NULL, NULL, 1, '2019-10-21', '09:01:01', NULL, NULL, 1, 1);
INSERT INTO `ingredientes` (`codigo`, `nome`, `descricao`, `valor_venda`, `valor_compra`, `quantidade`, `habilitar_adicional`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(7, 'Queijo', '', 1.50, NULL, NULL, 1, '2019-10-21', '09:01:10', NULL, NULL, 1, 1);
/*!40000 ALTER TABLE `ingredientes` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.ingredientes_x_lanches
CREATE TABLE IF NOT EXISTS `ingredientes_x_lanches` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `cod_lanche` int(11) NOT NULL,
  `cod_ingrediente` int(11) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.ingredientes_x_lanches: ~0 rows (aproximadamente)
DELETE FROM `ingredientes_x_lanches`;
/*!40000 ALTER TABLE `ingredientes_x_lanches` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingredientes_x_lanches` ENABLE KEYS */;

-- Copiando estrutura para tabela dbfastcommand.lanches
CREATE TABLE IF NOT EXISTS `lanches` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` text,
  `valor_producao` float(10,2) DEFAULT NULL,
  `valor_venda` float(10,2) NOT NULL,
  `data_cadastro` date NOT NULL,
  `horario_cadastro` time NOT NULL,
  `data_atualizacao` date DEFAULT NULL,
  `horario_atualizacao` time DEFAULT NULL,
  `cod_status` int(11) NOT NULL,
  `cod_situacao` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela dbfastcommand.lanches: ~4 rows (aproximadamente)
DELETE FROM `lanches`;
/*!40000 ALTER TABLE `lanches` DISABLE KEYS */;
INSERT INTO `lanches` (`codigo`, `nome`, `descricao`, `valor_producao`, `valor_venda`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(1, 'X-Bacon', 'Bacon, hambúrguer de carne e queijo', 0.00, 10.00, '0000-00-00', '00:00:00', '2019-10-21', '08:39:39', 1, 1);
INSERT INTO `lanches` (`codigo`, `nome`, `descricao`, `valor_producao`, `valor_venda`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(2, 'X-Burguer', 'Hambúrguer de Carne e queijo', 0.00, 8.00, '2019-10-21', '08:40:40', NULL, NULL, 1, 1);
INSERT INTO `lanches` (`codigo`, `nome`, `descricao`, `valor_producao`, `valor_venda`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(3, 'X-Egg', 'Ovo, hambúrguer de carne e queijo', 0.00, 8.80, '2019-10-21', '08:41:05', NULL, NULL, 1, 1);
INSERT INTO `lanches` (`codigo`, `nome`, `descricao`, `valor_producao`, `valor_venda`, `data_cadastro`, `horario_cadastro`, `data_atualizacao`, `horario_atualizacao`, `cod_status`, `cod_situacao`) VALUES
	(4, 'X-Egg Bacon', 'Ovo, bacon, hambúrguer de carne e queijo', 0.00, 10.80, '2019-10-21', '08:41:31', NULL, NULL, 1, 1);
/*!40000 ALTER TABLE `lanches` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
